$(document).ready(function () {

    PreInit();

    // Init fTelnet
    fTelnet.Init();

    PostInit();
});

function PostInit() {

    fTelnet.Connect();


}

function PreInit() {
    fTelnet.ConnectionType = 'telnet';
    fTelnet.Hostname = 'localhost';
    fTelnet.Port = '8000';
    fTelnet.ButtonBarVisible = true;
}
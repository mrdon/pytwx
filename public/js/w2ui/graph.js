$(document).ready(function () {
    var nodes = new vis.DataSet([]);
    var edges = new vis.DataSet([]);
    // create a network
    var container = document.getElementById('mynetwork');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        layout: {
            randomSeed: 2002,
            hierarchical: {
                enabled: true,
                // nodeSpacing: 80,
                // treeSpacing: 140,
                levelSeparation: 80,
                direction: "LR",
                sortMethod: 'hubsize'
            }
        },
        edges: {
            arrows: 'to'
        },
        nodes: {
            shape: 'box'
        },
        width: w2ui['layout'].get('right').width + "px"
    };
    var network = new vis.Network(container, data, options);

    game.addEventListener('currentSector', function(s) {
        console.log("current sector change: " + s);
        nodes.clear();
        edges.clear();

        function ensureSectorNode(sectorId) {
            var color = 'grey';
            var sector = game.getSector(sectorId);
            var warpsNum = Object.keys(sector.warps).length;
            if (warpsNum > 0) {
                color = 'green';
            }
            var label = sectorId;
            if (warpsNum > 0) {
                label += " [" + warpsNum + "]";
            } else {
                label += " [??]";
            }

            var args = {id: sectorId, label: label, color: color, fixed: false, physics: false};
            nodes.update(args);
        }
        function renderRing(processedIds, sectorId, num) {
            var sector = game.getSector(sectorId);
            if (num > 2) {
                return;
            }
            processedIds.push(sectorId);
            ensureSectorNode(sectorId);
            Object.keys(sector.warps).forEach(function(warpId) {
                if (!(warpId in processedIds)) {
                    ensureSectorNode(warpId);
                    edges.update({id: warpId + "-" + sectorId, to: warpId, from: sectorId});
                    renderRing(processedIds, warpId, num + 1)
                }
            });
            Object.keys(sector.warpsIn).forEach(function(warpId) {
                if (!(warpId in processedIds)) {
                    ensureSectorNode(warpId);
                    edges.update({id: sectorId + "-" + warpId, to: sectorId, from: warpId});
                    renderRing(processedIds, warpId, num + 1)
                }
            });
        }
        renderRing([], s.id, 0);
        // network.focus(currentSectorId);
        // network.moveTo(currentSectorId, {animation: false});
        network.selectNodes([s.id], true);
        network.fit({
            animation: false
        });
        $('#currentSector').html(s.id);

        // network.focus(currentSectorId);
    });
});


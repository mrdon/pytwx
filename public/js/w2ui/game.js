function Game() {
    var sectors = [];
    var listeners = {};
    var that = this;

    this.currentSector = 0;

    this.addEventListener = function (event, callback) {
        var lst = listeners[event];
        if (lst === undefined) {
            listeners[event] = lst = [];
        }
        lst.push(callback);
    };

    this.getSector = function (id) {
        if (!(id in sectors)) {
            sectors[id] = {
                warps: {},
                warpsIn: {},
                id: id
            }
        }
        return sectors[id];
    };

    this.start = function (sessionId) {
        initEvents(sessionId);
    };

    function fireEvent(name, data) {
        var lst = listeners[name];
        if (lst) {
            lst.forEach(function (callback) {
                callback(data);
            })
        }
    }

    function initEvents(sessionId) {
        var evtSource = null;


        function newSource() {
            var onErrorReconnectHandler = function (event) {
                var txt;
                switch (event.target.readyState) {
                    case EventSource.CONNECTING:
                        txt = 'Reconnecting...';
                        break;
                    case EventSource.CLOSED:
                        txt = 'Reinitializing...';
                        newSource();
                        break;
                }
                console.log(txt);
            };
            evtSource = new EventSource("/" + sessionId + "/events");
            evtSource.addEventListener('sector', function (e) {
                console.log("sector event");
                var data = JSON.parse(e.data);
                var s = that.getSector(data.id);
                s.warps = data.warps.reduce(function (map, obj) {
                    map[obj] = true;
                    return map;
                }, {});
                data.warps.forEach(function (e) {
                    var w = that.getSector(e);
                    w.warpsIn[s.id] = true;
                });
                fireEvent('sector', s);
            });
            evtSource.addEventListener('currentSector', function (e) {
                console.log("currentSector event");
                var data = JSON.parse(e.data);
                var s = that.getSector(data.id);
                that.currentSector = s;
                fireEvent('currentSector', s);
            });
            evtSource.onopen = function () {
                console.log("Connection to server opened.");
            };
            evtSource.onerror = onErrorReconnectHandler
        }
        newSource();
    }
}

var game = new Game();


$(function () {

    $('#layout').w2layout({
        name: 'layout',
        panels: [
            {type: 'top', size: 50, resizable: true, content: 'top'},
            {type: 'left', size: 200, resizable: true, content: 'left'},
            {type: 'main', size: 1000, content: 'main'},
            {type: 'preview', resizable: true, content: 'preview'},
            {type: 'right', size: 200, resizable: true, content: 'right'},
            {type: 'bottom', size: 50, resizable: true, content: 'bottom'}
        ]
    });

    function loadAndStart(sessionId) {
        w2ui['layout'].load('left', 'panels/dashboard.html');
        w2ui['layout'].load('main', 'panels/terminal.html');
        w2ui['layout'].load('right', 'panels/graph.html');

        game.start(sessionId);
    }

    if (sessionId) {
        loadAndStart(sessionId);
    } else {
        w2ui['layout'].content('main', 'login needed');
    }
});
$(document).ready(function () {
    document.title = 'PyTWX';

    // Load the remote css, if necessary
    // document.getElementById('EmbedCSS').href = decodeURIComponent(GetQueryStringValue('CSS'));

    FinishDocumentReady();
    // Load the remote splash screen, if necessary
    // if (GetQueryStringValue('SplashScreen') == '') {
    //     FinishDocumentReady();
    // } else {
    //     $.getScript(decodeURIComponent(GetQueryStringValue('SplashScreen'))).always(function () {
    //         FinishDocumentReady();
    //     });
    // }
});

function FinishDocumentReady() {
    // Parse querystring parameters and setup fTelnet properties that can be set before init
    PreInit();

    // Init fTelnet
    fTelnet.Init();

    // Parse querystring parameters and setup fTelnet properties that have to be set after init
    PostInit();
}

function PostInit() {
    var evtSource = new EventSource("/events");
    var nodes = new vis.DataSet([]);
    var edges = new vis.DataSet([]);
    // create a network
    var container = document.getElementById('mynetwork');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        layout: {
            randomSeed: 2002,
            hierarchical: {
                enabled: true,
                // nodeSpacing: 80,
                // treeSpacing: 140,
                levelSeparation: 80,
                sortMethod: 'hubsize'
            }
        },
        edges: {
            arrows: 'to'
        },
        nodes: {
            shape: 'box'
        }
    };
    var network = new vis.Network(container, data, options);

    var sectors = [];
    var currentSectorId = 0;

    function ensureSector(id) {
        if (!(id in sectors)) {
            sectors[id] = {warps: {}, warpsIn: {}}
        }
    }

    evtSource.addEventListener('sector', function (e) {
        console.log("sector event");
        var data = JSON.parse(e.data);
        ensureSector(data.id);

        sectors[data.id].warps = data.warps.reduce(function (map, obj) {
            map[obj] = true;
            return map;
        }, {});
        data.warps.forEach(function (e) {
            ensureSector(e);
            sectors[e].warpsIn[data.id] = true;
        });
        // nodes.update({id: data['id'], label: data['id']});
        // data.warps.forEach(function (e) {
        //     nodes.update({id: e, label: e});
        //     edges.update({id: data['id'] + "-" + e, from: data['id'], to: e});
        // })
    });
    evtSource.addEventListener('currentSector', function (e) {
        console.log("currentSector event");
        var data = JSON.parse(e.data);
        currentSectorId = data.id;
        ensureSector(data.id);

        nodes.clear();
        edges.clear();

        function ensureSectorNode(sectorId) {
            ensureSector(sectorId);
            var color = 'grey';
            var sector = sectors[sectorId];
            var warpsNum = Object.keys(sector.warps).length;
            if (warpsNum > 0) {
                color = 'green';
            }
            var label = sectorId;
            if (warpsNum > 0) {
                label += " [" + warpsNum + "]";
            } else {
                label += " [??]";
            }

            var args = {id: sectorId, label: label, color: color, fixed: false, physics: false};
            console.log("sector: " + sectorId + " current: " + currentSectorId + " data: " + JSON.stringify(args));
            nodes.update(args);
        }
        function renderRing(processedIds, sectorId, num) {
            ensureSector(sectorId);
            var sector = sectors[sectorId];
            if (num > 2) {
                return;
            }
            processedIds.push(sectorId);
            ensureSectorNode(sectorId);
            Object.keys(sector.warps).forEach(function(warpId) {
                if (!(warpId in processedIds)) {
                    ensureSectorNode(warpId);
                    edges.update({id: warpId + "-" + sectorId, to: warpId, from: sectorId});
                    renderRing(processedIds, warpId, num + 1)
                }
            });
            Object.keys(sector.warpsIn).forEach(function(warpId) {
                if (!(warpId in processedIds)) {
                    ensureSectorNode(warpId);
                    edges.update({id: sectorId + "-" + warpId, to: sectorId, from: warpId});
                    renderRing(processedIds, warpId, num + 1)
                }
            });
        }
        renderRing([], currentSectorId, 0);
        // network.focus(currentSectorId);
        // network.moveTo(currentSectorId, {animation: false});
        network.selectNodes([currentSectorId], true);
        network.fit({
            animation: false
        });
        $('#currentSector').html(data.id);

        // network.focus(currentSectorId);
    });
    evtSource.onopen = function () {
        console.log("Connection to server opened.");
    };
    var onErrorReconnectHandler = function (event) {
        var txt;
        switch( event.target.readyState ){
            case EventSource.CONNECTING:
                txt = 'Reconnecting...';
                break;
            case EventSource.CLOSED:
                txt = 'Reinitializing...';
                evtSource = new EventSource("/events");
                evtSource.onerror = onErrorReconnectHandler;
                break;
        }
        console.log(txt);
    };
    evtSource.onerror = onErrorReconnectHandler;
    fTelnet.Connect();


}

function PreInit() {
    fTelnet.ConnectionType = 'telnet';
    fTelnet.Hostname = 'localhost';
    fTelnet.Port = '8000';
    // if (GetQueryStringValue('Proxy') != '') {
    //     if (GetQueryStringValue('Proxy') == 'false') {
    //         // Legacy: false meant don't use a proxy, so don't do anything here
    //     } else if (GetQueryStringValue('Proxy') == 'true') {
    //         // Legacy: true meant use a proxy, so use the primary proxy here
    //         fTelnet.ProxyHostname = 'proxy-us-ga.ftelnet.ca';
    //     } else {
    //         // Any value other than true or false should be the actual proxy hostname
    //         fTelnet.ProxyHostname = GetQueryStringValue('Proxy');
    //     }
    //     fTelnet.ProxyPort = (GetQueryStringValue('ProxyPort') == '') ? '1123' : GetQueryStringValue('ProxyPort');
    //     fTelnet.ProxyPortSecure = (GetQueryStringValue('ProxyPortSecure') == '') ? '11235' : GetQueryStringValue('ProxyPortSecure');
    // }
    //
    // if (GetQueryStringValue('Emulation') == 'RIP') {
    //     fTelnet.Emulation = 'RIP';
    // }

    fTelnet.ButtonBarVisible = true;
    // (GetQueryStringValue('TopButtons') != 'false');
    // if (GetQueryStringValue('VirtualKeyboard') != 'auto') {
    //   fTelnet.VirtualKeyboardVisible = (GetQueryStringValue('VirtualKeyboard') != 'off');
    // }
}
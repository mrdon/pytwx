from unittest.mock import MagicMock

import pytest

from pytwx.database import Database, Sector, DataHeader, ProductType, FighterType
from pytwx.gui import TwxGui
from pytwx.process import ModExtractor, Display
from pytwx.script import ModInterpreter


@pytest.fixture
def extractor():
    db = MagicMock(Database)
    db.db_header = DataHeader(1000)
    db.load_sector.side_effect = lambda _: Sector(15)
    return ModExtractor(db, MagicMock(ModInterpreter), MagicMock(TwxGui))


def test_sector_line(extractor):
    extractor.process_line("Sector  : 28 in uncharted space.")
    assert extractor.current_sector == 28


def test_warps_one(extractor: ModExtractor):
    extractor.process_line("Sector  : 28 in uncharted space.")
    extractor.process_line("Warps to Sector(s) :  (100)")
    s = extractor._database.save_sector.call_args_list[0][0][0]
    assert s.warps == 1
    assert s.warp == [100]


def test_process_warp_line(extractor: ModExtractor):
    extractor._process_warp_line("283 > (417) > (782) > (711) > (926)")
    assert extractor._last_warp == 926


def test_process_warp_line_none(extractor: ModExtractor):
    extractor._process_warp_line("")
    assert extractor._last_warp == 0


def test_process_warp_line_engage(extractor: ModExtractor):
    extractor._process_warp_line("Engage the Autopilot? (Y/N/Single step/Express) [Y] N")
    assert extractor._last_warp == 0


def test_process_warp_line_autopilot(extractor: ModExtractor):
    extractor._process_warp_line("<Auto Pilot Engaging>")
    assert extractor._last_warp == 0


def test_process_cim_line(extractor: ModExtractor):
    extractor._current_display = Display.cim
    extractor.process_line("  13  222  928  955")
    s = extractor._database.save_sector.call_args_list[0][0][0]
    assert 222 in s.warp


def test_process_cim_unexplored(extractor: ModExtractor):
    extractor.process_line(": ")
    extractor.process_line("     1      2      3      4      5      6      7      8      9     10     11")
    extractor.process_line("     12     14     15     16     17     18     19     20     21     23     24")
    assert not extractor._database.save_sector.called


def test_process_cim_port(extractor: ModExtractor):
    extractor.process_line(": ")
    extractor.process_line("  22 - 1235  55% - 2110 100%    290  22% ")
    extractor.process_line("  25    860 100%   2810 100% - 2550 100% ")
    s22 = extractor._database.save_sector.call_args_list[0][0][0]
    assert 1235 == s22.port.product_amount[ProductType.fuel_ore]
    assert 2110 == s22.port.product_amount[ProductType.organics]
    assert 290 == s22.port.product_amount[ProductType.equipment]

    assert 55 == s22.port.product_percent[ProductType.fuel_ore]
    assert 100 == s22.port.product_percent[ProductType.organics]
    assert 22 == s22.port.product_percent[ProductType.equipment]
    s25 = extractor._database.save_sector.call_args_list[1][0][0]
    assert 860 == s25.port.product_amount[ProductType.fuel_ore]
    assert 2810 == s25.port.product_amount[ProductType.organics]
    assert 2550 == s25.port.product_amount[ProductType.equipment]

    assert 100 == s25.port.product_percent[ProductType.fuel_ore]
    assert 100 == s25.port.product_percent[ProductType.organics]
    assert 100 == s25.port.product_percent[ProductType.equipment]


def test_prompt_resets_display(extractor: ModExtractor):
    extractor._current_display = Display.fig_scan
    extractor.process_line('Command [TL=00:00:00]:[142] (?=Help)? : ')
    assert extractor._current_display == Display.none
    assert extractor._current_sector_index == 142


def test_computer_prompt_resets_display(extractor: ModExtractor):
    extractor._current_display = Display.fig_scan
    extractor.process_line("Computer command [TL=00:00:00]:[142] (?=Help)?")
    assert extractor._current_display == Display.none


def test_sector(extractor: ModExtractor):
    for line in """
Sector  : 28 in uncharted space.
Ports   : Brahe Major, Class 3 (SBB)
Planets : (M) asd
Warps to Sector(s) :  (100)
""".split("\n"):
        extractor.process_line(line)
    s = extractor._database.save_sector.call_args_list[0][0][0]  # type: Sector
    assert s.warp == [100]
    assert s.planets[0].name == "(M) asd"
    assert s.port.name == "Brahe Major"
    assert s.port.class_index == 3
    assert s.constellation == "uncharted space"


def test_sector_trader(extractor: ModExtractor):
    extractor._current_display = Display.sector
    extractor.process_line("Traders : Private 1st Class Furiousminer, w/ 30 ftrs,")
    extractor.process_line("           in Golden Glory (Le Richelieu Merchant Cruiser)")

    lst = extractor._trader_list
    assert lst[0].name == "Private 1st Class Furiousminer"
    assert lst[0].figs == 30
    assert lst[0].ship_name == "Golden Glory"
    assert lst[0].ship_type == "Le Richelieu Merchant Cruiser"


def test_stardock_in_v(extractor: ModExtractor):
    extractor.process_line("         The StarDock is located in sector 926.")
    s = extractor._database.save_sector.call_args_list[0][0][0]  # type: Sector
    assert s.port.class_index == 9


def test_port(extractor: ModExtractor):
    for line in """Docking...

Commerce report for Omicron Outpost: 08:48:00 PM Fri May 04, 2029

-=-=-        Docking Log        -=-=-
Golden Glory docked 2 hours and 41 minutes ago.

 Items     Status  Trading % of max OnBoard
 -----     ------  ------- -------- -------
Fuel Ore   Selling   2088     82%       0
Organics   Selling    853     47%      20
Equipment  Buying    1383     49%       0
""".split('\n'):
        extractor.process_line(line)
        
    s = extractor._database.save_sector.call_args_list[0][0][0]  # type: Sector
    assert s.port.product_amount[ProductType.fuel_ore] == 2088
    assert s.port.product_amount[ProductType.organics] == 853
    assert s.port.product_amount[ProductType.equipment] == 1383
    assert s.port.product_percent[ProductType.fuel_ore] == 82
    assert s.port.product_percent[ProductType.organics] == 47
    assert s.port.product_percent[ProductType.equipment] == 49
    assert s.port.name == "Omicron Outpost"
    assert s.port.class_index == 4
    assert not s.port.buy_product[ProductType.fuel_ore]
    assert not s.port.buy_product[ProductType.organics]
    assert s.port.buy_product[ProductType.equipment]

    assert s.explored


def test_fighter_report(extractor: ModExtractor):
    for line in """                 Deployed  Fighter  Scan                   

 Sector    Fighters    Personal/Corp    Mode        Tolls  
===========================================================
    82           1       Personal    Defensive            N/A
   259          1M       Personal    Toll                 0
   506          1T       Personal    Offensive            N/A
                 3 Total                                  0 Total
""".split('\n'):
        extractor.process_line(line)

    s82 = extractor._database.save_sector.call_args_list[0][0][0]  # type: Sector
    assert s82.figs.quantity == 1
    assert s82.figs.fig_type == FighterType.defensive
    assert s82.figs.owner == "yours"
    s259 = extractor._database.save_sector.call_args_list[1][0][0]  # type: Sector
    assert s259.figs.quantity == 1000000
    assert s259.figs.fig_type == FighterType.toll
    assert s259.figs.owner == "yours"
    s506 = extractor._database.save_sector.call_args_list[2][0][0]  # type: Sector
    assert s506.figs.quantity == 1000
    assert s506.figs.fig_type == FighterType.offensive
    assert s506.figs.owner == "yours"


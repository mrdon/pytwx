import pytest

from pytwx.database import Sector, Database, DataHeader

partial_sectors = {
    1: [2, 3, 4, 5, 6, 7],
    2: [1, 3, 7, 9, 10, 32],
    3: [1, 2, 4],
    4: [1, 3, 5, 66],
    5: [1, 4, 6, 36, 51, 71],
    6: [1, 5, 7, 65, 81],
    7: [1, 2, 6, 8, 17, 27],
    8: [2, 7, 48, 84, 86],
    9: [2, 10, 13, 29, 99, 17],
    10: [2, 9, 15],
    27: [7, 73],
    73: [27, 33, 65, 76, 95, 97],
    97: [73, 86]
}


@pytest.fixture
def db():
    sectors = []
    for num in range(1, 101):
        if num in partial_sectors:
            sectors.append(Sector(num, partial_sectors[num]))
        else:
            sectors.append(Sector(num))

    return Database(DataHeader(100), sectors, None, "blah", False, 2002)


def test_sector_save(db: Database):
    s = Sector(1)
    s.warp.append(100)
    db.save_sector(s, 100)
    assert db.sectors[100].warps == 1


def test_plot_courses(db: Database):
    expected = [None for _ in range(101)]
    expected[1] = [1]
    expected[2] = [1, 2]
    expected[3] = [1, 3]
    expected[4] = [1, 4]
    expected[5] = [1, 5]
    expected[6] = [1, 6]
    expected[7] = [1, 7]
    expected[8] = [1, 7, 8]
    expected[9] = [1, 2, 9]
    expected[10] = [1, 2, 10]
    expected[27] = [1, 7, 27]
    expected[73] = [1, 7, 27, 73]
    expected[97] = [1, 7, 27, 73, 97]

    result = db.plot_warp_cources(1)
    for x in range(101):
        if expected[x] is not None:
            assert expected[x] == result[x]


def test_plot_nearest_warps(db: Database):
    nearest = db.plot_nearest_warps(1)
    assert nearest[0:10] == [2, 3, 4, 5, 6, 7, 9, 10, 32, 66]


def test_plot_nearest_warps_with_voids(db: Database):
    db.set_void(7)
    nearest = db.plot_nearest_warps(1)
    assert nearest[0:10] == [2, 3, 4, 5, 6, 9, 10, 32, 66, 36]


def test_plot_course(db: Database):
    assert [1, 7, 27] == db.plot_warp_course(1, 27)


def test_plot_course_with_voids(db: Database):
    db.set_void(7)
    assert [] == db.plot_warp_course(1, 27)
    assert [1, 2, 9, 17] == db.plot_warp_course(1, 17)

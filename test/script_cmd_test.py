import asyncio
import os
from datetime import datetime
from time import sleep
from unittest.mock import Mock

import pytest
from pytest import skip

from pytwx.config import Config
from pytwx.database import Database, Sector, SectorExploredType, SpaceObject, FighterType, Port, ProductType, Trader, \
    Planet, Ship
from pytwx.gui import TwxGui
from pytwx.menu import ModMenu
from pytwx.protocol import TWToUsProtocol
from pytwx.script import ModInterpreter
from pytwx.script_cmd import BuiltinCommands
from pytwx.script_cmp import VarParam, ScriptCmp
from pytwx.script_ref import ScriptRef, ParamKind, CmdAction
from pytwx.tcp import ModServer


@pytest.fixture
def database():
    db = Mock(Database)
    db.db_header = Mock()
    db.db_header.sectors = 1000
    return db


@pytest.fixture
def menu():
    return Mock(ModMenu)


@pytest.fixture
def server():
    return Mock(ModServer)


@pytest.fixture
def client():
    return Mock(TWToUsProtocol)


@pytest.fixture
def script_ref(database, menu, server, client):
    script_ref = ScriptRef(BuiltinCommands(database, menu, server, client))
    return script_ref


@pytest.fixture
def interpreter(script_ref, server: ModServer, tmpdir):
    twxgui = Mock(TwxGui)
    twxmenu = Mock(ModMenu)
    config = Config()
    config.scripts_directory = str(tmpdir)
    interpreter = ModInterpreter(config, server, twxgui, script_ref, twxmenu)
    interpreter.start()
    yield interpreter
    print("\n")
    print("\n".join("> " + c[0][0].strip() for c in server.broadcast.call_args_list))


@pytest.fixture
def echo_cmd(script_ref):
    echo_cmd = Mock(return_value=CmdAction.none)
    script_ref.add_command('ECHO2', 1, -1, echo_cmd, [ParamKind.value], ParamKind.value)
    return echo_cmd


def test_single_statement(tmpdir, echo_cmd, interpreter):
    assert "hi" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, 'echo2 "hi"')


def test_two_statements(tmpdir, echo_cmd, interpreter):
    _run_script(tmpdir, interpreter, 'echo2 "hi"\necho2 "jim"')
    assert echo_cmd.call_count == 2
    assert echo_cmd.call_args_list[0][0][1][0].value == "hi"
    assert echo_cmd.call_args_list[1][0][1][0].value == "jim"


def test_multiple_string_args(tmpdir, echo_cmd, interpreter: ModInterpreter):
    _run_script(tmpdir, interpreter, 'echo2 "hi" "friend"')
    assert echo_cmd.called


def test_if_success(tmpdir, echo_cmd, interpreter):
    assert "sub" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, '''
if (4 <> 5)
  echo2 "sub"
end
''')


def test_if_fail(tmpdir, echo_cmd, interpreter):
    assert "out" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, '''
if (4 <> 4)
  echo2 "sub"
end
echo2 "out"
''')
    assert echo_cmd.call_count == 1


def test_if_with_and(tmpdir, echo_cmd, interpreter):
    assert "sub" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, '''
if (2 <> 1) and (1 <> 2)
  echo2 "sub"
end
''')


def test_var(tmpdir, echo_cmd, interpreter):
    assert "Bob" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, '''
setVar $name "Bob"
echo2 $name
''')


def test_add(tmpdir, echo_cmd, interpreter):
    assert "3.0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, '''
setVar $val (1 + 2)
echo2 $val
''')


def test_add_menu(tmpdir, menu, interpreter):
    _run_script(tmpdir, interpreter, '''
addMenu "" "BuyDown" "BuyDown Settings" "." "" "Main" FALSE
addMenu "BuyDown" "GO" "GO!" "G" :Menu_Go "" TRUE
''')
    assert menu.add_custom_menu.call_count == 2
    assert menu.add_custom_menu.call_args_list[0][0][:7] == ('', 'BUYDOWN', 'BuyDown Settings', '', 'Main', '.', False)
    assert menu.add_custom_menu.call_args_list[1][0][:7] == ('BUYDOWN', 'GO', 'GO!', ':MENU_GO', '', 'G', True)


def test_clear_all_voids(tmpdir, database, interpreter):
    _run_script(tmpdir, interpreter, 'clearAllAvoids')
    assert database.unset_all_voids.called


def test_client_message(tmpdir, server, interpreter):
    _run_script(tmpdir, interpreter, 'clientMessage "blah"')
    assert server.client_message.called
    assert "blah" == server.client_message.call_args_list[0][0][0]


def test_close_menu(tmpdir, menu, interpreter):
    _run_script(tmpdir, interpreter, 'closeMenu')
    assert not menu.close_menu.call_args_list[0][0][0]


def test_cut_lengths(tmpdir, echo_cmd, interpreter):
    _run_script(tmpdir, interpreter, '''
cutLengths "foobar" $BustList 3,3
echo2 $BustList[1] $BustList[2]
''')
    assert "foo" == echo_cmd.call_args_list[0][0][1][0].value
    assert "bar" == echo_cmd.call_args_list[0][0][1][1].value


def test_cut_text(tmpdir, echo_cmd, interpreter):
    assert "foo" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
cutText "blahfooblah" $foo 5 3
echo2 $foo""")


def test_divide(tmpdir, echo_cmd, interpreter):
    assert "5.0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $var 15
divide $var 3
echo2 $var""")


def test_subtract(tmpdir, echo_cmd, interpreter):
    assert "3.0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, '''
setVar $val (5 - 2)
echo2 $val
''')


def test_echo(tmpdir, server, interpreter):
    _run_script(tmpdir, interpreter, '''
echo "foo" "bar"
''')
    assert "foobar" == server.broadcast.call_args_list[1][0][0]


def test_format_currency(tmpdir, echo_cmd, interpreter):
    assert "$4,567.22" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $in 4567.22
format $in $out CURRENCY
echo2 $out""")


def test_format_number(tmpdir, echo_cmd, interpreter):
    assert "4,567.22" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $in 4567.2234
format $in $out NUMBER
echo2 $out""")


def test_format_datetime_to_str(tmpdir, echo_cmd, interpreter):
    assert "02/04/15" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $in "2/4/15"
format $in $out STRTODATETIME
format $out $out2 DATETIMETOSTR
echo2 $out2""")


def test_format_datetime_to_str_with_time(tmpdir, echo_cmd, interpreter):
    assert "02/04/15 11:22:33" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $in "2/4/15 11:22:33"
format $in $out STRTODATETIME
format $out $out2 DATETIMETOSTR
echo2 $out2""")


def test_get_char_code(tmpdir, echo_cmd, interpreter):
    assert "97" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getCharCode "a" $out
echo2 $out""")


def test_get_console_input(tmpdir, interpreter, menu):
    _run_script(tmpdir, interpreter, "getConsoleInput $var TRUE")
    assert menu.begin_script_input.called
    assert menu.begin_script_input.call_args_list[0][0][2] == True


def test_get_course(tmpdir, interpreter, database: Database, server):
    database.plot_warp_course.return_value = [300, 20, 1]
    _run_script(tmpdir, interpreter, """
getCourse $out 1 300
echo $out " " $out[1] " " $out[2] " " $out[3]""")
    assert "2 1 20 300" == server.broadcast.call_args_list[1][0][0]


def test_get_date(tmpdir, echo_cmd, interpreter):
    assert datetime.now().strftime("%x %X") == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getDate  $out
echo2 $out""")


def test_get_distance(tmpdir, echo_cmd, interpreter, database):
    database.plot_warp_course.return_value = [300, 20, 1]
    assert "2" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getDistance $out 1 300
echo2 $out""")


def test_get_input(tmpdir, menu, interpreter):
    _run_script(tmpdir, interpreter, """
getInput $out "Enter some data" """)
    assert "Enter some data" == menu.begin_script_input.call_args[1]["prompt"]


def test_get_length(tmpdir, echo_cmd, interpreter):
    assert "3" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getLength "foo" $out
echo2 $out""")


def test_get_menu_value(tmpdir, interpreter, echo_cmd, menu):
    menu.get_menu_by_name.return_value = VarParam("foo", "bar")
    assert "bar" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
    getMenuValue FOO $out
    echo2 $out""")


def test_get_nearest_warps(tmpdir, echo_cmd, interpreter, database):
    database.plot_nearest_warps.return_value = [3, 5, 10]
    assert [3, 5, 10] == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getNearestWarps $out 1
echo2 $out""")


def test_get_out_text(tmpdir, echo_cmd, interpreter, database):
    assert "" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getOutText $out 
echo2 $out""")


def test_get_rnd(tmpdir, echo_cmd, interpreter, database):
    assert 2 <= int(_run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getRnd $out 2 5
echo2 $out""")) <= 5


def test_get_script_version(tmpdir, echo_cmd, interpreter: ModInterpreter, database):
    _write_script(tmpdir, "script.ts", """
    getScriptVersion script.ts $out 
    echo2 $out""")

    script_path = os.path.join(tmpdir, "script.ts")
    script_cmp_path = os.path.join(tmpdir, "script-cmp.ts")
    compiler: ScriptCmp = ScriptCmp(interpreter.script_ref)
    compiler.compile_from_file(script_path, "blah.txt")
    compiler.write_to_file(script_cmp_path)
    compiler: ScriptCmp = ScriptCmp(interpreter.script_ref)
    compiler.load_from_file(script_cmp_path)

    assert "5" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, f"""
    getScriptVersion "{script_cmp_path}" $out 
    echo2 $out""")


def test_get_sector(tmpdir, echo_cmd, interpreter, database):
    p = Port("Port", 6)
    p.build_time = 44
    p.product_percent = {
        ProductType.fuel_ore: 10,
        ProductType.organics: 20,
        ProductType.equipment: 30
    }
    p.product_amount = {
        ProductType.fuel_ore: 40,
        ProductType.organics: 50,
        ProductType.equipment: 60
    }
    p.update = datetime.now()

    ship = Ship("Serenity")
    ship.ship_type = "Firefly"
    ship.owner = "Malcolm"
    ship.figs = 55

    t = Trader("Malcolm")
    t.figs = 55
    t.ship_type = "Firefly"
    t.ship_name = "Serenity"

    s = Sector(5, [2, 3])
    s.explored = SectorExploredType.density
    s.beacon = "blah"
    s.constellation = "foo"
    s.mines_limpet = SpaceObject(6, 'bar')
    s.mines_armid = SpaceObject(5, 'baz')
    s.figs = SpaceObject(6, 'jim', FighterType.offensive)
    s.density = 7
    s.navhaz = 8
    s.update = datetime.now()
    s.anomaly = True
    s.port = p
    s.traders = [t]
    s.planets = [Planet("Miranda")]
    s.ships = [ship]

    database.load_sector.return_value = s
    database.get_backdoors.return_value = [Sector(88)]

    vars = ((".EXPLORED", "DENSITY"),
            (".INDEX", "5"),
            (".BEACON", "blah"),
            (".CONSTELLATION", "foo"),
            (".ARMIDMINES.QUANTITY", "5"),
            (".LIMPETMINES.QUANTITY", "6"),
            (".ARMIDMINES.OWNER", "baz"),
            (".LIMPETMINES.OWNER", "bar"),
            (".FIGS.QUANTITY", "6"),
            (".FIGS.OWNER", "jim"),
            (".WARPS", "2"),
            (".DENSITY", "7"),
            (".NAVHAZ", "8"),
            (".WARP", "['2', '3']"),
            (".UPDATED", s.update.strftime("%x %X")),
            (".PORT.NAME", "Port"),
            (".FIGS.TYPE", "OFFENSIVE"),
            (".ANOMALY", "YES"),
            (".PORT.CLASS", "6"),
            (".PORT.EXISTS", "1"),
            (".PORT.BUILDTIME", "44"),
            (".PORT.PERC_ORE", str(p.product_percent[ProductType.fuel_ore])),
            (".PORT.PERC_ORG", str(p.product_percent[ProductType.organics])),
            (".PORT.PERC_EQUIP", str(p.product_percent[ProductType.equipment])),
            (".PORT.ORE", str(p.product_amount[ProductType.fuel_ore])),
            (".PORT.ORG", str(p.product_amount[ProductType.organics])),
            (".PORT.EQUIP", str(p.product_amount[ProductType.equipment])),
            (".PORT.UPDATED", p.update.strftime("%x %X")),
            (".PORT.BUY_ORE", "YES"),
            (".PORT.BUY_ORG", "NO"),
            (".PORT.BUY_EQUIP", "NO"),
            (".PLANETS", "1"),
            (".PLANET", "['Miranda']"),
            (".TRADERS", "1"),
            (".TRADER.NAME", "['Malcolm']"),
            (".TRADER.SHIP", "['Firefly']"),
            (".TRADER.SHIPNAME", "['Serenity']"),
            (".TRADER.FIGS", "['55']"),
            (".SHIPS", "1"),
            (".SHIP.OWNER", "['Malcolm']"),
            (".SHIP.SHIP", "['Firefly']"),
            (".SHIP.NAME", "['Serenity']"),
            (".SHIP.FIGS", "['55']"),
            (".BACKDOOR", "['88']")
            )

    _run_script(tmpdir, interpreter, "GetSector 5 $s\n" +
                "\n".join(f"echo2 $s{v[0]}" for v in vars))

    assert echo_cmd.called
    for idx, v in enumerate(vars):
        expected = v[1]
        actual = result_to_string(echo_cmd, idx)
        assert expected == actual


def result_to_string(echo_cmd, idx):
    actual_raw = echo_cmd.call_args_list[idx][0][1][0]
    if isinstance(actual_raw, VarParam) and actual_raw._vars:
        actual = str([x.value for x in actual_raw._vars])
    else:
        actual = actual_raw.value
    return actual


def test_get_sector_parameter(tmpdir, echo_cmd, interpreter, database):
    database.get_sector_var.return_value = "foo"
    assert "foo" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getSectorParameter 5 "bar" $out 
echo2 $out""")


def test_get_text(tmpdir, echo_cmd, interpreter, database):
    assert "copy" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $line "This is a test line to copy text from"
getText $line $value "line to " " text from"
echo2 $value""")


def test_get_time(tmpdir, echo_cmd, interpreter, database):
    now = datetime.now()
    assert _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
getTime $out 
echo2 $out""").startswith(now.strftime("%H:%M:"))


def test_get_word(tmpdir, echo_cmd, interpreter, database):
    assert "line" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $line "This is a line of text"
getWord $line $word 4
echo2 $word""")


def test_get_word_pos(tmpdir, echo_cmd, interpreter, database):
    assert "11" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $line "This is a line of text"
getWordPos $line $pos "line"
echo2 $pos""")


def test_get_word_count(tmpdir, echo_cmd, interpreter, database):
    assert "6" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $line "This is a line of text"
getWordCount $line $out 
echo2 $out""")


def test_gosub(tmpdir, echo_cmd, interpreter, database):
    _run_script(tmpdir, interpreter, """
    echo2 "start"
    gosub :subroutine
    echo2 "end"
    halt

    :subroutine
    echo2 "sub"
    return""")
    assert ["start", "sub", "end"] == [c[0][1][0].value for c in echo_cmd.call_args_list]


def test_goto(tmpdir, echo_cmd, interpreter, database):
    _run_script(tmpdir, interpreter, """
echo2 "start"
goto :otherPart
echo2 "never"

:otherPart
echo2 "end"
halt""")
    assert ["start", "end"] == [c[0][1][0].value for c in echo_cmd.call_args_list]


def test_is_equal(tmpdir, echo_cmd, interpreter, database):
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
isEqual $out "foo" "foo" 
echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
    isEqual $out "foo" "bar" 
    echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        isEqual $out 1.01 1.02
        echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 2
            isEqual $out 1.01 1.02
            echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 1
            isEqual $out 1.01 1.02
            echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            isEqual $out 1 2
            echo2 $out""")


def test_is_greater(tmpdir, echo_cmd, interpreter, database):
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        isGreater $out 1 2
        echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            isGreater $out 2 1
            echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 1
            isGreater $out 1.02 1.01
            echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 2
            isGreater $out 1.02 1.01
            echo2 $out""")


def test_is_greater_equal(tmpdir, echo_cmd, interpreter, database):
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        isGreaterEqual $out 1 2
        echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            isGreaterEqual $out 2 1
            echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
                isGreaterEqual $out 2 2
                echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 1
            isGreaterEqual $out 1.01 1.02
            echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 2
            isGreater $out 1.01 1.02
            echo2 $out""")


def test_is_lesser(tmpdir, echo_cmd, interpreter, database):
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        isLesser $out 1 2
        echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            isLesser $out 2 1
            echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 1
            isLesser $out 1.01 1.02
            echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 2
            isLesser $out 1.01 1.02
            echo2 $out""")


def test_is_lesser_equal(tmpdir, echo_cmd, interpreter, database):
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        isLesserEqual $out 1 2
        echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            isLesserEqual $out 2 1
            echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
                isLesserEqual $out 2 2
                echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 1
            isLesserEqual $out 1.01 1.02
            echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 2
            isLesser $out 1.01 1.02
            echo2 $out""")


def test_is_number(tmpdir, echo_cmd, interpreter, database):
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        isNumber $out 1
        echo2 $out""")
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            isNumber $out 1.1
            echo2 $out""")
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            isNumber $out "blah"
            echo2 $out""")


def test_list_active_scripts(tmpdir, echo_cmd, interpreter, database):
    assert "['script.ts']" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        listActiveScripts $out
        echo2 $out""")


def test_list_avoids(tmpdir, echo_cmd, interpreter, database):
    database.voids = [5, 10]
    assert "['5', '10']" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
listAvoids $out 
echo2 $out""")


def test_list_sector_parameters(tmpdir, echo_cmd, interpreter, database):
    database.list_sector_vars.return_value = ["foo"]
    assert "['foo']" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
listSectorParameters 5 $out 
echo2 $out""")


def test_load(tmpdir, echo_cmd, interpreter, database):
    included_script = _write_script(tmpdir, "script-tmp.ts", 'echo2 "included"')
    _run_script(tmpdir, interpreter, f"""
echo2 "start"
load "{included_script}"
echo2 "end"
""")
    assert ["start", "included", "end"] == [c[0][1][0].value for c in echo_cmd.call_args_list]


def test_load_var(tmpdir, echo_cmd, interpreter, database):
    database.get_var.return_value = "foo"
    assert "foo" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
loadVar $out 
echo2 $out""")


def test_lower_case(tmpdir, echo_cmd, interpreter, database):
    assert "foo" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        setVar $out "Foo"
        lowerCase $out
        echo2 $out""")


def test_merge_text(tmpdir, echo_cmd, interpreter, database):
    assert "foobar" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        mergeText "foo" "bar" $out
        echo2 $out""")


def test_multiply(tmpdir, echo_cmd, interpreter, database):
    assert "15" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        setPrecision 0
        setVar $out 5
        multiply $out 3
        echo2 $out""")
    assert "15.8" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
            setPrecision 1
            setVar $out 5.1
            multiply $out 3.1
            echo2 $out""")


def test_or(tmpdir, echo_cmd, interpreter, database):
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        echo2 "1" or "0"
        """)
    assert "1" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        echo2 "1" or "1"
        """)
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        echo2 "0" or "0"
        """)


def test_pad_left(tmpdir, echo_cmd, interpreter, database):
    assert "   foo" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        setVar $out "foo"
        padLeft $out 3
        echo2 $out""")


def test_pad_right(tmpdir, echo_cmd, interpreter, database):
    assert "foo   " == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        setVar $out "foo"
        padRight $out 3
        echo2 $out""")


def test_process_out(tmpdir, echo_cmd, interpreter: ModInterpreter, client):
    _write_script(tmpdir, "script-tmp.ts", """
setTextOutTrigger "blahname" ":blahlabel" "blah" 
pause
halt

:blahlabel
echo2 "it worked"
    """)

    _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
    echo2 "start"
    load "script-tmp.ts"
    processOut "blah" 
echo2 "done"
""")
    assert ["start", "it worked", "done"] == [c[0][1][0].value for c in echo_cmd.call_args_list]
    assert not client.write.called


def test_replace_text(tmpdir, echo_cmd, interpreter, database):
    assert "thing about yet another thing" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
        setVar $out "thing about another thing"
        replaceText $out "another" "yet another"
        echo2 $out""")


def test_round(tmpdir, echo_cmd, interpreter, database):
    assert "500.5" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setPrecision 2
setVar $x "500.52"
round $x 1
echo2 $x""")


def test_save_var(tmpdir, echo_cmd, interpreter, database):
    _run_script(tmpdir, interpreter, """
        setVar $out "blah"
        saveVar $out""")
    assert database.save_var.called


def test_send(tmpdir, echo_cmd, interpreter, client):
    _run_script(tmpdir, interpreter, """
        send "blah" "jim"
        """)
    assert client.write.called
    client.write.assert_called_with("blahjim")


def test_set_array(tmpdir, echo_cmd, interpreter, database):
    assert "foo" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setArray $out 2
setVar $out[1] "foo"
setVar $out[2] "bar"
echo2 $out[1]""")


def test_set_avoid(tmpdir, echo_cmd, interpreter, database):
    _run_script(tmpdir, interpreter, """
        setAvoid 5""")
    database.set_void.assert_called_with(5)


@pytest.mark.asyncio
async def test_set_delay_trigger(tmpdir, echo_cmd, interpreter, client):
    _run_script(tmpdir, interpreter, """
        setDelayTrigger delay :wait 500
        pause
        halt
        :wait
        echo2 "done"
        """)
    assert not echo_cmd.called
    await asyncio.sleep(1)
    assert echo_cmd.called


def test_set_event_trigger(tmpdir, echo_cmd, interpreter: ModInterpreter, database):
    _run_script(tmpdir, interpreter, """
        setEventTrigger disconnect :disconnected "Connection lost"
        pause 
        halt
        :disconnected
        echo2 "hit"
    """)
    assert not echo_cmd.called
    interpreter.program_event("Connection lost", "", False)
    assert echo_cmd.called


def test_set_sector_parameter(tmpdir, echo_cmd, interpreter, database: Database):
    _run_script(tmpdir, interpreter, """
setSectorParameter 5 myvar "blah" 
""")
    database.set_sector_var.assert_called_with(5, "MYVAR", "blah")


def test_set_text_line_trigger(tmpdir, echo_cmd, interpreter: ModInterpreter, database):
    _run_script(tmpdir, interpreter, """
        setTextLineTrigger blah :blah "blah"
        pause 
        halt
        :blah
        echo2 "hit"
    """)
    assert not echo_cmd.called
    interpreter.activate_triggers()
    interpreter.text_line_event("foo", False)
    assert not echo_cmd.called
    interpreter.activate_triggers()
    interpreter.text_line_event("blah", False)
    assert echo_cmd.called


def test_set_text_trigger(tmpdir, echo_cmd, interpreter: ModInterpreter, database):
    _run_script(tmpdir, interpreter, """
        setTextTrigger blah :blah "blah"
        pause 
        halt
        :blah
        echo2 "hit"
    """)
    assert not echo_cmd.called
    interpreter.activate_triggers()
    interpreter.text_event("foo", False)
    assert not echo_cmd.called
    interpreter.activate_triggers()
    interpreter.text_event("blah", False)
    assert echo_cmd.called


def test_split_text(tmpdir, echo_cmd, interpreter, database):
    assert "foo" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
splitText "foo bar" $out
echo2 $out[1]""")
    assert "foo" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
    splitText "foo,bar" $out ","
    echo2 $out[1]""")


def test_stop(tmpdir, echo_cmd, interpreter: ModInterpreter, database):
    dep_file = _write_script(tmpdir, "script-dep.ts", """
        setTextTrigger blah :blah "blah"
        pause 
        halt
        :blah
        echo2 "hit"
    """)
    interpreter.load(dep_file, False)
    assert 1 == len(interpreter.scripts)
    assert not echo_cmd.called
    _run_script(tmpdir, interpreter, """
    stop "script-dep.ts"
    """)
    assert 0 == len(interpreter.scripts)
    interpreter.activate_triggers()
    interpreter.text_event("blah", False)
    assert not echo_cmd.called


def test_strip_text(tmpdir, echo_cmd, interpreter, database):
    assert "This is value" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $value "This is my value"
stripText $value "my "
echo2 $value
""")


def test_trim(tmpdir, echo_cmd, interpreter, database):
    assert "jim" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $value "  jim "
trim $value
echo2 $value
""")


def test_truncate(tmpdir, echo_cmd, interpreter, database):
    assert "3" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $value 3.2
truncate $value
echo2 $value
""")


def test_uppercase(tmpdir, echo_cmd, interpreter, database):
    assert "BLAH" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $value "blah"
uppercase $value
echo2 $value
""")


def test_wait_for(tmpdir, echo_cmd, interpreter: ModInterpreter, database):
    _run_script(tmpdir, interpreter, """
        waitFor "blah"
        echo2 "hit"
    """)
    assert not echo_cmd.called
    interpreter.activate_triggers()
    interpreter.text_event("foo", False)
    assert not echo_cmd.called
    interpreter.activate_triggers()
    interpreter.text_event("blah", False)
    assert echo_cmd.called


def test_xor(tmpdir, echo_cmd, interpreter, database):
    assert "0" == _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, """
setVar $value "1" xor "1"
echo2 $value
""")


def test_clear_avoid(tmpdir, database, interpreter):
    _run_script(tmpdir, interpreter, 'clearAvoid 5')
    database.unset_void.assert_called_with(5)


def _write_script(tmpdir, name, script):
    script_file = os.path.join(tmpdir, name)
    with open(script_file, "w") as f:
        f.write(script)
    return script_file


def _run_script(tmpdir, interpreter, script):
    script_file = _write_script(tmpdir, "script.ts", script)
    interpreter.load(script_file, False)


def _run_script_with_echo_called(tmpdir, interpreter, echo_cmd, script):
    echo_cmd.reset_mock()
    _run_script(tmpdir, interpreter, script)
    assert echo_cmd.called
    return result_to_string(echo_cmd, 0)

from pytwx.utility import get_parameter


def test_get_parameter_numbers():
    assert get_parameter("234 456", 0) == '234'


def test_get_parameter_numbers_leading_space():
    assert get_parameter(" 312  205  450  479  502  907  958", 0) == '312'
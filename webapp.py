from functools import partial

import aiohttp_jinja2
import jinja2
from aiohttp.web import Application, run_app

from pytwx.session import SessionManager
from pytwx.web import websocket_handler, events_handler, login_handler

app = Application()
aiohttp_jinja2.setup(app,
                     loader=jinja2.FileSystemLoader('templates'))
sessions = SessionManager()
app.router.add_route('GET', '/', partial(login_handler, sessions=sessions))
app.router.add_route('GET', '/{session_id}/telnet', partial(websocket_handler, sessions=sessions))
app.router.add_route('GET', '/{session_id}/events', partial(events_handler, sessions=sessions))
app.router.add_static("/", 'public')
run_app(app, port=8000)

from enum import Enum, auto
from typing import List, Callable, Any, Union

import pytwx.script
from pytwx.protocol import TWToUsProtocol


class CmdAction(Enum):
    none = auto()
    stop = auto()
    pause = auto()
    auth = auto()


class ParamKind(Enum):
    value = auto()
    var = auto()


class ScriptError(Exception):
    pass


class CmdParam:
    """
    CmdParam: Base class for all command parameters processed within a script.  These
    parameters are identified during script compilation and stored in a list within the
    script.  While the compiled script is interpreted these parameters are passed to the
    script command handling functions within the ScriptCmd unit.
    """

    def __init__(self, value='0', is_temporary: bool = False):
        self._str_value: str = None
        self._dec_value: int = None
        self.is_numeric: bool = True
        self.value = value
        self.sig_digits: int = 0
        self._num_changed: bool = False
        self.is_temporary: bool = is_temporary

    def set_bool(self, b: bool):
        if b:
            self._str_value = '1'
            self._dec_value = 1
            self.sig_digits = 0
            self.is_numeric = True
            self._num_changed = False
        else:
            self._str_value = '0'
            self._dec_value = 0
            self.sig_digits = 0
            self.is_numeric = True
            self._num_changed = False

    @property
    def value(self) -> str:
        # EP - if it is a number, then the string value needs to be determined
        if self.is_numeric and self._num_changed:
            # EP: Perhaps this needs to be rounded first
            if self.sig_digits == 0:
                self._str_value = str(int(self._dec_value))
            else:
                self._str_value = str(round(self._dec_value, self.sig_digits))
                self._num_changed = False
        return self._str_value

    @value.setter
    def value(self, val: str):
        self._str_value = val
        self.is_numeric = False

    @property
    def dec_value(self) -> float:
        if not self.is_numeric:
            try:
                self._dec_value = float(self._str_value)
                try:
                    dec_pos = self._str_value.index('.')
                except ValueError:
                    self.sig_digits = 0
                else:
                    self.sig_digits = len(self._str_value) - dec_pos
                self.is_numeric = True
                self._num_changed = False  # EP - meaning FStrValue & FDecValue are sync'd
            except ValueError:
                raise ScriptError(f"{self._str_value} is not a number")

        return self._dec_value

    @dec_value.setter
    def dec_value(self, val: float):
        self._dec_value = val
        self.is_numeric = True
        self._num_changed = True  # EP - Signals a FloatToText conversion when FStrValue is queried


ScriptConstHandler = Callable[[List[str]], str]
ScriptCmdHandler = Callable[['pytwx.script.Script', List[CmdParam]], CmdAction]


class ScriptCmd:
    """
    A built in script command.  Script commands are defined in the ScriptCmd
    unit.  Each command has its own internal parameter processing which updates script
    variables/triggers to allow the scripting language to function.
    """

    def __init__(self, name: str, min_params: int, max_params: int, param_kinds: List[ParamKind],
                 on_cmd: ScriptCmdHandler, def_param_kind: ParamKind):
        self.name: str = name
        self.min_params: int = min_params
        self.max_params: int = max_params
        self.param_kinds: List[ParamKind] = param_kinds
        self.on_cmd: ScriptCmdHandler = on_cmd
        self.def_param_kind: ParamKind = def_param_kind

    def get_param_kind(self, index: int) -> ParamKind:
        if index >= len(self.param_kinds):
            # wasn't specified with command - just give the default
            return self.def_param_kind
        else:
            return self.param_kinds[index]


class ScriptSysConst:
    def __init__(self, name: str, on_read: ScriptConstHandler):
        self.name: str = name
        self.on_read: ScriptConstHandler = on_read

    def read(self, indexes: List[str]) -> str:
        return self.on_read(indexes)


class ScriptRef:
    """
    An object that holds all the references to script consts/cmds
    """

    def __init__(self, script_cmd: 'pytwx.script_cmd.BuiltinCommands'):
        self.cmds: List[ScriptCmd] = []
        self._script_cmd = script_cmd
        # build up the command list
        script_cmd.build_command_list(self)

        self.sys_consts: List[ScriptSysConst] = []

        # build up system constant list
        script_cmd.build_sys_const_list(self)

    def set_interpreter(self, interpreter: 'pytwx.script.ModInterpreter'):
        self._script_cmd._interpreter = interpreter

    def add_command(self, name: str, min_params: int, max_params: int, on_cmd: ScriptCmdHandler, param_kinds: List[ParamKind],
                    def_param_kind: ParamKind = ParamKind.value):
        new_cmd = ScriptCmd(
            name=name,
            min_params=min_params,
            max_params=max_params,
            on_cmd=on_cmd,
            def_param_kind=def_param_kind,
            param_kinds=param_kinds
        )
        self.cmds.append(new_cmd)

    def add_sys_constant(self, name: str, on_read: Union[ScriptConstHandler, str]):
        on_read_func = (lambda _: on_read) if isinstance(on_read, str) else on_read

        self.sys_consts.append(ScriptSysConst(
            name=name,
            on_read=on_read_func
        ))

    def find_cmd(self, name: str) -> int:
        return next((i for i, x in enumerate(self.cmds) if x.name == name), -1)

    def find_sys_const(self, name: str) -> int:
        return next((i for i, x in enumerate(self.sys_consts) if x.name == name), -1)



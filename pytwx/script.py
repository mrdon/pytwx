import os
import struct
import traceback
from datetime import datetime
from enum import Enum, auto
from typing import List, Any, Callable, Dict, Union

from pytwx.ansi import ANSI_7, ANSI_15
from pytwx.config import Config
from pytwx.core import endl, post_notification
from pytwx.gui import MenuItem, TwxGui
from pytwx.menu import ModMenu
from pytwx.observer import Observer, NotificationType
from pytwx.script_cmp import ScriptCmp, VarParam, PARAM_CONST, PARAM_VAR, PARAM_SYSCONST, PARAM_PROGVAR, \
    PARAM_CHAR
from pytwx.script_ref import ScriptRef, CmdParam, ScriptCmd, CmdAction, ScriptError
from pytwx.tcp import ModServer
from pytwx.utility import PeriodicTimer, Handled, Pointer

"""
***************** Notes:

Standard compiled command specification:
  ScriptID:Byte|LineNumber:Word|CmdID:Word|Params|0:Byte

  'Params' is a series of command parameters specified as:
  Type:Byte|Value

  'Type:Byte' can be one of the following:
    - PARAM_VAR : User variable prefix (see below)
    - PARAM_CONST : Compiler string constant prefix
    - PARAM_SYSCONST : Read only system value
    - PARAM_PROGVAR : Program variable
    - PARAM_CHAR : Character code

  'Value' can be one of the following:
    - A 16-bit listed system constant reference (from TModInterpreter.SysConsts[]) type PARAM_SYSCONST
    - A 32-bit listed variable/constant reference (from TScript.Params[]) type PARAM_CONCAT
    - A 32-bit global-listed program variable reference type PARAM_VAR/PARAM_CONST
    - An 8-bit character code type PARAM_CHAR

  If 'Type:Byte' is equal to PARAM_VAR or PARAM_SYSCONST, the full parameter is instead
  specified as:
  PARAM_VAR:Byte|Ref:Word/Integer|IndexCount:Byte

  The IndexCount is the number of values the variable or sysconst has been indexed by.  These
  values are specified using the above methods and can therefore be indexed in the
  same way.  Any variable not indexed must have an IndexCount of zero.

  I.e. compiled indexed variable:

  PARAM_VAR:Byte|VarRef:Integer|1:Byte|PARAM_CONST:Byte|ConstRef:Integer

"""


class Trigger:
    def __init__(self, name: str, value: str = '', label_name: str = '', param: str = None, timer: Union[PeriodicTimer, None] = None):
        self.timer = timer
        self.param = param
        self.label_name = label_name
        self.value = value
        self.name = name


class DelayPeriodicTimer(PeriodicTimer):
    def __init__(self) -> None:
        self.delay_trigger = Trigger()
        super().__init__()


class TriggerType(Enum):
    text = 'Text'
    text_line = 'Text-Line'
    text_out = 'Text-Out'
    delay = 'Delay'
    event = 'Event'


class Script(Observer):
    """
    A physical script in memory.  Controlled by ModInterpreter - do not construct
    from outside ModInterpreter class.
    """
    def __init__(self, owner: 'ModInterpreter', menu: ModMenu):
        self._owner: 'ModInterpreter' = owner
        self._menu: ModMenu = menu
        self.system: bool = False
        self.triggers_active: bool = False
        self.locked: bool = False
        self.wait_for_active: bool = False
        self.wait_text: str = ''
        self._out_text: str = ''
        self.silent: bool = False
        self.decimal_precision: int = 2

        self._code_pos: Pointer = None
        self._window_list: List = []
        self._menu_item_list: List = []
        self._triggers: Dict[TriggerType, List[Trigger]] = {t: [] for t in TriggerType}
        self._waiting_for_auth: bool = False
        self._log_data: bool = True
        self._out_text: str = ''
        self._cmp: ScriptCmp = ScriptCmp(self._owner.script_ref)
        self._exec_script_id: int = 0
        self._sub_stack: List = []
        self._cmd_params: List[CmdParam] = []

    @property
    def log_data(self):
        return self._log_data

    @log_data.setter
    def log_data(self, val: bool):
        self._log_data = val

    @property
    def out_text(self):
        return self._out_text

    @property
    def exec_script(self) -> int:
        return self._exec_script_id

    def get_from_file(self, filename: str, compile: bool):
        if compile:
            self._cmp.compile_from_file(filename, '')
        else:
            self._cmp.load_from_file(filename)

        self._code_pos = self._cmp.code  # always start at the beginning of script

    def execute(self) -> bool:
        """
        :return: True if script was terminated 
        """
        line = 0
        self._exec_script_id = 0
        cmd_action = CmdAction.none
        try:
            # execute code from code_pos
            if self._cmp.code_size == 0 or self._code_pos.pos >= self._cmp.code_size:
                cmd_action = CmdAction.stop
            else:
                while cmd_action is CmdAction.none:
                    # fetch script id
                    self._exec_script_id = self._read_byte(self._code_pos)
                    # fetch the line number
                    line = self._read_word(self._code_pos)
                    # fetch command from code
                    cmd = self._read_word(self._code_pos)
                    # read and execute command
                    cmd_action = self._process_cmd(self._cmp.script_ref.cmds[cmd], self._code_pos)
                    if self._code_pos.pos >= self._cmp.code_size and cmd_action is not CmdAction.pause:
                        cmd_action = CmdAction.stop  # reached end of compiled code

                self._exec_script_id = 0
        except ScriptError as e:
            traceback.print_exc()
            self._owner._server.broadcast(f"{ANSI_15}Script run-time error in '{self._cmp.include_scripts[self._exec_script_id]}': {ANSI_7}{e}, line {line}{endl}")
            cmd_action = CmdAction.stop
        except Exception as e:
            traceback.print_exc()
            self._owner._server.broadcast(
                f"{ANSI_15}Unknown script run-time error in '{self._cmp.include_scripts[self._exec_script_id]}': {ANSI_7}{e}, line {line}{endl}")
            cmd_action = CmdAction.stop

        result = False
        if cmd_action is CmdAction.stop:
            self._self_terminate()
            result = True

        return result

    def dump_vars(self, search_name: str):
        """
        Call all variable dump procedures
        """
        search_name = search_name.upper()
        self._owner._broadcast_important("Variable dump for script", self._cmp.script_file)

        var_params = [param for param in self._cmp.params if isinstance(param, VarParam)]
        var_params.sort()

        for param in var_params:
            if search_name == '' or search_name in param.name.upper():
                param.dump('')

    def dump_triggers(self):
        """
        Dump all triggers
        """

        self._owner._broadcast_important("Trigger dump for script", self._cmp.script_file)

        for trigger_type, trigger_list in self._triggers.items():
            self._owner._server.broadcast(f"{ANSI_15} {trigger_type} Triggers:{ANSI_7}{endl}")

            if trigger_type is TriggerType.delay:
                for t in trigger_list:
                    self._owner._server.broadcast(f"    {t.name} = [{t.label_name}, {t.timer.interval}]{endl}")
            else:
                for t in trigger_list:
                    self._owner._server.broadcast(f"    {t.name} = [{t.label_name}, {t.value}, {t.param}]{endl}")

        if self.wait_for_active:
            self._owner._server.broadcast(f'{ANSI_15}  Waiting for: {ANSI_7}"{self.wait_text}"{endl}')

    def add_menu(self, menu_item: MenuItem):
        """
        add menu item to internal list
        """
        self._menu_item_list.append(menu_item)

    def goto_label(self, l: str):
        # seek label with name l
        error = False

        if len(l) < 2:
            error = True
        elif not l.startswith(':'):
            error = True

        if error:
            raise ScriptError(f"Bad goto label '{l}")

        l = l[1:].upper()
        l = self._cmp.extend_name(l, self._exec_script_id)

        for label in self._cmp.labels:
            if label.name == l:
                self._code_pos.pos = label.location
                return

        raise ScriptError(f"Goto label not found '{l}'")

    def gosub(self, label_name: str):
        self._sub_stack.append(self._code_pos.pos)
        self.goto_label(label_name)

    def do_return(self):
        if not self._sub_stack:
            raise ScriptError("Return without gosub")

        self._code_pos.pos = self._sub_stack.pop()

    def text_line_event(self, text: str, force_trigger: bool) -> bool:
        return self._check_triggers(self._triggers[TriggerType.text_line], text, False, force_trigger, Handled())

    def text_event(self, text: str, force_trigger: bool) -> bool:
        # check waitfor
        if self.wait_for_active:
            if text in self.wait_text:
                self.triggers_active = False
                self.wait_for_active = False
                return self.execute()

        # check through text_triggers for matches with text
        return self._check_triggers(self._triggers[TriggerType.text], text, False, force_trigger, Handled())

    def text_out_event(self, text: str, handled: Handled) -> bool:
        self._out_text = text

        # check through text_out triggers for matches with text
        return self._check_triggers(self._triggers.get(TriggerType.text_out, []), text, True, False, handled)

    def program_event(self, event_name: str, match_text: str, exclusive: bool) -> bool:
        """
        Check through event triggers for matches with text
        """
        result = False
        for i, e in enumerate(list(self._triggers.setdefault(TriggerType.event, []))):
            if e.value == event_name and (
                    (not exclusive and e.param in match_text) or
                    (exclusive and e.param == match_text) or
                    match_text == '' or
                    e.param == ''):

                # remove this trigger and enact it
                result = self.__handle_and_remove_trigger(i, result, e, self._triggers[TriggerType.event])
                break
        return result

    def event_active(self, event_name: str) -> bool:
        """
        Check for events matchignt his event name
        """
        return next((True for t in self._triggers[TriggerType.event] if t.value == event_name), False)

    def set_text_line_trigger(self, name: str, label_name: str, value: str):
        self._triggers[TriggerType.text_line].append(self._create_trigger(name, label_name, value))

    def set_text_out_trigger(self, name: str, label_name: str, value: str):
        self._triggers[TriggerType.text_out].append(self._create_trigger(name, label_name, value))

    def set_text_trigger(self, name: str, label_name: str, value: str):
        self._triggers[TriggerType.text].append(self._create_trigger(name, label_name, value))

    def set_event_trigger(self, name: str, label_name: str, value: str, param: str):
        name = self._cmp.extend_name(name, self._exec_script_id)
        label_name = self._cmp.extend_label_name(label_name, self._exec_script_id)

        if self._trigger_exists(name):
            raise ScriptError("Trigger already exists: '{name}'")

        trigger = Trigger(
            name=name,
            label_name=label_name,
            value=value.upper(),
            param=param,
            timer=None
        )

        if trigger.value == 'TIME HIT':
            self._owner.count_timer_event()

        self._triggers[TriggerType.event].append(trigger)

    def set_delay_trigger(self, name: str, label_name: str, value: int):
        name = self._cmp.extend_name(name, self._exec_script_id)
        label_name = self._cmp.extend_label_name(label_name, self._exec_script_id)

        if self._trigger_exists(name):
            raise ScriptError("Trigger already exists: '{name}'")

        trigger = Trigger(
            name=name,
            label_name=label_name
        )
        timer = PeriodicTimer(delay_trigger=trigger, interval=value, on_timer=self._delay_timer_event, enabled=True)
        trigger.timer = timer
        self._triggers[TriggerType.delay].append(trigger)

    def kill_trigger(self, name: str):
        name = self._cmp.extend_name(name, self._exec_script_id)

        # locate and dispose of this trigger
        for trigger_type, trigger_list in self._triggers.items():
            for i, trigger in enumerate(list(trigger_list)):
                if trigger.name == name:
                    if trigger_type == TriggerType.event and trigger.value == 'TIME HIT':
                        self._owner.uncount_timer_event()

                    del trigger_list[i]
                    return

    def kill_all_triggers(self):
        for trigger_type, trigger_list in self._triggers.items():
            for i, trigger in enumerate(list(trigger_list)):
                if trigger_type == TriggerType.event and trigger.value == 'TIME HIT':
                    self._owner.uncount_timer_event()
                trigger_list.clear()

    def input_completed(self, input_text: str, var_param: VarParam):
        """
        input has just been completed into a menu this script called for
        """
        var_param.value = input_text

        # unlock script and resume execution
        self.locked = False
        self.execute()

    def notify(self, note_type: NotificationType):
        if self._waiting_for_auth:
            if note_type is NotificationType.authentication_done:
                self._waiting_for_auth = False
                self.execute()
            elif note_type is NotificationType.authentication_failed:
                # Looks like we won't be authenticated - self terminate.
                self._waiting_for_auth = False
                self._self_terminate()

    def set_variable(self, var_name: str, value: str, index: str = ''):
        # find a variable with a name that matches VarName and set its value
        # this method exists for compatibility with older scripts only

        var_name = self._cmp.extend_name(var_name, self._exec_script_id)

        for i, param in enumerate(self._cmp.params):
            if isinstance(param, VarParam):
                if param.name == var_name:
                    if index == '':
                        indexes = []
                    else:
                        indexes = [index]
                    param.get_index_var(indexes).value = value

    @property
    def owner(self):
        return self._owner

    def _compare_vars(self, item1: VarParam, item2: VarParam) -> int:
        return item1.__cmp__(item2)

    def _read_byte(self, code_ref: Pointer) -> int:
        return code_ref.read_int(1)

    def _read_char(self, code_ref: Pointer) -> str:
        return chr(code_ref.read_int(2))

    def _read_word(self, code_ref: Pointer) -> int:
        return code_ref.read_int(2)

    def _read_integer(self, code_ref: Pointer) -> int:
        return code_ref.read_int(4)

    def _read_index_values(self, code_ref: Pointer, index_count: int) -> List[str]:
        """
        read an index list from the byte code and into a string list
        """
        result = []
        for i in range(0, index_count):
            param_type = self._read_byte(code_ref)
            if param_type == PARAM_CONST:
                # 32-bit constant reference
                result.append(self._cmp.params[self._read_integer(code_ref)].value)
            elif param_type == PARAM_VAR:
                # 32-bit variable reference - may be indexed
                result.append(self._seek_variable(code_ref).value)
            elif param_type == PARAM_SYSCONST:
                # 16-bit system constant reference - may be indexed
                result.append(self._get_sys_const_value(code_ref))
            elif param_type == PARAM_PROGVAR:
                # 32-bit program variable reference
                pass
            elif param_type == PARAM_CHAR:
                result.append(self._read_char(code_ref))
        return result

    def _seek_variable(self, code_ref: Pointer) -> VarParam:
        """
        construct a variable index list and retrieve the variable from this code reference
        """
        var_ref = self._read_integer(code_ref)
        index_count = self._read_byte(code_ref)
        if index_count == 0:
            return self._cmp.params[var_ref]
        else:
            index_values = self._read_index_values(code_ref, index_count)
            return self._cmp.params[var_ref].get_index_var(index_values)

    def _get_sys_const_value(self, code_ref: Pointer) -> str:
        const_ref = self._read_word(code_ref)
        index_count = self._read_byte(code_ref)
        index_values = self._read_index_values(code_ref, index_count)
        return self._cmp.script_ref.sys_consts[const_ref].read(index_values)

    def _process_cmd(self, cmd: ScriptCmd, cmd_values: Pointer) -> CmdAction:
        """
        Note on passing to script commands:

        All ScriptCmdHandlers accept values as array of type CmdParam.  This allows them to access
        both the values and the references of the object containing them.  Because of this, SysConsts
        and chars must be built up and passed as temporary ConstParams.
        """

        def append_temp_values(value: str):
            # make a temporary param to pass the value
            self._cmd_params.append(CmdParam(value=value, is_temporary=True))

        def append_value(cmd_param: CmdParam):
            self._cmd_params.append(cmd_param)

        result = CmdAction.none

        try:
            while True:
                param_type = self._read_byte(cmd_values)
                if param_type == 0:
                    result = cmd.on_cmd(self, self._cmd_params)
                    break
                elif param_type == PARAM_VAR:
                    # 32-bit variable reference
                    append_value(self._seek_variable(cmd_values))
                elif param_type == PARAM_CONST:
                    # 32-bit constant reference
                    code_int = self._read_integer(cmd_values)
                    append_value(self._cmp.params[code_int])
                elif param_type == PARAM_SYSCONST:
                    # 16-bit system constant reference
                    append_temp_values(self._get_sys_const_value(cmd_values))
                elif param_type == PARAM_CHAR:
                    # 8-bit character code
                    code_byte = self._read_byte(cmd_values)
                    append_temp_values(chr(code_byte))
                elif param_type == PARAM_PROGVAR:
                    # 32-bit program variable reference
                    pass
        finally:
            self._cmd_params = []

        return result

    def _trigger_exists(self, name: str) -> bool:
        """
        check through all trigger lists to see if this trigger name is in use
        """
        result = False
        for trigger_type in TriggerType:
            trigger_list = self._triggers[trigger_type]
            result = next((True for t in trigger_list if t.name == name), False)
            if result:
                break
        return result

    def _check_triggers(self, trigger_list: List[Trigger], text: str, text_out_trigger: bool, force_trigger: bool, handled: Handled) -> bool:
        """
        check through textLineTriggers for matches with Text
        """
        result = False

        if not text_out_trigger and not force_trigger and not self.triggers_active or self.locked:
            return False  # triggers are not enabled or locked in stasis (waiting on menu?)

        for i, trigger in enumerate(list(trigger_list)):
            if text in trigger.value or trigger.value == '':
                # remove this trigger and enact it
                handled.true()
                result = self.__handle_and_remove_trigger(i, result, trigger, trigger_list)

                if result:
                    return True

                break

        return result

    def __handle_and_remove_trigger(self, i, result, trigger, trigger_list):
        result = self.__goto_and_clear_trigger(i, result, trigger, trigger_list)
        if not result:
            self.triggers_active = False

            if self.execute():
                # script was self-terminated
                result = True
        return result

    def __goto_and_clear_trigger(self, i, result, trigger, trigger_list):
        label_name = trigger.label_name
        del trigger_list[i]
        try:
            self.goto_label(label_name)
        except ScriptError as e:
            self._owner._server.broadcast(f"{ANSI_15}Script run-time error (trigger activation): {ANSI_7}{e}{endl}")
            self._self_terminate()
            result = True
        except Exception as e:
            self._owner._server.broadcast(
                f"{ANSI_15}Unknown script run-time error (trigger activation): {ANSI_7}{e}{endl}")
            self._self_terminate()
            result = True
        return result

    def _create_trigger(self, name: str, label_name: str, value: str) -> Trigger:
        name = self._cmp.extend_name(name, self._exec_script_id)
        label_name = self._cmp.extend_label_name(label_name, self._exec_script_id)

        if self._trigger_exists(name):
            raise ScriptError(f"Trigger already exists: '{name}'")

        return Trigger(
            name=name,
            label_name=label_name,
            value=value,
            timer=None
        )

    def _get_program_dir(self) -> str:
        return self._owner.program_dir

    @property
    def script_name(self) -> str:
        return os.path.basename(self._cmp.script_file)

    def _delay_timer_event(self, sender: DelayPeriodicTimer):
        label_name = sender.delay_trigger.label_name
        term = False

        # remove the trigger and its timer
        i = self._triggers[TriggerType.delay].index(sender.delay_trigger)
        term = self.__goto_and_clear_trigger(i, term, sender.delay_trigger, self._triggers[TriggerType.delay])

        if not term:
            self.execute()

    def _free_trigger(self, trigger: Trigger):
        pass

    def _notify_terminate(self):
        self._owner.stop_by_handle(self)

    def _self_terminate(self):
        # Send a notification message to self to start termination
        # Somehow, it seems that sometimes this notification isn't handled before TWX terminates
        post_notification(self._notify_terminate(), None)


class ModInterpreter:
    def __init__(self, config: Config, twx_server: ModServer, twx_gui: TwxGui, script_ref: ScriptRef, menu: ModMenu):
        self._config = config
        self._gui = twx_gui
        self._server = twx_server
        self._menu = menu
        self._script_ref: ScriptRef = script_ref
        self._script_ref.set_interpreter(self)
        self._last_script: str = None
        self.script_menu: MenuItem = None
        self.scripts: List[Script] = []
        self._auto_run: List[str] = []
        self._tmr_time: PeriodicTimer = None
        self._timer_event_count: int = 0
        self._program_dir: str = None
        self.auto_run_text: str = ''

    def start(self):
        """
                this is called when all modules have been fully initialised
                load up our auto run scripts
                """
        for script in self._auto_run:
            self.load(script, False)

    def load(self, filename: str, silent: bool):
        script = Script(self, self._menu)
        script.silent = silent
        self.scripts.append(script)

        self._last_script = filename
        error = True

        filename = os.path.join(self._config.scripts_directory, filename)

        if filename.upper().endswith('.CTS'):
            if not silent:
                self._broadcast_info('Loading script', filename)

            try:
                script.get_from_file(filename, False)
                error = False
            except Exception as e:
                self._broadcast_important("Script load error", filename)
                self.stop(self.count - 1)
        else:
            if not silent:
                self._broadcast_info('Loading and compiling script', filename)

            self._last_script = filename

            try:
                script.get_from_file(filename, True)
                error = False
            except Exception as e:
                self._broadcast_important('Script compilation error', str(e))
                self.stop(self.count - 1)

        if not error:
            self.program_event('SCRIPT LOADED', filename, True)
            self._server.notify_script_load()

            # add menu option for script
            self._gui.add_script_menu(script)

            script.execute()

    def stop(self, index: int):
        # broadcast termination message
        if not self.scripts[index].silent:
            self._broadcast_important("Script terminated", self.scripts[index]._cmp.script_file)

        script = self.scripts[index]
        script_name = script._cmp.script_file

        # remove stop menu option from interface
        self._gui.remove_script_menu(script)

        # remove the script from the list
        del self.scripts[index]

        # trigger program event
        self.program_event('SCRIPT STOPPED', script_name, True)

        self._server.notify_script_stop()

    def stop_by_handle(self, script: Script):
        self.stop(self.scripts.index(script))

    def stop_all(self, stop_sys_scripts: bool):
        """
        terminate all scripts
        """
        for script in (s for s in self.scripts if stop_sys_scripts or not s.system):
            self.stop_by_handle(script)

    def program_event(self, event_name: str, match_text: str, exclusive: bool):
        # trigger all matching program events in active scripts
        event_name = event_name.upper()
        i = 0

        while i < len(self.scripts):
            if not self.scripts[i].program_event(event_name, match_text, exclusive):
                i += 1

    def text_out_event(self, text: str, start_script: Script) -> bool:
        """
        trigger matching text out triggers in active scripts        
        """
        i = 0

        # find starting script
        if start_script is not None:
            while i < len(self.scripts):
                # i += 1
                break

            i += 1

        result = False
        handled = Handled()

        # loop through scripts and trigger off any text out triggers
        while i < len(self.scripts):
            if not self.scripts[i].text_out_event(text, handled):
                i += 1

            if handled:
                result = True
                break

        return result

    def text_event(self, text: str, force_trigger: bool):
        """
        trigger matching text triggers in active scripts
        """
        self._call_scripts_with_retries(lambda script: script.text_event(text, force_trigger))

    def text_line_event(self, text: str, force_trigger: bool):
        """
        trigger matching textline triggers in active scripts
        """
        self._call_scripts_with_retries(lambda script: script.text_line_event(text, force_trigger))

    def event_active(self, event_name: str):
        # check if any scripts hold matching event triggers
        result = False
        i = 0

        while i < len(self.scripts):
            if self.scripts[i].event_active(event_name):
                result = True
                break
            else:
                i += 1
        return result

    def activate_triggers(self):
        """
        All text related triggers are deactivated for the rest of the line after they activate.
        this is to prevent double triggering.
        turn them back on.
        """
        for script in self.scripts:
            script.triggers_active = True

    def dump_vars(self, search_name: str):
        if not search_name:
            self._server.client_message("Dumping all script variables")
        else:
            self._server.client_message(f"Dumping all script variables containing '{search_name}'")

        # dump variables in all scripts
        for script in self.scripts:
            script.dump_vars(search_name)

        self._server.client_message("Variable dump complete")

    def dump_triggers(self):
        # dump all triggers in all scripts
        for script in self.scripts:
            script.dump_triggers()

    def count_timer_event(self):
        self._timer_event_count += 1
        self._tmr_time.enabled = True
        pass

    def uncount_timer_event(self):
        self._timer_event_count -= 1
        if self._timer_event_count >= 0:
            raise Exception('Timer uncount without count')

        if self._timer_event_count <= 0:
            self._tmr_time.enabled = False

    @property
    def count(self) -> int:
        return self._get_count()

    @property
    def last_script(self) -> str:
        return self._last_script

    @property
    def log_data(self) -> bool:
        return self._get_log_data()

    @property
    def script_ref(self) -> ScriptRef:
        return self._script_ref

    @property
    def program_dir(self) -> str:
        return self._get_program_dir()

    @property
    def auto_run(self) -> List[str]:
        return self._get_auto_run()

    def script(self, index: int) -> Script:
        return self._get_script(index)

    def _get_count(self) -> int:
        return len(self.scripts)

    def _get_log_data(self) -> bool:
        """
        Check all scripts for logging settings
        """
        return next((False for s in self.scripts if not s._log_data), True)

    def _get_program_dir(self) -> str:
        return self._program_dir

    def _get_script(self, index: int) -> Script:
        """
        retrieve a script from the scriptlist
        """
        return self.scripts[index]

    def _get_auto_run(self) -> List[str]:
        return self._auto_run

    def _on_time_timer(self, sender: Any):
        self.program_event('Time hit', str(datetime.now()), True)

    def _broadcast_important(self, text: str, param: str):
        self._server.broadcast(f'{endl}{ANSI_15}{text}: {ANSI_7}{param}{endl}{endl}')

    def _broadcast_info(self, text: str, param: str):
        self._server.broadcast(f'{text}: {ANSI_7}{param}')

    def _call_scripts_with_retries(self, script_callable: Callable[[Script], bool]):
        i = 0
        while i < len(self.scripts):
            if not script_callable(self.scripts[i]):
                i += 1





ANSI_0 = chr(27) + '[0m' + chr(27) + '[30m'
ANSI_1 = chr(27) + '[0m' + chr(27) + '[34m'
ANSI_2 = chr(27) + '[0m' + chr(27) + '[32m'
ANSI_3 = chr(27) + '[0m' + chr(27) + '[36m'
ANSI_4 = chr(27) + '[0m' + chr(27) + '[31m'
ANSI_5 = chr(27) + '[0m' + chr(27) + '[35m'
ANSI_6 = chr(27) + '[0m' + chr(27) + '[33m'
ANSI_7 = chr(27) + '[0m' + chr(27) + '[37m'
ANSI_8 = chr(27) + '[301m'
ANSI_9 = chr(27) + '[341m'
ANSI_10 = chr(27) + '[321m'
ANSI_11 = chr(27) + '[361m'
ANSI_12 = chr(27) + '[311m'
ANSI_13 = chr(27) + '[351m'
ANSI_14 = chr(27) + '[331m'
ANSI_15 = chr(27) + '[371m'
ANSI = [
    ANSI_0,
    ANSI_1,
    ANSI_2,
    ANSI_3,
    ANSI_4,
    ANSI_5,
    ANSI_6,
    ANSI_7,
    ANSI_8,
    ANSI_9,
    ANSI_10,
    ANSI_11,
    ANSI_12,
    ANSI_13,
    ANSI_14,
    ANSI_15
]
ANSI_CLEARLINE = chr(27) + '[K'
ANSI_MOVEUP = chr(27) + '[1A'

import asyncio
import logging

import sys
from typing import Callable

log = logging.getLogger(__file__)

BUFFER_SIZE = 65536


class ModServer:

    def __init__(self):
        self.ws_writer: Callable[[bytes],None] = None

    def activate(self, ws_writer: Callable[[bytes],None]):
        self.ws_writer = ws_writer

    def add_buffer(self, text: str):
        pass

    def client_message(self, text: str):
        pass

    def send(self, data: bytes):
        self.ws_writer(data)

    def broadcast(self, text: str):
        pass

    def notify_script_load(self):
        pass

    def notify_script_stop(self):
        pass



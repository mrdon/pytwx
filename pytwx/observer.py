"""
Implements Observer pattern.
"""

from enum import Enum, auto
from typing import List


class NotificationType(Enum):
    authentication_done = auto()
    authentication_failed = auto()


class Observer:
    def notify(self, note_type: NotificationType):
        raise NotImplementedError()


class Subject:

    def __init__(self):
        self._observations: List[Observation] = []

    def notify_observers(self, note_type: NotificationType):
        for observation in list(self._observations):
            observer = observation.observer

            if observation.one_notify_only:
                self.unregister_observer(observer)  # could perhaps be optimized

            observer.notify(note_type)

    def register_observer(self, observer: Observer, one_notify_only: bool = False):
        self._observations.append(Observation(observer=observer, one_notify_only=one_notify_only))

    def unregister_observer(self, observer: Observer):
        observation = next((o for o in self._observations if o.observer == observer), False)
        if observation:
            self._observations.remove(observation)


class Observation:
    def __init__(self, observer: Observer, one_notify_only: bool):
        self.observer: Observer = observer
        self.one_notify_only: bool = one_notify_only



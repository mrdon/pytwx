from datetime import datetime
from enum import auto, Enum
from typing import NamedTuple, List, Union, Set, Dict


class Day(Enum):
    Sun = 1
    Mon = 2
    Tues = 3
    Wed = 4
    Thurs = 5
    Fri = 6
    Sat = 7


class FighterType(Enum):
    toll = auto()
    defensive = auto()
    offensive = auto()
    none = auto()


class SectorExploredType(Enum):
    no = auto()
    calc = auto()
    density = auto()
    holo = auto()


class ProductType(Enum):
    fuel_ore = auto()
    organics = auto()
    equipment = auto()

    @staticmethod
    def names():
        return list(ProductType)


class DataHeader:
    def __init__(self, sectors):
        self.programName: str = ""
        self.sectors: int = sectors
        self.star_dock: int = 0
        self.alpha_centauri: int = 0
        self.rylos: int = 0
        self.address = ""
        self.description = ""
        self.port = 22
        self.login_script = ""
        self.password = ""
        self.login_name = ""
        self.game = ""
        self.use_login = False
        self.rob_factor = .5
        self.steal_factor = .5
        self.last_port_cim = datetime.now()


class SpaceObject:
    def __init__(self, quantity: int = 0, owner: str = '', fig_type: FighterType = None):
        self.quantity = quantity
        self.owner = owner
        self.fig_type = fig_type


class Trader:
    def __init__(self, name: str = ""):
        self.name = name
        self.ship_type: str = ""
        self.ship_name: str = ""
        self.figs: int = 0


class Ship:
    def __init__(self, name: str = ""):
        self.name = name
        self.owner: str = ""
        self.ship_type: str = ""
        self.figs: int = 0


class Port:

    classes = {1: 'BBS', 2: 'BSB', 3: 'SBB', 4: 'SSB', 5: 'SBS', 6: 'BSS', 7: 'SSS', 8: 'BBB'}
    labels = {v: k for k, v in classes.items()}

    def __init__(self, name: str = "", class_index: int = -1):
        self.name = name
        self.dead: bool = False
        self.build_time: int = -1
        self._class_index: int = -1
        self.buy_product: Dict[ProductType, bool] = {}
        self.class_index = class_index
        self.product_percent: Dict[ProductType, float] = {t: -1 for t in ProductType}
        self.product_amount: Dict[ProductType, int] = {t: -1 for t in ProductType}
        self.update: datetime = None

    @property
    def class_index(self):
        if self._class_index == -1 and self.buy_product:
            self._class_index = self.labels["".join(['B' if val else 'S' for val in self.buy_product.values()])]
        return self._class_index

    @class_index.setter
    def class_index(self, index):
        if index in self.classes:
            self.buy_product = {
                ProductType.fuel_ore: self.classes[index][0] == 'B',
                ProductType.organics: self.classes[index][1] == 'B',
                ProductType.equipment: self.classes[index][2] == 'B'
            }
        self._class_index = index


class Sector:
    def __init__(self, id: int, warps: List[int] = None):
        self.id = id
        self.warp: List[int] = warps or []
        self.warps_in: List[int] = set()
        self.port: Port = Port()
        self.navhaz: float = 0
        self.figs: SpaceObject = SpaceObject()
        self.mines_armid: SpaceObject = SpaceObject()
        self.mines_limpet: SpaceObject = SpaceObject()
        self.constellation: str = ''
        self.beacon: str = ''
        self.update: datetime = None
        self.anomaly: bool = False
        self.density: int = -1
        self.warps: int = len(self.warp)
        self.explored: SectorExploredType = SectorExploredType.no
        self.ships: List[Ship] = []
        self.traders: List[Trader] = []
        self.planets: List[Planet] = []
        self.vars: Dict[str, str] = {}


class Planet:
    def __init__(self, name=""):
        self.name: str = name


class WarpIn(NamedTuple):
    origin: int


class SectorVar(NamedTuple):
    name: str
    value: str


class SectorItem(Enum):
    planet = auto()
    trader = auto()
    ship = auto()


class Database:
    def __init__(self, db_header: DataHeader, sectors: List[Sector], last_port_cim: Union[datetime,None], name: str,
                 recording: bool, server_port: int):
        self.server_port = server_port
        self.recording = recording
        self.name = name
        self.last_port_cim = last_port_cim
        self.sectors: Dict[int, Sector] = {s.id: s for s in sectors}
        self.db_header = db_header
        self._void_array: List[int] = [0] * (db_header.sectors + 1)
        self.vars = {}

    @property
    def voids(self):
        return self._void_array

    def save_sector(self, s: Sector, index: int):

        bad = index < 1 or index > self.db_header.sectors
        if not bad:
            bad = any(True for w in s.warp if w > self.db_header.sectors)

        if bad:
            raise Exception(f"Unable to store sector '{index}', closing database")

        sect = self.sectors[index]

        # If this sector has been probed, recall warps if seen before
        if not s.warp and sect.warp:
            s.warp = sect.warp

        # Don't go over density or Anomaly readings unless this is a density scan
        if s.density == -1:
            s.density = sect.density
            s.anomaly = sect.anomaly

        # Don't go over port details unless they are specified
        if not s.port.update:
            s.port.update = sect.port.update

            for product in ProductType:
                s.port.product_amount[product] = sect.port.product_amount[product]
                s.port.product_percent[product] = sect.port.product_percent[product]

        # save stardock details if this sector has it
        if s.port.class_index == 9 and sect.port.class_index != 9:
            self.db_header.star_dock = index
            # todo: save

        # save Alpha Centauri and Rylos details if this sector has them
        if s.port.name == 'Alpha Centauri':
            self.db_header.alpha_centauri = index
            # todo: save
        elif s.port.name == 'Rylos':
            self.db_header.rylos = index
            # todo: save

        # save sector parameters if any exist
        if not s.vars and sect.vars:
            s.vars = sect.vars

        # # -1 is returned when the last sector param is deleted
        # if s.vars == -1:
        #     s.vars = 0

        # update sector warp count
        s.warps = len(s.warp)

        # todo?: Purge old ship, trader and planet data if found
        self.sectors[index] = s
        print(f"Saving sector: {s}")
        # todo: save

        for target in s.warp:
            self.sectors[target].warps_in.add(index)

    def load_sector(self, index: int) -> Sector:
        return self.sectors[index]

    def update_warps(self, sect_index: int):
        s = self.sectors[sect_index]
        s.warps = len(s.warp)
        self.save_sector(s, sect_index)

    def get_sector_items(self, type: SectorItem, sector: Sector) -> Union[List[Planet], List[Trader], List[Ship]]:
        # fixme: needed?
        if type == SectorItem.planet:
            return sector.planets
        elif type == SectorItem.ship:
            return sector.ships
        elif type == SectorItem.trader:
            return sector.traders

    def get_warps_in(self, sector: int) -> Set[int]:
        return self.sectors[sector].warps_in

    def get_backdoors(self, sector: Sector) -> List[Sector]:
        # fixme: needed?
        return [self.sectors[x] for x in sector.warps_in]

    def plot_warp_course(self, from_sect: int, to_sector: int) -> List[int]:
        return self._find_shortest_path(from_sect, to_sector)

    def plot_warp_cources(self, from_sect: int):
        # fixme: this works, but is super slow
        result: List[List] = [[] for _ in range(self.db_header.sectors + 1)]
        for to_sector in range(1, self.db_header.sectors):
            result[to_sector] = self.plot_warp_course(from_sect, to_sector)
        return result

    def set_void(self, sector_index: int):
        self._void_array[sector_index] = 1

    def unset_void(self, sector_index: int):
        self._void_array[sector_index] = 0

    def unset_all_voids(self):
        for x in range(len(self._void_array)):
            self._void_array[x] = 0

    def _find_shortest_path(self, start, end, path=None):
        if path is None:
            path = []

        path = path + [start]
        if start == end:
            return path
        if not start in self.sectors:
            return None
        shortest = None
        for node in self.sectors[start].warp:
            if node not in path and not self._void_array[node]:
                newpath = self._find_shortest_path(node, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest or []

    def set_sector_var(self, sector_index: int, name: str, value: str):
        sector = self.sectors[sector_index]
        if not value:
            del sector.vars[name]
        else:
            sector.vars[name] = value

    def get_sector_var(self, sector_index: int, name: str) -> str:
        sector = self.sectors[sector_index]
        return sector.vars.get(name, '')

    def list_sector_vars(self, sector_index: int) -> List[str]:
        sector = self.sectors[sector_index]
        return list(sector.vars.keys())

    def plot_nearest_warps(self, start_sect):
        if start_sect not in self.sectors:
            return None

        visited, queue = [], [start_sect]
        while queue:
            cur = queue.pop(0)
            if cur not in visited and not self._void_array[cur]:
                visited.append(cur)
                queue.extend(self.sectors[cur].warp)
        return visited[1:]

    def get_var(self, name, default):
        return self.vars.get(name, default)

    def save_var(self, name: str, value: str):
        self.vars[name] = value


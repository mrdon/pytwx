from typing import Dict, Callable
from uuid import uuid4

from pytwx.config import Config
from pytwx.database import Database, DataHeader, Sector
from pytwx.gui import TwxGui
from pytwx.menu import ModMenu
from pytwx.script import ModInterpreter
from pytwx.script_cmd import BuiltinCommands
from pytwx.script_ref import ScriptRef
from pytwx.tcp import ModServer


class AutoLogin:
    def __init__(self, bbs_username: str, password: str):
        self.bbs_username = bbs_username
        self.password = password


class Session:
    def __init__(self, host: str, port: int, auto_login: AutoLogin, server: ModServer,
                 database: Database, gui: TwxGui, interpreter: ModInterpreter):
        self.id = str(uuid4())
        self.host = host
        self.port = port
        self.auto_login = auto_login
        self.database = database
        self.gui = gui
        self.server = server
        self.interpreter = interpreter

    def activate(self, writer: Callable[[bytes], None]):
        self.server.activate(writer)


class SessionManager:

    def __init__(self):
        self.sessions: Dict[str, Session] = {}

    def create(self, host: str, port: int, num_sectors: int, game: str, auto_login: AutoLogin) -> Session:
        gui: TwxGui = TwxGui()
        config = Config()
        print(f"creating server at {host}:{port}")
        database: Database = Database(db_header=DataHeader(num_sectors),
                            sectors=[Sector(id) for id in range(num_sectors + 1)],
                            last_port_cim=None,
                            name=game,
                            recording=False,
                            server_port=port)
        server = ModServer()
        menu = ModMenu()
        scripts: ScriptRef = ScriptRef(BuiltinCommands(database, menu, server))
        interpreter: ModInterpreter = ModInterpreter(config, server, gui, scripts, menu)
        session = Session(
            host=host,
            port=port,
            auto_login=auto_login,
            database=database,
            gui=gui,
            server=server,
            interpreter=interpreter
        )
        self.sessions[session.id] = session
        return session

    def get(self, id: str):
        return self.sessions.get(id)

    def ids(self):
        return self.sessions.keys()
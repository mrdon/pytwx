import asyncio

import logging
import re
from traceback import print_exc

from pytwx.process import ModExtractor

log = logging.getLogger(__file__)


class ClientProtocol(asyncio.StreamReaderProtocol):
    _transport = None

    def __init__(self, stream_reader, stream_writer=None, on_status=None, client_connected_cb=None,
                 loop=None):
        super().__init__(stream_reader, client_connected_cb, loop)
        if stream_writer:
            self._stream_writer = stream_writer
        self.on_status = on_status

    def data_received(self, data):
        super().data_received(data)
        log.debug("received: {}".format(data))

    def connection_made(self, transport):
        super().connection_made(transport)
        self._transport = transport
        self._stream_writer = asyncio.StreamWriter(transport, self, self._stream_reader, asyncio.get_event_loop())

    def connection_lost(self, exc):
        super().connection_lost(exc)
        self._transport = None

    def close(self):
        if self._transport:
            self._transport.close()
        else:
            log.warning("No transport to close")


ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -\\/]*[@-~]')


class TWToUsProtocol(asyncio.StreamReaderProtocol):
    _transport: asyncio.Transport = None

    def __init__(self, extractor: ModExtractor, stream_reader, stream_writer=None, on_status=None, client_connected_cb=None,
                 loop=None):
        super().__init__(stream_reader, client_connected_cb, loop)
        self.extractor = extractor
        if stream_writer:
            self._stream_writer = stream_writer
        self.on_status = on_status
        self._current_line: str = ''
        self._current_ansi_line: str = ''
        self._raw_ansi_line: str = ""

    def data_received(self, data: bytes):
        super().data_received(data)

        in_data = data.decode('ISO-8859-1')
        log.debug("received: {}".format(in_data))
        s = in_data
        self._raw_ansi_line = in_data

        # Remove null chars
        s = s.replace(chr(0), '')

        ansi_s = s

        # Strip the ANSI and remove bells
        s = ansi_escape.sub('', s).replace(chr(7), '')

        # todo: log ansi and stripped

        # Remove linefeed
        s = s.replace(chr(10), '')
        ansi_s = ansi_s.replace(chr(10), '')

        # Form and process lines out of data
        i = 0
        line = self._current_line + s
        ansi_line = self._current_ansi_line + ansi_s
        while i < len(line):
            if line[i] == chr(13):
                # find the matching carriage return in the ansi line
                x = 0
                if len(ansi_line) > 0:
                    while ansi_line[x] != chr(13) and x < len(ansi_line):
                        x += 1
                self._current_line = line[0:i]
                self._current_ansi_line = ansi_line[0:x]
                print('[{}] {}'.format(self.extractor._current_display, self._current_line))
                try:
                    self.extractor.process_line(self._current_line)
                except Exception as e:
                    self.extractor.reset()
                    print_exc()

                if i < len(line):
                    line = line[i + 1:]
                    ansi_line = ansi_line[x + 1:]
                else:
                    line = ''
                    ansi_line = ''
                    break
                i = -1

            i += 1

        # Process what we have left
        self._current_line = line
        self._current_ansi_line = ansi_line
        self.extractor.process_prompt(self._current_line)

    def connection_made(self, transport):
        super().connection_made(transport)
        self._transport = transport
        self._stream_writer = asyncio.StreamWriter(transport, self, self._stream_reader, asyncio.get_event_loop())

    def connection_lost(self, exc):
        super().connection_lost(exc)
        self._transport = None

    def close(self):
        if self._transport:
            self._transport.close()
        else:
            log.warning("No transport to close")

    def write(self, data):
        self._transport.write(data)

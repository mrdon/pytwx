import asyncio
import logging
import os

log = logging.getLogger("main")

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("asyncio").setLevel(logging.WARN)
    if os.name == 'nt':
        main_loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(main_loop)
    else:
        main_loop = asyncio.get_event_loop()

    main_loop.run_until_complete(_populate_game_statuses())
    main_loop.run_until_complete(start_proxy(get_game))

    try:
        from web import start_web
        main_loop.run_until_complete(start_web(games))
    except ImportError:
        log.warn("aiohttp not detected, so no web interface will be available")

    try:
        main_loop.run_forever()
    except KeyboardInterrupt:
        pass
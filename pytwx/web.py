import asyncio
import json
from traceback import print_tb, print_exc

import aiohttp
from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import Response
from aiohttp_sse import sse_response

from pytwx.gui import TwxEvent
from pytwx.process import ModExtractor
from pytwx.protocol import TWToUsProtocol
from pytwx.session import SessionManager, AutoLogin
from pytwx.tcp import ModServer
from pytwx.vendor import aiohttp_jinja2


@aiohttp_jinja2.template('w2ui/index.html.jinja2')
def login_handler(request: Request, sessions: SessionManager):
    if 'host' in request.query:
        auto_login = None
        if "true" == request.query.get('auto_login'):
            auto_login = AutoLogin(request.query['bbs_username'],
                                   request.query['password'])

        session = sessions.create(
            host=request.query['host'],
            port=request.query['port'],
            num_sectors=int(request.query['sectors']),
            game=request.query['game'],
            auto_login=auto_login
        )
        print("created session: {}".format(session.id))
        return {"sessionId": session.id}
        # todo: render templates with session.id
        # todo: fix ftelnet to accept a path so we can inject the session id
    else:
        # todo: show game create dialog
        session = sessions.create(
            host='localhost',
            port=2002,
            num_sectors=1000,
            game='a',
            auto_login=None
        )
        print("created session: {}".format(session.id))
        return {"sessionId": session.id}


async def events_handler(request, sessions: SessionManager):
    session_id = request.match_info['session_id']
    session = sessions.get(session_id)
    print(f"requested {session_id} but only had {sessions.ids()}")
    if not session:
        return Response(status=404)
    resp = await sse_response(request)
    resp.send(retry=100, data='')

    try:
        while True:
            event: TwxEvent = await session.gui.events.get()
            data = json.dumps(event.data)
            print("sending {}: {}".format(event.name, data))
            resp.send(event=event.name, data=data, id=hash(data))
            session.gui.events.task_done()
    except Exception as e:
        print_exc()
        resp.stop_streaming()
        return resp


async def websocket_handler(request, sessions: SessionManager):
    session_id = request.match_info['session_id']
    session = sessions.get(session_id)
    reader = asyncio.StreamReader(limit=2 ** 16)
    loop = asyncio.get_event_loop()

    extractor = ModExtractor(gui=session.gui,
                             database=session.database,
                             interpreter=session.interpreter)
    protocol = TWToUsProtocol(extractor, reader, loop=loop)
    transport, _ = await loop.create_connection(
        protocol_factory=lambda: protocol,
        host=session.host,
        port=session.port)
    ws = web.WebSocketResponse(protocols={"binary"})
    session.activate(lambda b: ws.send_bytes(b))
    await ws.prepare(request)
    async def tw_to_ws():
        async def read():
            while True:
                b = await reader.read(1024)
                if not b:
                    await ws.close()
                    break
                ws.send_bytes(b)
        asyncio.Task(read())

    asyncio.Task(tw_to_ws())
    async for msg in ws:
        if msg.tp == aiohttp.WSMsgType.text:
            protocol.write(msg.data.encode('utf8'))
        if msg.tp == aiohttp.WSMsgType.binary:
            protocol.write(msg.data)
        elif msg.tp == aiohttp.WSMsgType.error:
            print('ws connection closed with exception %s' %
                  ws.exception())

    print('websocket connection closed')

    return ws
# TWX Proxy 2.02 is version 1
# TWX Proxy 2.03Beta is version 2?
# TWX Proxy 2.03Final is version 3
# TWX Proxy 2.04 is version 4
# TWX Proxy 2.05 is version 5
import os
import struct
import traceback
from io import FileIO, SEEK_CUR
from typing import List, Union, Tuple, Any

from pytwx.ansi import ANSI_15, ANSI_7
from pytwx.core import endl
from pytwx.script_ref import CmdParam, ScriptRef, ParamKind, ScriptError
from pytwx.tcp import ModServer
from pytwx.utility import fetch_script, read_string, Pointer

COMPILED_SCRIPT_VERSION = 5

PARAM_CMD = 0
PARAM_VAR = 1  # User variable prefix
PARAM_CONST = 2  # Compiler string constant prefix
PARAM_SYSCONST = 3  # Read only system value
PARAM_PROGVAR = 4  # Program variable
PARAM_CHAR = 5  # Character code

OP_GREATEREQUAL = chr(230)
OP_LESSEREQUAL = chr(231)
OP_AND = chr(232)
OP_OR = chr(233)
OP_XOR = chr(234)
OP_NOT = chr(235)
OP_NOTEQUAL = chr(236)
OP_NONE = chr(0)


class ScriptFileHeader:
    """
    header at top of compiled script file
    """
    def __init__(self, program_name: bytes, version: int, desc_size: int, code_size: int):
        self.program_name = program_name.decode('ascii')
        self.version = version
        self.desc_size = desc_size
        self.code_size = code_size

    def to_bytes(self):
        return struct.pack('<10sHHi', self.program_name.encode('ascii'), self.version, self.code_size, self.desc_size)

    @staticmethod
    def from_bytes(file_reader: FileIO) -> 'ScriptFileHeader':
        return ScriptFileHeader(*struct.unpack('<10sHHi', file_reader.read(18)))


class ConditionStruct:
    def __init__(self, con_label: str, end_label: str, is_while: bool, at_else: bool):
        self.is_while = is_while
        self.at_else = at_else
        self.end_label = end_label
        self.con_label = con_label


class VarParam(CmdParam):
    """
    A variable within the script.  Typically referenced by its ID.  Can contain
    a list of indexed values in the event of it being used as an array within the script.
    """
    def __init__(self, name: str = None, value: str = None):
        super().__init__()
        if name is not None:
            self.name: str = name
        if value is not None:
            self.value = value
        # list of variables indexed within this variable
        self._vars: List[VarParam] = []
        # static array size
        self.array_size: int = 0

    def add_var(self, new_var: 'VarParam') -> int:
        self._vars.append(new_var)
        return len(self._vars) - 1

    def dump(self, twxserver: ModServer, tab: str):
        # broadcast variable details to activate telnet connections
        if len(self.name) >= 2:
            if self.name.startswith('$$'):
                return  # don't dump system variables

        twxserver.add_buffer(f'{tab}{ANSI_15}"{ANSI_7}{self.name}{ANSI_15}" = "{ANSI_7}{self.value}{ANSI_15}"{endl}')

        if self._vars:
            # dump array contents
            if self.array_size:
                twxserver.add_buffer(f'{tab}{ANSI_15}Static array of "{self.name}" (size {len(self._vars)}){endl}')
            else:
                twxserver.add_buffer(f'{tab}{ANSI_15}Dynamic array of "{self.name}" (size {len(self._vars)}){endl}')

            for var in self._vars:
                var.dump(twxserver, f'{tab} ')

    def get_index_var(self, indexes: List[str]) -> 'VarParam':
        # move through the array of index dimensions and return a reference to the
        # variable with the specified name/index.
        if not indexes:
            return self

        next_index = []
        if len(indexes) > 1:
            next_index = indexes[1:]

        # search the index for a variable with a matching name
        if self.array_size:
            # static array - we can look up the variable directly
            i = 0 if not indexes[0].isnumeric() else int(indexes[0])
            if i < 1 or i > self.array_size:
                raise ScriptError(f"Static array index '{indexes[0]}' is out of range (must be 1-'{self.array_size}')")
            else:
                return self._vars[i - 1].get_index_var(next_index)
        else:
            result = next((v.get_index_var(next_index) for v in self._vars if v.name == indexes[0]), None)
            if result is None:
                # variable not found in index - make a new one
                new_var = VarParam(indexes[0])
                self.add_var(new_var)
                result = new_var.get_index_var(next_index)
            return result

    def set_array(self, dimensions: List[int]):
        self._trim_array(dimensions[0])
        # build variables up until size limit
        for i in range(0, dimensions[0]):
            # See if an Item already exists
            if i + 1 > len(self._vars):
                self.add_var(VarParam(str(i + 1)))
                # NewVar is now both Vars.Count - 1, and Vars.Items[I]
            else:
                # Okay, we are dealing with an existing TVarParam.
                # We need to clear it, then check if it has a sub array
                self._vars[i].name = str(i + 1)
                self._vars[i].value = '0'

            if len(dimensions) > 1 or len(self._vars[i]._vars):
                # We have sub-dimension, either currently existing, newly specified, or both
                if len(dimensions) == 1:
                    # We need to clear sub-dimensions, so pass a Dimension[0] of 0
                    next_dimen = [0]
                else:
                    next_dimen = dimensions[1:]

                # NextDimen array is now set.  Begin Recursive Call
                self._vars[i].set_array(next_dimen)

    def _trim_array(self, new_size):
        self.array_size = new_size
        # First, let's delete any existing Vars.Items above FArraySize
        # This also deletes any of their sub-lists of TVarParams
        for i in range(len(self._vars) - 1, self.array_size, -1):
            del self._vars[i]

    def set_array_from_strings(self, strings: List[str]):
        self._trim_array(len(strings))
        for i in range(0, self.array_size):
            if i + 1 > len(self._vars):
                self.add_var(VarParam(str(i + 1), strings[i]))
                # NewVar is now both Vars.Count - 1, and Vars.Items[I]
            else:
                self._vars[i].name = str(i + 1)
                self._vars[i].value = strings[i]
                if self._vars[i]._vars:
                    self._vars[i].set_array([0])

    def set_array_from_list(self, lst: List):
        self._trim_array(len(lst))
        for i in range(0, self.array_size):
            if i + 1 > len(self._vars):
                self.add_var(VarParam(str(i + 1), str(lst[i])))
            else:
                self._vars[i].name = str(i + 1)
                self._vars[i].value = str(lst[i])
                if self._vars[i]:
                    self._vars[i].set_array([0])

    def set_multi_arrays_from_lists(self, list_array: List[List]):
        self._trim_array(len(list_array))
        for i in range(0, self.array_size):
            if i + 1 > len(self._vars):
                self.add_var(VarParam(str(i + 1), str(len(list_array[i]) - 1)))
            else:
                self._vars[i].name = str(i + 1)
                self._vars[i].value = str(len(list_array[i]) - 1)
            self._vars[i].set_array_from_list(list_array[i])


class ScriptLabel:
    def __init__(self, name, location):
        self.name: str = name
        self.location: int = location


class ScriptCmp:
    def __init__(self, script_ref: ScriptRef):
        self._stack: List[ConditionStruct] = []
        self.params: List[CmdParam] = []
        self.labels: List[ScriptLabel] = []
        self.include_scripts: List[str] = []
        self._description: List[str] = []
        self._script_file: str = None
        self._code: Pointer = Pointer()
        self._label_count: int = 0
        self._sys_var_count: int = 0
        self._wait_on_count: int = 0
        self._line_count: int = 0
        self._cmd_count: int = 0
        self._code_size: int = 0
        self._script_ref: ScriptRef = script_ref

    def compile_from_file(self, filename: str, desc_file: str):
        # compile this file into byte code

        self._script_file = filename
        with open(filename) as f:
            script_text = f.read().splitlines()

        self._label_count = 0
        self._wait_on_count = 0
        self._compile_from_strings(script_text, os.path.basename(filename))

    def add_param(self, param: CmdParam):
        pass

    def load_from_file(self, filename: str):
        self._script_file = filename

        if self._code_size:
            raise ScriptError('Code already exists - cannot load from file')

        with open(filename, 'rb') as f:

            # read header
            hdr = ScriptFileHeader.from_bytes(f)

            # check header
            if hdr.program_name != 'TWX SCRIPT':
                raise ScriptError("File is not a compiled TWX script")
            # This version is backwards compatible with the v3 compiler
            elif hdr.version < 2 or hdr.version > COMPILED_SCRIPT_VERSION:
                raise ScriptError('Script file is an incorrect version')

            if hdr.desc_size:
                f.seek(hdr.desc_size, SEEK_CUR)

            # read code
            self._code = f.read(hdr.code_size)
            self._code_size = hdr.code_size

            # read cmdParams
            param_type = struct.unpack('<b', f.read(1))[0]

            while param_type > 0:
                if param_type == 1:
                    # CmdParam
                    val = read_string(f)
                    self.params.append(CmdParam(self._apply_encryption(val, 113)))
                else:
                    # VarParam (2)
                    val = read_string(f)
                    param = VarParam(value=self._apply_encryption(val, 113))
                    param.name = read_string(f)
                    self.params.append(param)
                param_type = struct.unpack('<b', f.read(1))[0]

            # read include script names
            length = struct.unpack('<i', f.read(4))[0]

            while length:
                val = f.read(length)
                # f.read(1)
                self.include_scripts.append(val)
                b = f.read(4)
                length = struct.unpack('<i', b)[0]

            # read labels
            while True:
                b = f.read(4)
                if not b:
                    break
                location = struct.unpack('<i', b)[0]

                name = read_string(f)
                self.labels.append(ScriptLabel(name, location))

    def write_to_file(self, filename: str):
        # write script to a file to be loaded from later

        with open(filename, 'wb') as f:
            desc = "\n".join(self._description)
            f.write(ScriptFileHeader(
                program_name='TWX SCRIPT'.encode('ascii'),
                version=COMPILED_SCRIPT_VERSION,
                code_size=self.code_size,
                desc_size=len(desc)
            ).to_bytes())

            if self._description:
                # write description to file
                f.write(struct.pack(f'<{len(desc)}s', desc.encode('ascii')))

            # write code
            f.write(self._code.buffer[0:self._code_size])

            # write cmd_params
            for i, param in enumerate(self.params):
                val = self._apply_encryption(param.value, 113)

                if isinstance(param, VarParam):
                    param_type = 2  # VarParam is parameter type 2
                    f.write(struct.pack('<b', param_type))
                    f.write(struct.pack('<i', len(param.value)))
                    f.write(struct.pack(f'<{len(val)}s', val.encode('ascii')))
                    val = self._apply_encryption(param.name, 113)
                    f.write(struct.pack('<i', len(param.name)))
                    f.write(struct.pack(f'<{len(val)}s', val.encode('ascii')))
                else:  # CmdParam
                    param_type = 1  # CmdParam is parameter type 2
                    f.write(struct.pack('<b', param_type))
                    f.write(struct.pack('<i', len(val)))
                    f.write(struct.pack(f'<{len(val)}s', val.encode('ascii')))

            # terminate cmdParams with null value
            param_type = 0
            f.write(struct.pack('<b', param_type))

            # write include script names
            for val in self.include_scripts:
                f.write(struct.pack('<i', len(val)))
                f.write(struct.pack(f'<{len(val)}s', val.encode('ascii')))

            # terminal include script names with null length
            f.write(struct.pack('<i', 0))

            # write labels
            for label in self.labels:
                pos = label.location
                f.write(struct.pack('<i', pos))
                f.write(struct.pack('<i', len(label.name)))
                f.write(struct.pack(f'<{len(label.name)}s', label.name.encode('ascii')))

    def extend_name(self, name: str, script_id: int) -> str:
        if '~' not in name:
            if script_id:
                return self.include_scripts[script_id] + '~' + name
        else:
            if name.startswith('~'):
                if len(name) == 1:
                    raise ScriptError("Bad name")

                return name[1:]
        return name

    def extend_label_name(self, name: str, script_id: int) -> str:
        if '~' not in name and script_id:
            return f':{self.include_scripts[script_id]}~{name[1:]}'
        else:
            return name

    @property
    def param_count(self) -> int:
        return len(self.params)

    @property
    def label_count(self) -> int:
        return len(self.labels)

    @property
    def code(self) -> Pointer:
        return self._code

    @property
    def code_size(self) -> int:
        return self._code_size

    @property
    def line_count(self) -> int:
        return self._line_count

    @property
    def cmd_count(self) -> int:
        return self._cmd_count

    @property
    def script_ref(self) -> ScriptRef:
        return self._script_ref

    @property
    def script_file(self) -> str:
        return self._script_file

    def _apply_encryption(self, value: str, key: int) -> str:
        return "".join(chr(x ^ key) for x in value.encode('ascii'))

    def _append_code(self, new_code: bytearray):
        # write this data to the end of the byte-code
        self._code.buffer.extend(new_code)
        self._code_size += len(new_code)

    def _build_label(self, name: str, location: int):
        self.labels.append(ScriptLabel(name, location))

    def _identify_param(self, param_name: str) -> int:
        if param_name.startswith('$'):
            return PARAM_VAR
        elif param_name.startswith('%'):
            return PARAM_CHAR
        else:
            result = PARAM_CONST

            # remove indexes from constant name (if it is a constant)
            index_level = 0
            const_name = ''
            for c in param_name:
                if c == '[':
                    index_level += 1
                elif c == ']':
                    index_level -= 1
                elif index_level == 0:
                    const_name += c

            # check for system constant
            if self.script_ref.find_sys_const(const_name) > -1:
                result = PARAM_SYSCONST

            return result

    def _write_code(self, cmd_code: bytearray, code: Union[Pointer, int], code_length: int):
        if isinstance(code, int):
            if code_length == 4:
                ptn = "i"
            elif code_length == 2:
                ptn = "H"
            elif code_length == 1:
                ptn = "B"
            else:
                raise ValueError()
            s = struct.pack(ptn, code)
        elif isinstance(code, Pointer):
            s = code.buffer[code.pos:code.pos+code_length]
        else:
            raise ValueError()
        cmd_code.extend(s)

    def _compile_value(self, value: str, cmd_code: bytearray, param_kind: ParamKind, line: int, script_id: int):
        """
        :param value: can be a variable name, a constant, a system constant, a program variable or a character
        """
        def quotation_error():
            raise ScriptError('Quotation syntax error')

        def param_type_error():
            raise ScriptError(f"Invalid command parameter type '{value}")

        def build_index_list(index_list: List[str], name: str) -> str:
            retn_name = ''
            index_depth = 0
            index = ''
            # EP - This next routine is to verify that array variables ($array[$var[2]]) are formatted correctly
            for i, c in enumerate(name):
                if c == '[':
                    if index_depth:
                        index += c
                    index_depth += 1
                elif c == ']':
                    index_depth -= 1
                    if index_depth > 0:
                        index += c
                    elif index_depth < 0:
                        raise ScriptError("Array syntax error")
                    if index_depth == 0:
                        if index == '':
                            raise ScriptError("Expected array index specifier")
                        index_list.append(index)
                        index = ''
                elif index_depth == 0:
                    retn_name += c
                else:
                    index += value[i]

            if index_depth > 0:
                raise ScriptError("Array syntax error")

            return retn_name

        def write_index_list(index_list: List[str]):
            if len(index_list) > 255:
                raise ScriptError("Too many array dimensions")

            code_byte = len(index_list)
            self._write_code(cmd_code, code_byte, 1)  # write index count

            # loop through index list and process each index value as a separate parameter
            for s in index_list:
                self._compile_param(s, cmd_code, ParamKind.value, line, script_id)

        type = 0
        if '"' in value:
            # constant - remove the quotes
            # EP - meaning we have a line, setVar $var "one".  "one" is a constant
            if not value.startswith('"') or not value.endswith('"'):
                quotation_error()

            value = value[1:-1].replace('*', chr(13))
            # EP - In the above example, Value would now contain 'one', without the quotes

            if '"' in value:
                quotation_error()

            type = PARAM_CONST
        else:
            # EP - See if it's a $var, %progVar,  #char, [const], or [sysConst]
            type = self._identify_param(value)

        # write value type to byte code
        self._write_code(cmd_code, type, 1)

        if type == PARAM_CONST:
            # write 32-bit constant reference
            if param_kind != ParamKind.value:
                param_type_error()

            new_constant = CmdParam(value=value)
            self.params.append(new_constant)
            self._write_code(cmd_code, len(self.params) - 1, 4)
        elif type == PARAM_VAR:
            # write 32-bit variable reference
            if len(value) < 2:
                raise ScriptError("Variable name expected")

            if script_id > 0 and '~' not in value:
                value = f"${self.include_scripts[script_id]}~{value[1:]}"

            if param_kind != ParamKind.value and param_kind != ParamKind.var:
                param_type_error()

            # generate index list from variable, stripping out index specifiers
            index_list: List[str] = []

            value = build_index_list(index_list, value)

            # see if the variable exists
            id = next((i for i, item in enumerate(self.params)
                          if isinstance(item, VarParam) and item.name == value), -1)

            if id == -1:
                # build new variable
                new_var = VarParam(value)
                # EP - See if it could be a number
                if value.isnumeric():
                    new_var.dec_value = float(value)

                id = len(self.params)
                self.params.append(new_var)

            self._write_code(cmd_code, id, 4)  # write variable reference
            write_index_list(index_list)

        elif type == PARAM_SYSCONST:
            # write 16-bit system constant reference
            if param_kind is not ParamKind.value:
                param_type_error()

            # generate index list for this constant
            index_list: List[str] = []

            # get the ID of this system const
            code_word = self._script_ref.find_sys_const(value)

            self._write_code(cmd_code, code_word, 2)
            write_index_list(index_list)
        elif type == PARAM_PROGVAR:
            # write 32-bit program variable reference
            if param_kind is not ParamKind.value:
                param_type_error()

            if len(value) < 2:
                raise ScriptError('No character code supplied')

            try:
                id = int(str(value[1]))
            except ValueError:
                raise ScriptError('Bad character code')

            if id < 0 or id > 255:
                raise ScriptError(f'Character #{id} does not exist')

            self._write_code(cmd_code, id, 1)

    def _recurse_cmd(self, cmd_line: List[str], line: int, script_id: int):
        # convert the cmd_line into a string list and throw it all back through compiler
        self._compile_param_line(cmd_line[:], line, script_id)

    def _compile_param(self, param: str, cmd_code: bytearray, param_kind: ParamKind, line: int, script_id: int):
        # Param is a full equation of values joined by operators, it must be broken down and
        # the values passed to CompileValue

        class Branch:
            def __init__(self, value1: Any, value2: Any, op: str):
                self.value1 = value1
                self.value2 = value2
                self.op = op

        def split_operator(equation: str, ops: str) -> Union[Tuple[str, str, str], bool]:
            # seek out the specified operator within this equation, and split the values around it
            bracket_level = 0
            in_quote = False

            for i, c in enumerate(equation):
                if c == '"':
                    in_quote = not in_quote
                elif not in_quote:
                    if c == '(':
                        bracket_level += 1
                    elif c == ')':
                        bracket_level -= 1

                    elif bracket_level == 0:
                        for j, op in enumerate(ops):
                            if op == c:
                                if i == 0 or i == len(equation) - 1:
                                    raise ScriptError('Operation syntax error')

                                # split into values
                                return equation[0:i], equation[i + 1:], op

            return '', '', ''

        def break_down(equation: str) -> Branch:
            test_brackets = True
            while test_brackets:
                test_brackets = False
                if len(equation) > 1:
                    if equation.startswith('(') and equation.endswith(')'):
                        if equation == '()':
                            raise ScriptError('Empty brackets')

                        bracket_level = 0
                        encased = True
                        in_quote = False

                        for i, e in enumerate(equation):
                            if e == '"':
                                in_quote = not in_quote
                            elif not in_quote:
                                if e == '(':
                                    bracket_level += 1
                                elif e == ')':
                                    bracket_level -= 1

                                    if bracket_level == 0 and i is not len(equation) - 1:
                                        encased = False
                                        break

                        if encased:
                            # entire equation is encased in brackets, strip them out
                            equation = equation[1:-1]
                            test_brackets = True

            split = True

            value1, value2, op = split_operator(equation, f'-<>&{OP_GREATEREQUAL}{OP_LESSEREQUAL}{OP_AND}{OP_OR}{OP_XOR}{OP_NOTEQUAL}')
            if not op:
                value1, value2, op = split_operator(equation, '+-')
                if not op:
                    value1, value2, op = split_operator(equation, '*/')
                    if not op:
                        split = False

            if split:
                return Branch(break_down(value1), break_down(value2), op)
            else:
                return Branch(equation, '', OP_NONE)

        def compile_tree(branch: Branch) -> str:
            # return the name of the variable containing the result of this operation
            self._sys_var_count += 1
            result = f'$${self._sys_var_count}'

            if branch.op == OP_NONE:
                # its a value
                result = branch.value1
            else:
                # its an operation
                value1 = compile_tree(branch.value1)
                value2 = compile_tree(branch.value2)

                if branch.op == '+':
                    # addition (+): add the values together
                    self._recurse_cmd(['SETVAR', result, value1], line, script_id)
                    self._recurse_cmd(['ADD', result, value2], line, script_id)
                elif branch.op == '-':
                    # subtraction (-): subtract value2 from value1
                    self._recurse_cmd(['SETVAR', result, value1], line, script_id)
                    self._recurse_cmd(['SUBTRACT', result, value2], line, script_id)
                elif branch.op == '*':
                    # multiplication (*): multiply values together
                    self._recurse_cmd(['SETVAR', result, value1], line, script_id)
                    self._recurse_cmd(['MULTIPLY', result, value2], line, script_id)
                elif branch.op == '/':
                    # division (/): divide value1 from value2
                    self._recurse_cmd(['SETVAR', result, value1], line, script_id)
                    self._recurse_cmd(['DIVIDE', result, value2], line, script_id)
                elif branch.op == '&':
                    # concatenation (%): concatenate both values
                    self._recurse_cmd(['MERGETEXT', value1, value2], line, script_id)
                elif branch.op == '=':
                    # equal test (=): set result to 1 if both values are equal, otherwise 0
                    self._recurse_cmd(['ISEQUAL', result, value1, value2], line, script_id)
                elif branch.op == '>':
                    # greater than test (>): set result to 1 if Value1 > Value2, otherwise 0
                    self._recurse_cmd(['ISGREATER', result, value1, value2], line, script_id)
                elif branch.op == '<':
                    # lesser than test (<): set result to 1 if Value1 < Value2, otherwise 0
                    self._recurse_cmd(['ISLESSER', result, value1, value2], line, script_id)
                elif branch.op == OP_GREATEREQUAL:
                    # greater than or equal to test (>=): set result to 1 if Value1 >= Value2, otherwise 0
                    self._recurse_cmd(['ISGREATEREQUAL', result, value1, value2], line, script_id)
                elif branch.op == OP_LESSEREQUAL:
                    # lesser than or equal to test (<=): set result to 1 if Value1 <= Value2, otherwise 0
                    self._recurse_cmd(['ISLESSEREQUAL', result, value1, value2], line, script_id)
                elif branch.op == OP_NOTEQUAL:
                    # not equal to test (<>): set result to 1 if values are not equal, otherwise 0
                    self._recurse_cmd(['ISNOTEQUAL', result, value1, value2], line, script_id)
                elif branch.op == OP_AND:
                    # AND boolean operator (AND)
                    self._recurse_cmd(['SETVAR', result, value1], line, script_id)
                    self._recurse_cmd(['AND', result, value2], line, script_id)
                elif branch.op == OP_OR:
                    # OR boolean operator (OR)
                    self._recurse_cmd(['SETVAR', result, value1], line, script_id)
                    self._recurse_cmd(['OR', result, value2], line, script_id)
                elif branch.op == OP_XOR:
                    # Exclusive OR boolean operator (XOR)
                    self._recurse_cmd(['SETVAR', result, value1], line, script_id)
                    self._recurse_cmd(['XOR', result, value2], line, script_id)

            return result

        # Param is a set of Command parameters linked by operators, i.e. ($VAR+50)&"text"
        # We need to form a binary tree of all operations to be performed

        root = break_down(param)
        if root.op == OP_NONE:
            # tree is only one value, compile it
            self._compile_value(root.value1, cmd_code, param_kind, line, script_id)
        else:
            if param_kind is PARAM_VAR:
                raise ScriptError('Command parameter must be a variable')

            # branch up the tree and process each operation
            self._compile_value(compile_tree(root), cmd_code, param_kind, line, script_id)

    def _compile_param_line(self, param_line: List[str], line: int, script_id: int):
        # ParamLine is a broken down list of all params in the line, including the command.
        if param_line[0][0] == ':':
            if len(param_line) > 1:
                raise ScriptError('Unnecessary parameters after label declaration')

            if len(param_line[0]) < 2:
                raise ScriptError('Bad label name')

            label_name = param_line[0][1:]

            if '~' not in label_name and script_id > 0:
                self._build_label(f"{self.include_scripts[script_id]}~{label_name}", self._code_size)
            else:
                self._build_label(label_name, self._code_size)
        else:
            # check for macro commands
            if param_line[0] == 'WHILE':
                if len(param_line) > 2:
                    raise ScriptError('Unnecessary parameters after WHILE macro')
                elif len(param_line) < 2:
                    raise ScriptError('No parameters to compare with WHILE macro')

                # write WHILE macro
                con_struct = ConditionStruct(f'::{self.label_count + 1}', f'::{self._label_count + 2}', True, False)
                self._label_count += 2
                self._stack.append(con_struct)
                self._recurse_cmd([con_struct.con_label], line, script_id)
                self._recurse_cmd(['BRANCH', param_line[1], con_struct.end_label], line, script_id)
            elif param_line[0] == 'IF':
                if len(param_line) > 2:
                    raise ScriptError('Unnecessary parameters after IF macro')
                elif len(param_line) < 2:
                    raise ScriptError('No parameters to compare with IF macro')

                # write IF macro
                con_struct = ConditionStruct(f'::{self.label_count + 1}', f'::{self._label_count + 2}', False, False)
                self._label_count += 2
                self._stack.append(con_struct)
                self._recurse_cmd(['BRANCH', param_line[1], con_struct.con_label], line, script_id)
            elif param_line[0] == 'ELSE':
                if len(param_line) > 1:
                    raise ScriptError('Unnecessary parameters after ELSE macro')
                elif not self._stack:
                    raise ScriptError('ELSE without IF')

                # write ELSE macro
                con_struct = self._stack[-1]
                if con_struct.is_while:
                    raise ScriptError('Cannot use ELSE with WHILE')

                if con_struct.at_else:
                    raise ScriptError('IF macro syntax error')

                con_struct.at_else = True
                self._recurse_cmd(['GOTO', con_struct.end_label], line, script_id)
                self._recurse_cmd([con_struct.con_label], line, script_id)
            elif param_line[0] == 'ELSEIF':
                if len(param_line) > 2:
                    raise ScriptError('Unnecessary parameters after ELSEIF macro')
                elif len(param_line) < 2:
                    raise ScriptError('No parameters to compare with ELSEIF macro')

                if not self._stack:
                    raise ScriptError('ELSEIF without IF')

                # write ELSEIF macro
                con_struct = self._stack[-1]

                if con_struct.is_while:
                    raise ScriptError('Cannot use ELSEIF with WHILE')

                if con_struct.at_else:
                    raise ScriptError('IF macro syntax error')

                self._recurse_cmd(['GOTO', con_struct.end_label], line, script_id)
                self._recurse_cmd([con_struct.con_label], line, script_id)
                self._label_count += 1
                con_struct.con_label = f'::{self._label_count}'
                self._recurse_cmd(['BRANCH', param_line[1], con_struct.con_label], line, script_id)
            elif param_line[0] == 'END':
                if len(param_line) > 1:
                    raise ScriptError('Unnecessary parameters after END macro')

                if not self._stack:
                    raise ScriptError('END without IF')

                # write END macro
                con_struct = self._stack.pop()

                if con_struct.is_while:
                    self._recurse_cmd(['GOTO', con_struct.con_label], line, script_id)
                else:
                    self._recurse_cmd([con_struct.con_label], line, script_id)

                self._recurse_cmd([con_struct.end_label], line, script_id)
            elif param_line[0] == 'INCLUDE':
                if len(param_line) > 2:
                    raise ScriptError('Unnecessary parameters after INCLUDE macro')
                elif len(param_line) < 2:
                    raise ScriptError('No file name supplied for INCLUDE macro')

                if param_line[1][0] == '"':
                    param_line[1][0] = param_line[1][1:-1]

                # include script
                self._include_file(param_line[1])
            elif param_line[0] == 'WAITON':
                # WaitOn macro - create text trigger and label
                if len(param_line) > 2:
                    raise ScriptError('Unnecessary parameters after WAITON macro')
                elif len(param_line) < 2:
                    raise ScriptError('No wait text supplied for WAITON macro')

                self._wait_on_count += 1

                self._recurse_cmd(['SETTEXTTRIGGER', f'WAITON{self._wait_on_count}', f':WAITON{self._wait_on_count}', param_line[1]], line, script_id)
                self._recurse_cmd(['PAUSE'], line, script_id)
                self._recurse_cmd([f':WAITON{self._wait_on_count}'], line, script_id)
            else:
                self._cmd_count += 1
                i = self._script_ref.find_cmd(param_line[0])

                if i < 0:
                    # command not found, give error message
                    raise ScriptError(f"Unknown command '{param_line[0]}'")

                id = i

                cmd_code = bytearray()
                line_word = line
                self._write_code(cmd_code, script_id, 1)
                self._write_code(cmd_code, line_word, 2)
                self._write_code(cmd_code, id, 2)

                # check if we have the right number of parameters
                if len(param_line) - 1 > self._script_ref.cmds[id].max_params > -1:
                    raise ScriptError(f"Too many parameters for command '{param_line[0]}'")
                elif len(param_line) - 1 < self._script_ref.cmds[id].min_params:
                    raise ScriptError(f"Too few parameters for command '{param_line[0]}'")

                # compile the parameters independently
                for i, param in enumerate(param_line[1:]):
                    self._compile_param(param, cmd_code, self._script_ref.cmds[id].get_param_kind(i), line, script_id)

                # write the command to byte code (with null termination)
                cmd_code.append(0)
                self._append_code(cmd_code)

    def _convert_ops(self, line: str) -> str:
        param = ''
        result = ''
        in_quote = False

        for i, c in enumerate(line):
            if c == '"':
                in_quote = not in_quote
            if c == ' ':
                if not in_quote:
                    up_param = param.upper()
                    if up_param == 'AND':
                        param = OP_AND
                    elif up_param == 'OR':
                        param = OP_OR
                    elif up_param == 'XOR':
                        param = OP_XOR

                result = f"{result}{param} "
                param = ''
            else:
                param += c

        return result

    def _convert_conditions(self, line: str) -> str:
        # convert multi-character conditions (>=, <> and <=) into single character ones
        last = chr(0)
        result = ''
        in_quote = False
        for i, c in enumerate(line):
            if c == '"':
                in_quote = not in_quote
                result += c
            elif c == '=' and not in_quote:
                if last == '>':
                    result += OP_GREATEREQUAL
                elif last == '<':
                    result += OP_LESSEREQUAL
                else:
                    result += '='
            elif c != '>' and c != '<' or in_quote:
                if last == '>' or last == '<' and not in_quote:
                    result += last + c
                else:
                    result += c
            elif last == '<' and c == '>':
                result += OP_NOTEQUAL
                c = chr(0)

            last = c
        return result

    def _is_operator(self, c: str) -> bool:
        return c in ('=', '>', '<', '+', '-', '*', '/', '&',
                     OP_GREATEREQUAL, OP_LESSEREQUAL, OP_AND, OP_OR, OP_XOR, OP_NOTEQUAL)

    def _compile_from_strings(self, script_text: List[str], script_name: str):
        self._line_count += len(script_text)
        script_id = len(self.include_scripts)
        self.include_scripts.append(script_name.upper())
        param_line: List[str] = []

        line = 1

        try:
            while line <= len(script_text):
                last = ' '
                param_str = ''
                line_text = self._convert_conditions(self._convert_ops(script_text[line - 1] + ' '))
                in_quote = False
                linked = False
                param_line.clear()

                if len(line_text) > 0:
                    for i, c in enumerate(line_text):
                        if c == '#' and param_str == '' and len(param_line) == 0:
                            break  # its a comment

                        if c == '/' and last == '/':
                            # Its an in-line comment, like this one (//)
                            param_str = param_str[1:]
                            break

                        if not in_quote and self._is_operator(c):
                            if linked:
                                raise ScriptError("Operation syntax error")

                            linked = True
                            param_str += c
                        elif (c != ' ' and c != chr(9)) or in_quote:
                            if (last == ' ' or last == chr(9)) and not linked and not in_quote and param_str != '':
                                # parameter completed
                                param_line.append(param_str)
                                param_str = ''

                            if in_quote:
                                param_str += c
                            else:
                                param_str += c.upper()

                            if c == '"':
                                in_quote = not in_quote

                            linked = False

                        last = c

                    # reset our system variable count (for operators)
                    self._sys_var_count = 0

                    # complete last param
                    param_line.append(param_str)

                    # compile the line
                    if not (len(param_line) == 0 or ((param_str == '') and (len(param_line) == 1))):
                        self._compile_param_line(param_line, line, script_id)

                line += 1

            # make sure our IF/ELSE/END blocks match up properly
            if len(self._stack) != 0:
                raise ScriptError('IF/WHILE .. END structure mismatch')
        except ScriptError as e:
            # add a line number to the exception message
            raise ScriptError(f"{e}, line {line} ({self.include_scripts[script_id]})")
        except Exception as e:
            traceback.print_exc()
            raise ScriptError(f"Unknown error, line {line} ({self.include_scripts[script_id]})")

    def _include_file(self, filename: str):
        # include more code in this script
        name = fetch_script(filename, True).upper()

        # see if its already included
        if next(s for s in self.include_scripts if s == name):
            return  # already included

        if name.endswith(".INC"):
            raise ScriptError("Encrypted scripts not supported")
        else:
            try:
                with open(name) as f:
                    script_text = f.read().splitlines()
            except:
                raise ScriptError(f"Unable to process include file '{filename}")

            self._compile_from_strings(script_text, os.path.splitext(os.path.basename(filename))[0])


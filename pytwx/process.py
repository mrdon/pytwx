from datetime import datetime
from enum import Enum, auto
from typing import List

from pytwx.ansi import ANSI_9
from pytwx.core import HistoryType
from pytwx.database import Trader, Ship, Planet, Sector, SectorExploredType, Database, ProductType as PT, FighterType, \
    Port
from pytwx.gui import TwxGui
from pytwx.utility import get_parameter, get_parameter_pos, strip_chars, get_parameters


class SectorPosition(Enum):
    normal = auto()
    ports = auto()
    planets = auto()
    ships = auto()
    mines = auto()
    traders = auto()


class Display(Enum):
    none = auto()
    sector = auto()
    density = auto()
    warp_lane = auto()
    cim = auto()
    port_cim = auto()
    port = auto()
    port_cr = auto()
    warp_cim = auto()
    fig_scan = auto()


class FigScanType(Enum):
    personal = auto()
    corp = auto()


def line_with_ts(line):
    return "  ".join([datetime.now(), strip_chars(line)])


class ModExtractor:
    def __init__(self, database: Database, interpreter: 'pytwx.script.ModInterpreter', gui: TwxGui):
        self.interpreter = interpreter

        self._database = database
        self._gui = gui

        self._current_sector_index: int = 0
        self._current_sector: Sector = None
        self._port_sector_index: int = 0
        self._fig_scan_sector: int = 0
        self._sector_position: SectorPosition = None
        self._current_display: Display = None
        self._last_warp: int = 0
        self._sector_saved: bool = True
        self._current_trader: Trader = Trader()
        self._current_ship: Ship = None
        self._current_message: str = None
        self._trader_list: List[Trader] = []
        self._ship_list: List[Ship] = []
        self._planet_list: List[Planet] = []

        self._in_ansi: bool = False

    @property
    def current_sector(self):
        return self._current_sector_index

    def reset(self):
        self._in_ansi = False
        self._current_display = Display.none
        self._reset_sector_lists()

    def _sector_completed(self):
        if self.current_sector == 0:
            raise Exception("Current sector cannot be 0")

        self._current_sector.update = datetime.now()
        self._current_sector.explored = SectorExploredType.holo
        self._sector_saved = True

        self._current_sector.warps = len(self._current_sector.warp)
        self._current_sector.planets = list(self._planet_list)
        self._current_sector.traders = list(self._trader_list)
        self._current_sector.ships = list(self._ship_list)
        self._save_sector(self._current_sector, self._current_sector_index)
        self._reset_sector_lists()

    def _save_sector(self, sector: Sector, sector_index: int):
        self._database.save_sector(sector, sector_index)
        self._gui.on_sector_update(sector, sector_index)

    def _reset_sector_lists(self):
        self._ship_list.clear()
        self._trader_list.clear()
        self._planet_list.clear()

    def process_prompt(self, line: str):
        """
        This procedure checks command prompts.  It is called from both
        processline and processinbound, as it can come in as part of
        a large packet or still be waiting for the user.
        """
        if line.startswith('Command [TL='):
            # Save current sector if not done already
            if not self._sector_saved:
                self._sector_completed()

            # Record Current Sector Index
            self._update_player_in_sector(int(line[23:line.find(']', 23)]))

            self._current_display = Display.none
            self._last_warp = 0
        elif line.startswith('Probe entering sector :') or line.startswith('Probe Self Destructs'):
            # mid probe - save the sector
            if not self._sector_saved:
                self._sector_completed()

            # No displays anymore, all done
            self._current_display = Display.none
        elif line.startswith('Computer command [TL='):
            # in computer prompt, kill all displays and clear warp data
            self._current_display = Display.none
            self._last_warp = 0

            # Record Current Sector Index to SysConstant CURRENTSECTOR
            self._current_sector_index = int(line[32:32 + line.find('(') - 35])
            self._update_player_in_sector(self._current_sector_index)
        elif line.startswith('Citadel treasury contains'):
            # In Citadel - Save current sector if not done already
            if not self._sector_saved:
                self._sector_completed()

            # No displays anymore, all done
            self._current_display = Display.none
        elif line.startswith('Stop in this sector') or line.startswith('Engage the Autopilot?'):
            # Save current sector if not done already
            if not self._sector_saved:
                self._sector_completed()

            # No displays anymore, all done
            self._current_display = Display.none
        elif line.startswith(': '):
            # at the CIM prompt
            if self._current_display != Display.cim:
                self._current_display = Display.none

            self._last_warp = 0

        self.interpreter.text_event(line, False)

    def _update_player_in_sector(self, sector_index):
        self._current_sector_index = sector_index
        self._gui.on_current_sector_update(sector_index)

    def _add_warp(self, sect_num: int, warp: int):
        """
        Used by ProcessWarpLine to add a warp to a sector
        """

        s = self._database.load_sector(sect_num)

        # see if the warp is already in there
        if warp in s.warp:
            return

        s.warp.append(warp)

        if s.explored == SectorExploredType.no:
            s.constellation = '???' + ANSI_9 + ' (warp calc only)'
            s.explored = SectorExploredType.calc
            s.update = datetime.now()

        self._save_sector(s, sect_num)

    def _process_warp_line(self, line: str):
        """
        A WarpLine is a line of warps plotted using the ship's computer.  Add new warps to
        any sectors listed in the warp lane (used extensively for ZTM).
        e.g:  3 > 300 > 5362 > 13526 > 149 > 434
        """

        last_sect = self._last_warp
        line = line.translate(str.maketrans(dict.fromkeys('()')))
        if not line or any(c.isalpha() for c in line):
            return
        sectors = line.split('>')
        for cur_sect in [int(s.strip()) for s in sectors]:
            if cur_sect < 1 or cur_sect > self._database.db_header.sectors:
                raise Exception(f"Invalid sector number: {cur_sect}")
            if last_sect > 0:
                self._add_warp(last_sect, cur_sect)

            last_sect = cur_sect
            self._last_warp = cur_sect

    def _process_cim_line(self, line: str):
        def get_cim_value(m: str, num: int) -> int:
            s = get_parameter(m, num)

            if s == '':
                result = 0
            else:
                try:
                    result = int(s)
                except ValueError:
                    result = -1

            return result

        if self._current_display == Display.warp_cim:

            # fixme: this isn't perfect as a report of explored or unexplored sectors that only shows a few on a line
            # will pass this test
            if len(get_parameters(line)) > 7:
                return

            # save warp CIM data
            sect = get_cim_value(line, 0)

            if sect <= 0 or sect > self._database.db_header.sectors:
                self._current_display = Display.none
                return

            s = self._database.load_sector(sect)
            for i in range(0, 6):
                x = get_cim_value(line, i + 1)

                if x < 0 or x > self._database.db_header.sectors:
                    self._current_display = Display.none
                    return
                else:
                    s.warp.append(x)

            if s.explored == SectorExploredType.no:
                s.constellation = '???' + ANSI_9 + ' (warp calc only)'
                s.explored = SectorExploredType.calc
                s.update = datetime.now()

            self._save_sector(s, sect)
        else:
            # save port CIM data
            sect = get_cim_value(line, 0)
            sectors_len = len(str(self._database.db_header.sectors))

            if sect <= 0 or sect > self._database.db_header.sectors or len(line) < sectors_len + 36:
                self._current_display = Display.none
                return

            m = line.replace('-', '').replace('%', '')
            s = self._database.load_sector(sect)

            ore = get_cim_value(m, 1)
            org = get_cim_value(m, 3)
            equip = get_cim_value(m, 5)
            p_ore = get_cim_value(m, 2)
            p_org = get_cim_value(m, 4)
            p_equip = get_cim_value(m, 6)

            if ore < 0 or org < 0 or equip < 0 or \
                            p_ore < 0 or p_ore > 100 or \
                            p_org < 0 or p_org > 100 or \
                            p_equip < 0 or p_equip > 100:
                self._current_display = Display.none
                return

            s.port.product_amount[PT.fuel_ore] = ore
            s.port.product_amount[PT.organics] = org
            s.port.product_amount[PT.equipment] = equip
            s.port.product_percent[PT.fuel_ore] = p_ore
            s.port.product_percent[PT.organics] = p_org
            s.port.product_percent[PT.equipment] = p_equip
            s.port.update = datetime.now()

            if s.port.name == '':
                # port not saved/seen before - get its details

                if line[sectors_len + 1] == '-':
                    s.port.buy_product[PT.fuel_ore] = True
                else:
                    s.port.buy_product[PT.fuel_ore] = False

                if line[sectors_len + 13] == '-':
                    s.port.buy_product[PT.organics] = True
                else:
                    s.port.buy_product[PT.organics] = False

                if line[sectors_len + 25] == '-':
                    s.port.buy_product[PT.equipment] = True
                else:
                    s.port.buy_product[PT.equipment] = False

                buys = s.port.buy_product
                if buys[PT.fuel_ore] and buys[PT.organics] and buys[PT.equipment]:
                    s.port.class_index = 8
                elif buys[PT.fuel_ore] and buys[PT.organics] and not buys[PT.equipment]:
                    s.port.class_index = 1
                elif buys[PT.fuel_ore] and not buys[PT.organics] and buys[PT.equipment]:
                    s.port.class_index = 2
                elif not buys[PT.fuel_ore] and buys[PT.organics] and buys[PT.equipment]:
                    s.port.class_index = 3
                elif not buys[PT.fuel_ore] and not buys[PT.organics] and buys[PT.equipment]:
                    s.port.class_index = 4
                elif not buys[PT.fuel_ore] and buys[PT.organics] and not buys[PT.equipment]:
                    s.port.class_index = 5
                elif buys[PT.fuel_ore] and not buys[PT.organics] and not buys[PT.equipment]:
                    s.port.class_index = 6
                elif not buys[PT.fuel_ore] and not buys[PT.organics] and not buys[PT.equipment]:
                    s.port.class_index = 7

                s.port.name = "???"
            if s.explored == SectorExploredType.no:
                s.constellation = '???' + ANSI_9 + ' (port data/calc only)'
                s.explored = SectorExploredType.calc
                s.update = datetime.now()

            self._save_sector(s, sect)

    def _process_sector_line(self, line: str):
        if line.startswith('Beacon  : '):
            # Get beacon text
            self._current_sector.beacon = line[10:]
        elif line.startswith('Ports   : '):
            # Save port data
            if '<=-DANGER-=>' in line:
                # Port is destroyed
                self._current_sector.port.dead = True
            else:
                self._current_sector.port.dead = False
                self._current_sector.port.build_time = 0
                self._current_sector.port.name = line[10:line.index(', Class')]
                cls_index = line.index(', Class')
                self._current_sector.port.class_index = int(line[cls_index + 8:cls_index + 9])

                if line[-3] == 'B':
                    self._current_sector.port.buy_product[PT.fuel_ore] = True
                else:
                    self._current_sector.port.buy_product[PT.fuel_ore] = False

                if line[-2] == 'B':
                    self._current_sector.port.buy_product[PT.organics] = True
                else:
                    self._current_sector.port.buy_product[PT.organics] = False

                if line[-1] == 'B':
                    self._current_sector.port.buy_product[PT.equipment] = True
                else:
                    self._current_sector.port.buy_product[PT.equipment] = False

                self._sector_position = SectorPosition.ports

        elif line.startswith('Planets : '):
            # Get planet data
            new_planet = Planet(line[10:])
            self._planet_list.append(new_planet)
            self._sector_position = SectorPosition.planets

        elif line.startswith("Traders : "):
            # Save traders
            i = line.index(', w/')
            self._current_trader.name = line[10:i]
            s = line[i + 5:line.index(' ftrs')]
            s = s.replace(',', '')
            self._current_trader.figs = int(s)
            self._sector_position = SectorPosition.traders

        elif line.startswith('Ships   : '):
            # Save ships
            i = line.index('[Owned by]')
            self._current_ship.name = line[10:i - 1]
            self._current_ship.owner = line[i + 11:line.index(', w/')]
            i = line.index(', w/')
            s = line[i + 5:line.index(' ftrs,')]
            s = s.replace(',', '')
            self._current_ship.figs = int(s)
            self._sector_position = SectorPosition.ships
        elif line.startswith('Fighters: '):
            # Get fig details
            s = get_parameter(line, 1)
            s = s.replace(',', '')
            self._current_sector.figs.quantity = int(s)
            i = get_parameter_pos(line, 2) + 1
            self._current_sector.figs.owner = line[i:line.index(')')]

            if line.endswith('[Toll]'):
                self._current_sector.figs.fig_type = FighterType.toll
            elif line.endswith('[Defensive]'):
                self._current_sector.figs.fig_type = FighterType.defensive
            else:
                self._current_sector.figs.fig_type = FighterType.offensive
        elif line.startswith('NavHaz  : '):
            s = get_parameter(line, 2)
            s = s[0:-1]
            self._current_sector.navhaz = int(s)
        elif line.startswith('Mines   : '):
            # Save mines
            self._sector_position = SectorPosition.mines
            i = get_parameter_pos(line, 6) + 1
            s = line[i:]

            if get_parameter(line, 6 == 'Armid)'):
                self._current_sector.mines_armid.quantity = int(get_parameter(line, 2))
                self._current_sector.mines_armid.owner = s
            else:
                self._current_sector.mines_limpet.quantity = int(get_parameter(line, 2))
                self._current_sector.mines_limpet.owner = s
        elif line.startswith('        '):
            # Continue from last occurrence
            if self._sector_position == SectorPosition.mines:
                i = get_parameter_pos(line, 5) + 1
                self._current_sector.mines_limpet.quantity = int(get_parameter(line, 1))
                self._current_sector.mines_limpet.owner = line[i:]
            elif self._sector_position == SectorPosition.ports:
                self._current_sector.port.build_time = int(get_parameter(line, 3))
            elif self._sector_position == SectorPosition.planets:
                # Get planet data
                self._planet_list.append(Planet(line[10:-1]))
            elif self._sector_position == SectorPosition.traders:
                if get_parameter(line, 0) == 'in':
                    # Still working on one trader
                    new_trader = Trader()
                    i = get_parameter_pos(line, 1)
                    new_trader.ship_name = line[i:line.index('(') - 1]
                    new_trader.ship_type = line[line.index('(') + 1:line.index(')')]
                    new_trader.name = self._current_trader.name
                    new_trader.figs = self._current_trader.figs
                    self._trader_list.append(new_trader)
                else:
                    # New trader
                    i = line.index(', w/')
                    self._current_trader.name = line[10:i]
                    s = line[i + 5:line.index(' ftrs')]
                    s = s.replace(',', '')
                    self._current_trader.figs = int(s)
            elif self._sector_position == SectorPosition.ships:
                if line[11] == '(':
                    # Get the rest of the ship info
                    new_ship = Ship()
                    new_ship.name = self._current_ship.name
                    new_ship.owner = self._current_ship.owner
                    new_ship.figs = self._current_ship.figs
                    new_ship.ship_type = line[12:line.index(')')]
                    self._ship_list.append(new_ship)
                else:
                    # New ship
                    i = line.index('[Owned by]')
                    self._current_ship.name = line[10:i - 1]
                    self._current_ship.owner = line[i + 11: line.index(', w/')]
                    i = line.index(', w/')
                    s = line[i + 5:line.index(' ftrs,')]
                    s = s.replace(',', '')
                    self._current_ship.figs = int(s)
                    self._sector_position = SectorPosition.ships
        elif line[8] == ':':
            self._sector_position = SectorPosition.normal
        elif line.startswith("Warps to Sector(s) :"):
            line = line.replace(')', '').replace('(', '')

            # Get sector warps
            for pos in (4, 6, 8, 10, 12, 14):
                s = get_parameter(line, pos)
                if s:
                    self._current_sector.warp.append(int(s))
            # sector done
            if not self._sector_saved:
                self._sector_completed()

            # No displays anymore, all done
            self._current_display = Display.none
            self._sector_position = SectorPosition.normal

    def process_line(self, line: str):
        # Every line is passed to this procedure to be processed and recorded
        if self._current_message:
            if line:
                if self._current_message == 'Figs':
                    self._gui.add_to_history(HistoryType.fighter, line_with_ts(line))
                elif self._current_message == "Comp":
                    self._gui.add_to_history(HistoryType.computer, line_with_ts(line))
                else:
                    self._gui.add_to_history(HistoryType.msg, line_with_ts(line))

                self._current_message = ''
        elif line.startswith('R ') or line.startswith('F '):
            self._gui.add_to_history(HistoryType.msg, line_with_ts(line))
        elif line.startswith('P '):
            if get_parameter(line, 1) != 'indicates':
                self._gui.add_to_history(HistoryType.msg, line_with_ts(line))
        elif line.startswith("Incoming transmission from") or line.startswith("Continuing transmission from"):
            # Transmission with ansi off
            i = get_parameter_pos(line, 3)
            if line[-9:] == "comm-link:":
                # Fedlink
                self._current_message = 'F {} '.format(line[i:line.index(' on Federation')])
            elif get_parameter(line, 4) == "Fighters:":
                # Fighters
                self._current_message = "Figs"
            elif get_parameter_pos(line, 4) == "Computers:":
                # Computer
                self._current_message = "Comp"
            elif ' on channel ' in line:
                # Radio
                self._current_message = 'R {} '.format(line[i:line.index(' on channel ')])
            else:
                self._current_message = 'P {} '.format(line[i:])

        elif line.startswith("Deployed Fighters Report Sector"):
            self._gui.add_to_history(HistoryType.fighter, line_with_ts(line[18:]))
        elif line.startswith("Shipboard Computers "):
            self._gui.add_to_history(HistoryType.fighter, line_with_ts(line[20:]))
        elif line[13:21] == 'StarDock' and line[36:42] == 'sector':
            # Capture stardock from the 'V' screen. Bean and Constellation are assumed,
            # but will be updated when the sector is finally visited

            i = int(line[44:line.index('.')])
            if 0 < i <= self._database.db_header.sectors:
                if not self._database.db_header.star_dock:
                    sect = self._database.load_sector(i)
                    sect.constellation = 'The Federation'
                    sect.beacon = "FedSpace, FedLaw Enforced"
                    sect.port.dead = False
                    sect.port.build_time = 0
                    sect.port.name = "Stargate Alpha I"
                    sect.port.class_index = 9
                    sect.explored = SectorExploredType.calc
                    sect.update = datetime.now()
                    self._save_sector(sect, i)
        elif line.startswith("The shortest path (") or line.startswith("  TO > "):
            self._current_display = Display.warp_lane
            self._last_warp = 0
        elif self._current_display == Display.warp_lane:
            self._process_warp_line(line)
        elif self._current_display == Display.warp_cim or self._current_display == Display.port_cim:
            self._process_cim_line(line)
        elif self._current_display == Display.cim:
            # find out what kind of CIM this is
            if len(line) > 2:
                if line.strip()[-1] == '%':
                    self._database.last_port_cim = datetime.now()
                    self._current_display = Display.port_cim
                else:
                    self._current_display = Display.warp_cim

                self._process_cim_line(line)
        elif line.startswith('Sector  : '):
            sector_index = int(get_parameter(line, 2))
            # Check if this is a probe or holoscan (no warp pickup)
            if not self._sector_saved:
                self._sector_completed()
            else:
                self._update_player_in_sector(sector_index)

            # Begin recording of sector data
            self._current_display = Display.sector
            self._sector_saved = False

            # Clear sector variables
            self._current_sector = Sector(0)
            self._current_sector_index = sector_index
            i = get_parameter_pos(line, 4)
            self._current_sector.constellation = line[i:-1]
        elif self._current_display == Display.sector:
            self._process_sector_line(line)
        elif self._current_display == Display.port:
            self._process_port_line(line)
        elif line.startswith('Docking...'):  # Normal Port Report
            if not self._sector_saved:
                self._sector_completed()
            self._current_display = Display.port
            self._port_sector_index = self._current_sector_index
            self._current_sector = self._database.load_sector(self._port_sector_index)
            self._sector_saved = False
        elif self._current_display == Display.port_cr:
            self._process_port_line(line)
        elif line.startswith('What sector is the port in? '):  # Computer Port Report
            self._current_display = Display.port_cr
            i = line.index(chr(93))
            if len(line) != i + 1:
                self._port_sector_index = int(line[i + 1:])
            else:
                self._port_sector_index = self._current_sector_index

            self._current_sector = self._database.load_sector(self._port_sector_index)
        elif line[26:42] == "Relative Density":
            # A density scanner is being used - lets grab some data
            self._current_display = Display.density
        elif self._current_display == Display.density and line.startswith("Sector"):
            # Save all density data into sector database
            x = line
            x = x.replace('(', '').replace(')', '')
            i = int(get_parameter(x, 1))
            sect = self._database.load_sector(i)
            s = get_parameter(x, 3)
            s = s.replace(',', '')
            sect.density = int(s)

            if get_parameter(x, 12) == 'Yes':
                # Sector has Anomaly
                sect.anomaly = True
            else:
                sect.anomaly = False

            s = get_parameter(x, 9)
            s = s[:-1]
            sect.navhaz = int(s)

            sect.warps = int(get_parameter(x, 6))

            if sect.explored in [SectorExploredType.no, SectorExploredType.calc]:
                # Sector hasn't been scanned or seen before
                sect.constellation = '???' + ANSI_9 + ' (Density only)'
                sect.explored = SectorExploredType.density
                sect.update = datetime.now()

            self._save_sector(sect, i)
        elif line.startswith(": "):
            # begin CIM download
            self._current_display = Display.cim
        elif line[17:40] == "Deployed  Fighter  Scan":
            self._current_display = Display.fig_scan
            self._fig_scan_sector = 0
        elif self._current_display == Display.fig_scan:
            self._process_fig_scan_line(line)
        else:
            print(f"line not matched: {line}")

        self.interpreter.text_line_event(line, False)
        self.process_prompt(line)

        # Reactive script triggers
        self.interpreter.activate_triggers()

    def _process_port_line(self, line: str):
        """
        Process a line after Docking... or from a CR report
        """

        # By including the space after 'for' we avoid the problem with CR reports on Class 0's
        if line.startswith("Commerce report for "):
            # Get the port name
            self._current_sector.port.name = line[20:line.index(':')]
        elif line.startswith("Fuel Ore") and line[32] == '%':
            # Grab the data from the Fuel Ore line in the Port report
            line = line.replace('%', '')
            stat_fuel = get_parameter(line, 2)
            qty_fuel = int(get_parameter(line, 3))
            perc_fuel = int(get_parameter(line, 4))
            self._current_sector.port.buy_product[PT.fuel_ore] = stat_fuel == 'Buying'
            self._current_sector.port.product_amount[PT.fuel_ore] = qty_fuel
            self._current_sector.port.product_percent[PT.fuel_ore] = perc_fuel
        elif line.startswith("Organics") and line[32] == '%':
            # Grab the data from the organics line in the Port report
            line = line.replace('%', '')
            stat_org = get_parameter(line, 1)
            qty_org = int(get_parameter(line, 2))
            perc_org = int(get_parameter(line, 3))
            self._current_sector.port.buy_product[PT.organics] = stat_org == 'Buying'
            self._current_sector.port.product_amount[PT.organics] = qty_org
            self._current_sector.port.product_percent[PT.organics] = perc_org
        elif line.startswith("Equipment") and line[32] == '%':
            # Grab the data from the equipment line in the Port report
            line = line.replace('%', '')
            stat_equip = get_parameter(line, 1)
            qty_equip = int(get_parameter(line, 2))
            perc_equip = int(get_parameter(line, 3))
            self._current_sector.port.buy_product[PT.equipment] = stat_equip == 'Buying'
            self._current_sector.port.product_amount[PT.equipment] = qty_equip
            self._current_sector.port.product_percent[PT.equipment] = perc_equip

            # All products have been seen, so process the data
            # Timestamp the port data
            self._current_sector.port.update = datetime.now()

            # Only determine the class if it is unknown (-1)
            if not self._current_sector.port.class_index > 0:
                port_class = 'B' if self._current_sector.port.buy_product[PT.fuel_ore] else 'S'
                port_class += 'B' if self._current_sector.port.buy_product[PT.organics] else 'S'
                port_class += 'B' if self._current_sector.port.buy_product[PT.equipment] else 'S'

                self._current_sector.port.class_index = Port.labels[port_class]

            if self._current_sector.explored == SectorExploredType.no:
                # We're updating the Port data for a previously unseen sector.
                self._current_sector.constellation = '???' + ANSI_9 + ' (port data/calc only)'
                self._current_sector.explored = SectorExploredType.calc

            # That's all fo the product info, so save it now
            self._current_sector.port.update = datetime.now()
            self._save_sector(self._current_sector, self._port_sector_index)

    def _strip_ansi(self, s: str) -> str:
        pass

    def _process_fig_scan_line(self, line: str):
        """
        process and record the Fig Scan Info.  specifically parse and record this line:
           940           1       Personal    Defensive            N/A
                       10T Total
        """
        if line.startswith("No fighters deployed"):
            # no fighters in G list anymore, have to reset database.
            self._reset_fig_database()

        try:
            sector_num = int(get_parameter(line, 0))
        except ValueError:
            return

        if not get_parameter(line, 4):
            return

        sect = self._database.load_sector(sector_num)
        fig_owner = get_parameter(line, 2)
        if fig_owner == 'Personal':
            sect.figs.owner = "yours"
        else:
            sect.figs.owner = "belong to your Corp"

        # work on figuring out how many fighters are displayed
        fig_amount = get_parameter(line, 1)
        fig_amount = fig_amount.replace(',', '')
        if fig_amount.endswith('T'):
            fig_qty = int(fig_amount[:-1])
            multiplier = 1000
        elif fig_amount.endswith('M'):
            fig_qty = int(fig_amount[:-1])
            multiplier = 1000000
        elif fig_amount.endswith('B'):
            fig_qty = int(fig_amount[:-1])
            multiplier = 1000000000
        else:
            fig_qty = int(fig_amount)
            multiplier = 1

        if multiplier != 1:
            # Approximate figs
            fig_qty = fig_qty * multiplier
            # See if previously recorded fig amount is within the margin of rounding
            if (sect.figs.quantity < int(fig_qty - multiplier / 2)) or (
                        sect.figs.quantity > int(fig_qty + multiplier / 2)):
                sect.figs.quantity = fig_qty
        else:
            sect.figs.quantity = fig_qty

        # pull fig type from the FigScan line
        fig_type = get_parameter(line, 3)
        # Get Fig Type, and assign the right FigType value
        if fig_type == 'Defensive':
            sect.figs.fig_type = FighterType.defensive
        elif fig_type == 'Toll':
            sect.figs.fig_type = FighterType.toll
        else:
            sect.figs.fig_type = FighterType.offensive

        self._save_sector(sect, sector_num)

    def _reset_fig_database(self):
        """
        reset fighter owner, type, and quantity for sectors where our figs are thought to be
        """
        for i in range(11, self._database.db_header.sectors + 1):
            if i != self._database.db_header.star_dock:
                sect = self._database.load_sector(i)
                sect.figs.quantity = 0
                if sect.figs.owner == 'yours' or sect.figs.owner == 'belong to your Corp':
                    sect.figs.owner = ''
                    sect.figs.fig_type = FighterType.none
                    sect.figs.quantity = 0
                    self._save_sector(sect, i)


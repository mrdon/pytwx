"""
This unit contains the implementation for all script commands

EP's To Do:
Make boolean tests on the same line occur from left to right, and exit at the first fail
Add Array Insert / Delete or Remove... maybe Copy, Null

Elapsed <var> <TDateTime> <TDateTime>
Absolute <inputVar> <outputVar>
"""
from datetime import datetime
from os.path import isfile, splitext, basename
from random import randint
from typing import List, Callable

import pytwx.script
from pytwx.ansi import ANSI, ANSI_12
from pytwx.database import Database, SectorExploredType, FighterType, ProductType
from pytwx.menu import ModMenu
from pytwx.script_cmp import VarParam, ScriptFileHeader
from pytwx.script_ref import ParamKind as pk, ScriptRef, CmdAction, CmdParam, ScriptError
from pytwx.tcp import ModServer
from pytwx.utility import format_datetime

SCSectorParameterError = 'Sector parameter name cannot be longer than 10 characters'
SCSectorParameterValueError = 'Sector parameter value cannot be longer than 40 characters'


def _raise_to_power(value: float, power: int) -> float:
    return pow(value, power)


# noinspection PyUnusedLocal,PyMethodMayBeStatic
class BuiltinCommands:

    def __init__(self, database: Database, menu: ModMenu, server: ModServer):
        self._db = database
        self._menu = menu
        self._server = server
        self._interpreter: 'pytwx.script.ModInterpreter' = None
        # EP - These are for improving the speed of repetitive Read commands
        self._last_read_filename: str = ''
        self._last_read_mod_time: int = 0
        self._last_read_strings: List[str] = []

        # EP - To speed up Float -> String and String -> Float conversions - Set with cmdSetPrecision
        self._set_precision: int = 0
        self._last_precision: int = 0
        self._last_multiplier: float = 1
        self._max_float_variance: int = 0  # EP: Effectively half of the next decimal beyond Precision, aka Epsilon

    def _update_precision(self, precision: int):
        if precision != self._last_precision:
            self._last_precision = precision
            self._last_multiplier = pow(10, precision)
            self._max_float_variance = 0.5 / self._last_multiplier

    def _near_equal(self, f1: float, f2: float, precision: int) -> bool:
        # This is to compare floating point numbers for equality
        # An absolute difference of <= (0.5 / 10^Precision) will be considered equal
        self._update_precision(precision)
        return abs(f1 - f2) <= self._max_float_variance

    def _convert_to_number(self, s: str) -> int:
        try:
            return round(float(s))
        except ValueError:
            raise ScriptError(f"'{s}' is not a number")

    def _check_sector(self, index: int):
        if index <= 0 or index > self._db.db_header.sectors:
            raise ScriptError('Sector index out of bounds')

    # noinspection PyTypeChecker
    def build_command_list(self, script_ref: ScriptRef):
        script_ref.add_command('ADD', 2, 2, self._cmd_add, [pk.var, pk.value])
        script_ref.add_command('ADDMENU', 7, 7, self._cmd_add_menu, [pk.value] * 6)
        script_ref.add_command('AND', 2, 2, self._cmd_and, [pk.var, pk.value])
        script_ref.add_command('BRANCH', 2, 2, self._cmd_branch, [pk.value, pk.value])
        script_ref.add_command('CLIENTMESSAGE', 1, 1, self._cmd_client_message, [pk.value])
        script_ref.add_command('CLOSEMENU', 0, 0, self._cmd_close_menu, [])
        script_ref.add_command('CONNECT', 0, 0, self._cmd_connect, [])
        script_ref.add_command('CUTTEXT', 4, 4, self._cmd_cut_text, [pk.value, pk.var, pk.value, pk.value])
        script_ref.add_command('DELETE', 1, 1, self._cmd_delete, [pk.value])
        script_ref.add_command('DISCONNECT', 0, 0, self._cmd_disconnect, [])
        script_ref.add_command('DIVIDE', 2, 2, self._cmd_divide, [pk.var, pk.value])
        script_ref.add_command('ECHO', 1, -1, self._cmd_echo, [pk.value])
        script_ref.add_command('FILEEXISTS', 2, 2, self._cmd_file_exists, [pk.var, pk.value])
        script_ref.add_command('GETCHARCODE', 2, 2, self._cmd_get_char_code, [pk.value, pk.var])
        script_ref.add_command('GETCONSOLEINPUT', 1, 2, self._cmd_get_console_input, [pk.var, pk.value])
        script_ref.add_command('GETCOURSE', 3, 3, self._cmd_get_course, [pk.var, pk.value, pk.value])
        script_ref.add_command('GETDATE', 1, 1, self._cmd_get_date, [pk.var])
        script_ref.add_command('GETDISTANCE', 3, 3, self._cmd_get_distance, [pk.var, pk.value, pk.value])
        script_ref.add_command('GETINPUT', 2, 3, self._cmd_get_input, [pk.var, pk.value])
        script_ref.add_command('GETLENGTH', 2, 2, self._cmd_get_length, [pk.value, pk.var])
        script_ref.add_command('GETMENUVALUE', 2, 2, self._cmd_get_menu_value, [pk.value, pk.value])
        script_ref.add_command('GETOUTTEXT', 1, 1, self._cmd_get_out_text, [pk.var])
        script_ref.add_command('GETRND', 3, 3, self._cmd_get_rnd, [pk.var, pk.value, pk.value])
        script_ref.add_command('GETSECTOR', 2, 2, self._cmd_get_sector, [pk.value, pk.var])
        script_ref.add_command('GETSECTORPARAMETER', 3, 3, self._cmd_get_sector_parameter, [pk.value, pk.value, pk.var])
        script_ref.add_command('GETTEXT', 4, 4, self._cmd_get_text, [pk.value, pk.var, pk.value, pk.value])
        script_ref.add_command('GETTIME', 1, 2, self._cmd_get_time, [pk.var, pk.value])
        script_ref.add_command('GOSUB', 1, 1, self._cmd_gosub, [pk.value])
        script_ref.add_command('GOTO', 1, 1, self._cmd_goto, [pk.value])
        script_ref.add_command('GETWORD', 3, 4, self._cmd_get_word, [pk.value, pk.var, pk.value])
        script_ref.add_command('GETWORDPOS', 3, 3, self._cmd_get_word_pos, [pk.value, pk.var, pk.value])
        script_ref.add_command('HALT', 0, 0, self._cmd_halt, [])
        script_ref.add_command('ISEQUAL', 3, 3, self._cmd_is_equal, [pk.var, pk.value, pk.value])
        script_ref.add_command('ISGREATER', 3, 3, self._cmd_is_greater, [pk.var, pk.value, pk.value])
        script_ref.add_command('ISGREATEREQUAL', 3, 3, self._cmd_is_greater_equal, [pk.var, pk.value, pk.value])
        script_ref.add_command('ISLESSER', 3, 3, self._cmd_is_lesser, [pk.var, pk.value, pk.value])
        script_ref.add_command('ISLESSEREQUAL', 3, 3, self._cmd_is_lesser_equal, [pk.var, pk.value, pk.value])
        script_ref.add_command('ISNOTEQUAL', 3, 3, self._cmd_is_not_equal, [pk.var, pk.value, pk.value], pk.value)
        script_ref.add_command('ISNUMBER', 2, 2, self._cmd_is_number, [pk.var, pk.value])
        script_ref.add_command('KILLWINDOW', 1, 1, self._cmd_kill_window, [pk.value])
        script_ref.add_command('KILLALLTRIGGERS', 0, 0, self._cmd_kill_all_triggers, [])
        script_ref.add_command('KILLTRIGGER', 1, 1, self._cmd_kill_trigger, [pk.value])
        script_ref.add_command('LOAD', 1, 1, self._cmd_load, [pk.value])
        script_ref.add_command('LOADVAR', 1, 1, self._cmd_load_var, [pk.var])
        script_ref.add_command('LOGGING', 1, 1, self._cmd_logging, [pk.value])
        script_ref.add_command('LOWERCASE', 1, 1, self._cmd_lowercase, [pk.value])
        script_ref.add_command('MERGETEXT', 3, 3, self._cmd_merge_text, [pk.value, pk.value, pk.var])
        script_ref.add_command('MULTIPLY', 2, 2, self._cmd_multiply, [pk.var, pk.value])
        script_ref.add_command('OPENMENU', 1, 2, self._cmd_open_menu, [pk.value, pk.value])
        script_ref.add_command('OR', 2, 2, self._cmd_or, [pk.var, pk.value])
        script_ref.add_command('PAUSE', 0, 0, self._cmd_pause, [])
        script_ref.add_command('PROCESSIN', 2, 2, self._cmd_process_in, [pk.value, pk.value])
        script_ref.add_command('PROCESSOUT', 1, 1, self._cmd_process_out, [pk.value])
        script_ref.add_command('READ', 3, 3, self._cmd_read, [pk.value, pk.var, pk.value])
        script_ref.add_command('RENAME', 2, 2, self._cmd_rename, [pk.value, pk.value])
        script_ref.add_command('REPLACETEXT', 3, 3, self._cmd_replace_text, [pk.var, pk.value, pk.value])
        script_ref.add_command('REQRECORDING', 0, 0, self._cmd_req_recording, [])
        script_ref.add_command('RETURN', 0, 0, self._cmd_return, [])
        script_ref.add_command('ROUND', 1, 2, self._cmd_round, [pk.var, pk.value])
        script_ref.add_command('SAVEVAR', 1, 1, self._cmd_save_var, [pk.var])
        script_ref.add_command('SEND', 1, -1, self._cmd_send, [pk.value])
        script_ref.add_command('SETARRAY', 2, -1, self._cmd_set_array, [pk.var, pk.value])
        script_ref.add_command('SETDELAYTRIGGER', 3, 3, self._cmd_set_delay_trigger, [pk.value, pk.value, pk.value])
        script_ref.add_command('SETEVENTTRIGGER', 3, 4, self._cmd_set_event_trigger, [pk.value, pk.value, pk.value])
        script_ref.add_command('SETMENUHELP', 2, 2, self._cmd_set_menu_help, [pk.value, pk.value])
        script_ref.add_command('SETMENUVALUE', 2, 2, self._cmd_set_menu_value, [pk.value, pk.value])
        script_ref.add_command('SETMENUOPTIONS', 4, 4, self._cmd_set_menu_options, [pk.value, pk.value, pk.value])
        script_ref.add_command('SETPRECISION', 1, 1, self._cmd_set_precision, [pk.value])
        script_ref.add_command('SETPROGVAR', 2, 2, self._cmd_set_prog_var, [pk.value, pk.value])
        script_ref.add_command('SETSECTORPARAMETER', 3, 3, self._cmd_set_sector_parameter, [pk.value, pk.value, pk.value])
        script_ref.add_command('SETTEXTLINETRIGGER', 2, 3, self._cmd_set_text_line_trigger, [pk.value, pk.value, pk.value])
        script_ref.add_command('SETTEXTOUTTRIGGER', 2, 3, self._cmd_set_text_out_trigger, [pk.value, pk.value, pk.value])
        script_ref.add_command('SETTEXTTRIGGER', 2, 3, self._cmd_set_text_trigger, [pk.value, pk.value, pk.value])
        script_ref.add_command('SETVAR', 2, 2, self._cmd_set_var, [pk.var, pk.value])
        script_ref.add_command('SETWINDOWCONTENTS', 2, 2, self._cmd_set_window_contents, [pk.value, pk.value])
        script_ref.add_command('SOUND', 1, 1, self._cmd_sound, [pk.value])
        script_ref.add_command('STOP', 1, 1, self._cmd_stop, [pk.value])
        script_ref.add_command('STRIPTEXT', 2, 2, self._cmd_strip_text, [pk.var, pk.value])
        script_ref.add_command('SUBTRACT', 2, 2, self._cmd_subtract, [pk.var, pk.value])
        script_ref.add_command('SYS_CHECK', 0, 0, self._cmd_sys_check, [pk.value])
        script_ref.add_command('SYS_FAIL', 0, 0, self._cmd_sys_fail, [pk.value])
        script_ref.add_command('SYS_KILL', 0, 0, self._cmd_sys_kill, [pk.value])
        script_ref.add_command('SYS_NOAUTH', 0, 0, self._cmd_sys_no_auth, [pk.value])
        script_ref.add_command('SYS_NOP', 0, 0, self._cmd_sys_nop, [pk.value])
        script_ref.add_command('SYS_SHOWMSG', 0, 0, self._cmd_sys_show_msg, [pk.value])
        script_ref.add_command('SYSTEMSCRIPT', 0, 0, self._cmd_system_script, [])
        script_ref.add_command('UPPERCASE', 1, 1, self._cmd_uppercase, [pk.var])
        script_ref.add_command('XOR', 2, 2, self._cmd_xor, [pk.var, pk.value])
        script_ref.add_command('WAITFOR', 1, 1, self._cmd_wait_for, [pk.value])
        script_ref.add_command('WINDOW', 4, 5, self._cmd_window, [pk.value] * 5)
        script_ref.add_command('WRITE', 2, 2, self._cmd_write, [pk.value, pk.value])
        # Commands added for 2.04beta
        script_ref.add_command('GETTIMER', 1, 1, self._cmd_get_timer, [pk.var])
        script_ref.add_command('READTOARRAY', 2, 2, self._cmd_read_to_array, [pk.value, pk.value])
        # Commands added for 2.04final
        script_ref.add_command('CLEARALLAVOIDS', 0, 0, self._cmd_clear_all_avoids, [])
        script_ref.add_command('CLEARAVOID', 1, 1, self._cmd_clear_avoid, [pk.value])
        script_ref.add_command('GETALLCOURSES', 2, 2, self._cmd_get_all_courses, [pk.var, pk.value])
        script_ref.add_command('GETFILELIST', 1, 2, self._cmd_multiply, [pk.var, pk.value])
        script_ref.add_command('GETNEARESTWARPS', 2, 2, self._cmd_get_nearest_warps, [pk.var, pk.value])
        script_ref.add_command('GETSCRIPTVERSION', 2, 2, self._cmd_get_script_version, [pk.value, pk.var])
        script_ref.add_command('LISTACTIVESCRIPTS', 1, 1, self._cmd_list_active_scripts, [pk.var])
        script_ref.add_command('LISTAVOIDS', 1, 1, self._cmd_list_avoids, [pk.var])
        script_ref.add_command('LISTSECTORPARAMETERS', 2, 2, self._cmd_list_sector_parameters, [pk.value, pk.value])
        script_ref.add_command('SETAVOID', 1, 1, self._cmd_set_avoid, [pk.value])
        # Commands added for 2.05beta
        script_ref.add_command('CUTLENGTHS', 3, 3, self._cmd_cut_lengths, [pk.value, pk.var, pk.value])
        script_ref.add_command('FORMAT', 3, 3, self._cmd_format, [pk.value, pk.var, pk.value])
        script_ref.add_command('GETDIRLIST', 1, 2, self._cmd_get_dir_list, [pk.var, pk.value])
        script_ref.add_command('GETWORDCOUNT', 2, 2, self._cmd_get_word_count, [pk.value, pk.var])
        script_ref.add_command('MAKEDIR', 1, 1, self._cmd_make_dir, [pk.value])
        script_ref.add_command('PADLEFT', 2, 2, self._cmd_pad_left, [pk.var, pk.value])
        script_ref.add_command('PADRIGHT', 2, 2, self._cmd_pad_right, [pk.var, pk.value])
        script_ref.add_command('REMOVEDIR', 1, 1, self._cmd_remove_dir, [pk.value])
        script_ref.add_command('SETMENUKEY', 1, 1, self._cmd_set_menu_key, [pk.value])
        script_ref.add_command('SPLITTEXT', 2, 3, self._cmd_split_text, [pk.value, pk.var])
        script_ref.add_command('TRIM', 1, 1, self._cmd_trim, [pk.var])
        script_ref.add_command('TRUNCATE', 1, 1, self._cmd_truncate, [pk.var])

    def build_sys_const_list(self, script_ref: ScriptRef):
        for i, ansi in enumerate(ANSI):
            script_ref.add_sys_constant(f'ANSI_{i}', ansi)

        script_ref.add_sys_constant('FALSE', '0')
        script_ref.add_sys_constant('TRUE', '1')
        script_ref.add_sys_constant('DATE', lambda _: datetime.now().strftime("%d/%m/%Y"))

        '''
    AddSysConstant('CONNECTED', SCConnected);
    AddSysConstant('CURRENTANSILINE', SCCurrentANSILine);
    AddSysConstant('CURRENTLINE', SCCurrentLine);
    AddSysConstant('DATE', SCDate);
    AddSysConstant('FALSE', SCFalse);
    AddSysConstant('GAME', SCGame);
    AddSysConstant('GAMENAME', SCGameName);
    AddSysConstant('LICENSENAME', SCLicenseName);
    AddSysConstant('LOGINNAME', SCLoginName);
    AddSysConstant('PASSWORD', SCPassword);
    AddSysConstant('PORT.CLASS', SCPort_Class);
    AddSysConstant('PORT.BUYFUEL', SCPort_BuyFuel);
    AddSysConstant('PORT.BUYORG', SCPort_BuyOrg);
    AddSysConstant('PORT.BUYEQUIP', SCPort_BuyEquip);
    AddSysConstant('PORT.EXISTS', SCPort_Exists);
    AddSysConstant('PORT.FUEL', SCPort_Fuel);
    AddSysConstant('PORT.NAME', SCPort_Name);
    AddSysConstant('PORT.ORG', SCPort_Org);
    AddSysConstant('PORT.EQUIP', SCPort_Equip);
    AddSysConstant('PORT.PERCENTFUEL', SCPort_PercentFuel);
    AddSysConstant('PORT.PERCENTORG', SCPort_PercentOrg);
    AddSysConstant('PORT.PERCENTEQUIP', SCPort_PercentEquip);
    AddSysConstant('SECTOR.ANOMALY', SCSector_Anomaly);
    AddSysConstant('SECTOR.BACKDOORCOUNT', SCSector_BackDoorCount);
    AddSysConstant('SECTOR.BACKDOORS', SCSector_BackDoors);
    AddSysConstant('SECTOR.DENSITY', SCSector_Density);
    AddSysConstant('SECTOR.EXPLORED', SCSector_Explored);
    AddSysConstant('SECTOR.FIGS.OWNER', SCSector_Figs_Owner);
    AddSysConstant('SECTOR.FIGS.QUANTITY', SCSector_Figs_Quantity);
    AddSysConstant('SECTOR.LIMPETS.OWNER', SCSector_Limpets_Owner);
    AddSysConstant('SECTOR.LIMPETS.QUANTITY', SCSector_Limpets_Quantity);
    AddSysConstant('SECTOR.MINES.OWNER', SCSector_Mines_Owner);
    AddSysConstant('SECTOR.MINES.QUANTITY', SCSector_Mines_Quantity);
    AddSysConstant('SECTOR.NAVHAZ', SCSector_Navhaz);
    AddSysConstant('SECTOR.PLANETCOUNT', SCSector_PlanetCount);
    AddSysConstant('SECTOR.PLANETS', SCSector_Planets);
    AddSysConstant('SECTOR.SHIPCOUNT', SCSector_ShipCount);
    AddSysConstant('SECTOR.SHIPS', SCSector_Ships);
    AddSysConstant('SECTOR.TRADERCOUNT', SCSector_TraderCount);
    AddSysConstant('SECTOR.TRADERS', SCSector_Traders);
    AddSysConstant('SECTOR.UPDATED', SCSector_Updated);
    AddSysConstant('SECTOR.WARPCOUNT', SCSector_WarpCount);
    AddSysConstant('SECTOR.WARPS', SCSector_Warps);
    AddSysConstant('SECTOR.WARPSIN', SCSector_WarpsIn);
    AddSysConstant('SECTOR.WARPINCOUNT', SCSector_WarpInCount);
    AddSysConstant('SECTORS', SCSectors);
    AddSysConstant('STARDOCK', SCStardock);
    AddSysConstant('TIME', SCTime);
    AddSysConstant('TRUE', SCTrue);
    // SysConstants added in 2.04
    AddSysConstant('ALPHACENTAURI', SCAlphaCentauri);
    AddSysConstant('CURRENTSECTOR', SCCurrentSector);
    AddSysConstant('RYLOS', SCRylos);
    // Added in 2.04a
    AddSysConstant('PORT.BUILDTIME', SCPort_BuildTime);
    AddSysConstant('PORT.UPDATED', SCPort_Updated);
    AddSysConstant('RAWPACKET', SCRAWPACKET);
    AddSysConstant('SECTOR.BEACON', SCSector_Beacon);
    AddSysConstant('SECTOR.CONSTELLATION', SCSector_Constellation);
    AddSysConstant('SECTOR.FIGS.TYPE', SCSector_Figs_Type);
        :return: 
        '''

    def _cmd_add(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        """
        CMD: add var <value>
        add a value to a variable
        """
        f1 = params[0].dec_value
        f2 = params[1].dec_value
        self._update_param(params[0], f1 + f2, script.decimal_precision)
        return CmdAction.none

    def _cmd_add_menu(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        """
        CMD: addMenu <parent> <name> <description> <hotkey> <reference> <prompt> <closeMenu>
        """
        if len(params[3].value) != 1:
            raise ScriptError("Bad menu hotkey")

        label_name = params[4].value

        if label_name != '':
            label_name = script._cmp.extend_label_name(label_name, script.exec_script)

        script.add_menu(self._menu.add_custom_menu(
            params[0].value.upper(),
            params[1].value.upper(),
            params[2].value,
            label_name,
            params[5].value,
            params[3].value[0].upper(),
            params[6].value == '1',
            script))
        return CmdAction.none

    def _cmd_and(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        """
        CMD: and var <value>
        """
        b1 = _convert_to_boolean(params[0])
        b2 = _convert_to_boolean(params[1])

        params[0].value = _convert_bool_to_string(b1 and b2)
        return CmdAction.none

    def _cmd_branch(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        if params[0].dec_value == 1 or int(params[0].dec_value) == 1:
            # should short-circuit
            pass
        else:
            script.goto_label(params[1].value)
        return CmdAction.none

    def _cmd_clear_all_avoids(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: clearAllAvoids
        self._db.unset_all_voids()
        return CmdAction.none

    def _cmd_client_message(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: ClientMessage <value>

        self._server.client_message(params[0].value)
        return CmdAction.none

    def _cmd_close_menu(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: closeMenu
        self._menu.close_menu(False)
        return CmdAction.none

    def _cmd_connect(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: connect
        # no-op
        return CmdAction.none

    def _cmd_cut_text(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: cutText <value> var <start> <length>
        v1 = self._convert_to_number(params[2].value)
        v2 = self._convert_to_number(params[3].value)

        if v1 > len(params[0].value):
            raise ScriptError("CutText: Start position beyond End Of Line")

        params[1].value = params[0].value[v1 - 1:v1 - 1 + v2]
        return CmdAction.none

    def _cmd_delete(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: delete <filename>
        # todo: solve files
        return CmdAction.none

    def _cmd_disconnect(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: disconnect
        # no-op
        return CmdAction.none

    def _cmd_divide(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: divide var <value>
        # divide variable by a value

        f2 = params[1].dec_value
        if f2 == 0:
            raise ScriptError("Division by zero")
        f1 = params[0].dec_value

        self._update_param(params[0], f1 / f2, script.decimal_precision)
        return CmdAction.none

    def _cmd_echo(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: echo <values...>

        # string together the parameters and echo to all terms
        echo_text = "".join(p.value for p in params)

        # #13 on its own will warp the terminal display - add a linefeed with it
        self._server.broadcast(echo_text.replace("\r", "\r\n"))
        return CmdAction.none

    def _cmd_file_exists(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: fileExists var <filename>
        # todo: solve files
        return CmdAction.none

    def _cmd_format(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: format InputVar OutputVar CONSTANT
        format_type = params[2].value
        if format_type == "CURRENCY":  # ($4,567.22)
            try:
                params[1].value = "${:,.2f}".format(params[0].dec_value)
            except ValueError:
                raise ScriptError("Invalid Currency value")
        elif format_type == "NUMBER":  # (4,567.(PRECISION))
            try:
                params[1].value = '{:,.{prec}f}'.format(params[0].dec_value, prec=script.decimal_precision)
            except ValueError:
                raise ScriptError("Invalid Number value")
        elif format_type == "DATETIMETOSTR":
            try:
                d = datetime.fromtimestamp(params[0].dec_value)
                params[1].value = format_datetime(d)
            except ValueError:
                # Broadcast the exception
                raise ScriptError("Invalid DateTime value")
        elif format_type == "STRTODATETIME":
            try:
                try:
                    out_var = datetime.strptime(params[0].value, "%x %X")
                except ValueError:
                    out_var = datetime.strptime(params[0].value, "%x")
                self._update_param(params[1], out_var.timestamp(), 15)
            except ValueError:
                raise ScriptError("Invalid String value for DateTime conversion")

        return CmdAction.none

    def _cmd_get_char_code(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getCharCode <char> resultVar

        if len(params[0].value) != 1:
            raise ScriptError("Bad character")

        params[1].value = str(ord(params[0].value[0]))
        return CmdAction.none

    def _cmd_get_console_input(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getConsoleInput var [singleKey?]
        script.locked = True
        self._menu.begin_script_input(script, params[0], len(params) == 2)

        return CmdAction.pause

    def _cmd_get_course(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getCourse varspec <fromSector> <toSector>
        from_sect = self._convert_to_number(params[1].value)
        to_sect = self._convert_to_number(params[2].value)

        self._check_sector(from_sect)
        self._check_sector(to_sect)

        course = self._db.plot_warp_course(from_sect, to_sect)

        var_param: VarParam = params[0]
        params[0].value = str(len(course) - 1)

        if course:
            # Need to reverse the lists order
            course.reverse()
            var_param.set_array_from_list(course)

        return CmdAction.none

    def _cmd_get_date(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getDate var

        if len(params) == 2:
            params[0].value = format_datetime(params[1].value or datetime.now())
        else:
            params[0].value = format_datetime(datetime.now())

        return CmdAction.none

    def _cmd_get_distance(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getDistance var <fromSector> <toSector>

        from_sect = self._convert_to_number(params[1].value)
        to_sect = self._convert_to_number(params[2].value)

        self._check_sector(from_sect)
        self._check_sector(to_sect)

        course = self._db.plot_warp_course(from_sect, to_sect)
        params[0].value = (str(len(course) - 1))

        return CmdAction.none

    def _cmd_get_input(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getInput var <prompt>

        script.locked = True
        self._menu.begin_script_input(script, params[0], False, prompt=params[1].value)

        return CmdAction.pause

    def _cmd_get_length(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getLength <text> var

        params[1].value = str(len(params[0].value))
        return CmdAction.none

    def _cmd_get_menu_value(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getMenuValue <menuName> var

        try:
            params[1].value = self._menu.get_menu_by_name(params[0].value.upper()).value
        except Exception as e:
            raise ScriptError(e.value)

        return CmdAction.none

    def _cmd_get_nearest_warps(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getNearestWarps <ArrayName> <StartingSector>

        start_sect = self._convert_to_number(params[1].value)
        self._check_sector(start_sect)
        params[0].value = self._db.plot_nearest_warps(start_sect)

        return CmdAction.none

    def _cmd_get_out_text(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getOutText var

        params[0].value = script.out_text
        return CmdAction.none

    def _cmd_get_rnd(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getRnd var <lowestValue> <highestValue>
        min_val = _convert_to_number(params[1].value)
        max_val = _convert_to_number(params[2].value)
        params[0].value = randint(min_val, max_val)
        return CmdAction.none

    def _cmd_get_sector(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getSector <index> var
        index = self._convert_to_number(params[0].value)
        self._check_sector(index)
        s = self._db.load_sector(index)

        var = params[1].name

        def sv(suffix, val, index: int = 0):
            script.set_variable(f"{var}.{suffix.upper()}", str(val), '' if not index else str(index))

        if s.explored == SectorExploredType.no:
            sv("explored", "NO")
        elif s.explored == SectorExploredType.calc:
            sv("explored", "CALC")
        elif s.explored == SectorExploredType.density:
            sv("explored", "DENSITY")
        elif s.explored == SectorExploredType.holo:
            sv("explored", "YES")

        sv("index", str(index))
        sv("beacon", s.beacon)
        sv("constellation", s.constellation)
        sv("armidmines.quantity", s.mines_armid.quantity)
        sv("limpetmines.quantity", s.mines_limpet.quantity)
        sv("armidmines.owner", s.mines_armid.owner)
        sv("limpetmines.owner", s.mines_limpet.owner)
        sv("figs.quantity", s.figs.quantity)
        sv("figs.owner", s.figs.owner)
        sv("warps", s.warps)
        sv("density", s.density)
        sv('navhaz', s.navhaz)
        for idx, w in enumerate(s.warp):
            sv("warp", w, idx + 1)

        sv("updated", s.update.strftime("%x %X"))
        sv("port.name", s.port.name)

        if s.figs.fig_type == FighterType.toll:
            sv("figs.type", "TOLL")
        elif s.figs.fig_type == FighterType.defensive:
            sv("figs.type", "DEFENSIVE")
        else:
            sv("figs.type", "OFFENSIVE")

        sv("anomaly", "YES" if s.anomaly else "NO")

        if not s.port or not s.port.name:
            sv("port.class", "0")
            sv("port.exists", "0")
        else:
            sv("port.class", s.port.class_index)
            sv("port.exists", "1")
            sv("port.buildtime", s.port.build_time)
            sv("port.perc_ore", s.port.product_percent[ProductType.fuel_ore])
            sv("port.perc_org", s.port.product_percent[ProductType.organics])
            sv("port.perc_equip", s.port.product_percent[ProductType.equipment])
            sv("port.ore", s.port.product_amount[ProductType.fuel_ore])
            sv("port.org", s.port.product_amount[ProductType.organics])
            sv("port.equip", s.port.product_amount[ProductType.equipment])
            sv("port.updated", s.port.update.strftime("%x %X"))
            sv("port.buy_ore", "YES" if s.port.buy_product[ProductType.fuel_ore] else "NO")
            sv("port.buy_org", "YES" if s.port.buy_product[ProductType.organics] else "NO")
            sv("port.buy_equip", "YES" if s.port.buy_product[ProductType.equipment] else "NO")

        sv("planets", len(s.planets))
        for idx, t in enumerate(s.planets):
            sv("planet", t.name, idx + 1)
        
        sv("traders", len(s.traders))
        for idx, t in enumerate(s.traders):
            sv("trader.name", t.name, idx + 1)
            sv("trader.shipname", t.ship_name, idx + 1)
            sv("trader.ship", t.ship_type, idx + 1)
            sv("trader.figs", t.figs, idx + 1)
            
        sv("ships", len(s.ships))
        for idx, x in enumerate(s.ships):
            sv("ship.name", x.name, idx + 1)
            sv("ship.owner", x.owner, idx + 1)
            sv("ship.ship", x.ship_type, idx + 1)
            sv("ship.figs", x.figs, idx + 1)

        if s.explored != SectorExploredType.no:
            for idx, b in enumerate(self._db.get_backdoors(s)):
                sv("backdoor", b.id, idx + 1)
            
        return CmdAction.none

    def _cmd_get_sector_parameter(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getSectorParameter <sectorIndex> <parameterName> var

        index = _convert_to_number(params[0].value)
        self._check_sector(index)
        if len(params[1].value) > 10:
            raise ScriptError("Sector parameter name cannot be longer than 10 characters")

        if len(params[2].value) > 40:
            raise ScriptError("Sector parameter value cannot be longer than 40 characters")

        params[2].value = self._db.get_sector_var(index, params[1].value)
        return CmdAction.none

    def _cmd_get_text(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getText <line> var <startValue> <endValue>

        line = params[0].value
        start_str = params[2].value
        end_str = params[3].value

        if start_str == '':
            start_pos = 0
        else:
            try:
                start_pos = line.index(start_str)
            except ValueError:
                params[1].value = ''
                return CmdAction.none

        line = line[start_pos + len(start_str):]

        if end_str == '':
            end_pos = len(line)
        else:
            try:
                end_pos = line.index(end_str)
            except ValueError:
                params[1].value = ''
                return CmdAction.none

        params[1].value = line[:end_pos]
        return CmdAction.none

    def _cmd_get_time(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getTime var [<format>]

        if len(params) == 2:
            raise NotImplementedError("custom format of time not supported")
        else:
            params[0].value = datetime.now().strftime("%H:%M:%S")

        return CmdAction.none

    def _cmd_gosub(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: gosub <label>

        script.gosub(params[0].value)
        return CmdAction.none

    def _cmd_goto(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: goto <label>

        script.goto_label(params[0].value)

        return CmdAction.none

    def _cmd_get_word(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getWord <line> var <index> <default>

        index = _convert_to_number(params[2].value)
        words = params[0].value.split()

        if index > len(words):
            if len(params) > 3:
                params[1].value = params[3].value
            else:
                params[1].value = '0'
        else:
            params[1].value = words[index - 1]
        return CmdAction.none

    def _cmd_get_word_pos(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getWordPos <text> storageVar <subString>
        try:
            params[1].value = str(params[0].value.index(params[2].value) + 1)
        except ValueError:
            params[1].value = '0'
        return CmdAction.none

    def _cmd_halt(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: halt

        return CmdAction.stop

    def _cmd_is_equal(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: isEqual var <value1> <value2>

        try:
            val = abs(params[1].dec_value - params[2].dec_value) <= self._max_float_variance
            params[0].set_bool(val)
        except ScriptError:
            params[0].set_bool(params[1].value == params[2].value)

        return CmdAction.none

    def _cmd_is_greater(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: isGreaterEqual var <value1> <value2>
        # var = 1 if <value1> > <value2> else var = 0

        params[0].set_bool((params[1].dec_value - self._max_float_variance) > params[2].dec_value)
        return CmdAction.none

    def _cmd_is_greater_equal(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: isGreaterEqual var <value1> <value2>
        # var = 1 if <value1> >= <value2> else var = 0

        params[0].set_bool((params[1].dec_value - self._max_float_variance) >= params[2].dec_value or
                           abs(params[1].dec_value - params[2].dec_value) <= self._max_float_variance)

        return CmdAction.none

    def _cmd_is_lesser(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: isLesser var <value1> <value2>
        # var = 1 if <value1> < <value2> else var = 0

        params[0].set_bool((params[1].dec_value + self._max_float_variance) < params[2].dec_value)
        return CmdAction.none

    def _cmd_is_lesser_equal(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: isLesserEqual var <value1> <value2>
        # var = 1 if <value1> <= <value2> else var = 0

        params[0].set_bool((params[1].dec_value + self._max_float_variance) <= params[2].dec_value or
                           abs(params[1].dec_value - params[2].dec_value) <= self._max_float_variance)

        return CmdAction.none

    def _cmd_is_not_equal(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        """
        CMD: isNotEqual var <value1> <value2>
        var = 1 if <value1> <> <value2> else var = 0
        """
        try:
            val = abs(params[1].dec_value - params[2].dec_value) > self._max_float_variance
            params[0].set_bool(val)
        except ScriptError as e:
            # Float comparison failed, try string comparison
            params[0].set_bool(params[1].value == params[2].value is False)

        return CmdAction.none

    def _cmd_is_number(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: isNumber var value1
        try:
            _ = params[1].dec_value
            params[0].dec_value = 1
        except:
            params[0].dec_value = 0
        return CmdAction.none

    def _cmd_kill_window(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: killWindow <windowName>
        raise ScriptError("Not implemented")

    def _cmd_kill_all_triggers(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: killAllTriggers
        script.kill_all_triggers()
        return CmdAction.none

    def _cmd_kill_trigger(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: killTrigger <name>
        script.kill_trigger(params[0].value)
        return CmdAction.none

    def _cmd_load(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: loadVar var
        script.owner.load(params[0].value, False)
        return CmdAction.none

    def _cmd_load_var(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: loadVar var

        params[0].value = self._db.get_var(params[0].name, '0')
        return CmdAction.none

    def _cmd_logging(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: logging <value>
        script.log_data = params[0].value in ("ON", "on", "1")
        return CmdAction.none

    def _cmd_lowercase(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: lowerCase var
        params[0].value = params[0].value.lower()

        return CmdAction.none

    def _cmd_merge_text(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: mergeText <value1> <value2> var
        params[2].value = params[0].value + params[1].value
        return CmdAction.none

    def _cmd_multiply(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: multiply var <value>
        # multiply a variable by a value

        f1 = params[0].dec_value
        f2 = params[1].dec_value
        self._update_param(params[0], f1 * f2, script.decimal_precision)

        return CmdAction.none

    def _cmd_open_menu(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: openMenu <name> [<pause>]
        # Eg: openMenu TWX_SHOWSPECIALPORT FALSE
        raise ScriptError("Not implemented")

    def _cmd_or(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: or var <value>
        b1 = _convert_to_boolean(params[0])
        b2 = _convert_to_boolean(params[1])
        params[0].value = _convert_bool_to_string(b1 or b2)
        return CmdAction.none

    def _cmd_pause(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: pause
        return CmdAction.pause

    def _cmd_process_in(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: processIn processType <text>
        raise ScriptError("Not implemented")

    def _cmd_process_out(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: processOut <text>
        if not script.owner.text_out_event(params[0].value, script):
            self._server.send(params[0].value.encode("ascii"))
        return CmdAction.none

    def _cmd_read(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: read <file> storageVar <line>
        raise ScriptError("Not implemented")

    def _cmd_rename(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: rename <oldfile> <newfile>
        raise ScriptError("Not implemented")

    def _cmd_replace_text(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: replaceText var <oldText> <newText>
        params[0].value = params[0].value.replace(params[1].value, params[2].value)
        return CmdAction.none

    def _cmd_req_recording(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: reqRecording
        raise ScriptError("Not implemented")

    def _cmd_return(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: return
        script.do_return()
        return CmdAction.none

    def _cmd_round(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: round var <precision>
        params[0].value = str(round(params[0].dec_value, int(params[1].dec_value) if len(params) > 1 else 0))
        # todo: copy over more advanced rounding algorithm from twxproxy
        return CmdAction.none

    def _cmd_save_var(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: saveVar var
        self._db.save_var(params[0].name, params[0].value)
        return CmdAction.none

    def _cmd_send(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: send <values...>
        # string together the parameters and echo to all terms

        self._server.send("".join([p.value for p in params]).encode('ascii'))
        return CmdAction.none

    def _cmd_set_array(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setArray var <dimensions...>
        dimensions: List[int] = [0] * (len(params) - 1)
        for i in range(1, len(params)):
            val_int = self._convert_to_number(params[i].value)
            dimensions[i - 1] = val_int

        params[0].set_array(dimensions)
        return CmdAction.none

    def _cmd_set_delay_trigger(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setDelayTrigger <name> <label> <tics>

        val = _convert_to_number(params[2].value)
        script.set_delay_trigger(params[0].value, params[1].value, val)
        return CmdAction.none

    def _cmd_set_event_trigger(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setEventTrigger <name> <label> <event> [<parameter>]
        if len(params) < 4:
            param = ""
        else:
            param = params[3].value
        script.set_event_trigger(params[0].value, params[1].value, params[2].value, param)
        return CmdAction.none

    def _cmd_set_menu_help(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setMenuHelp <menuName> <helpText>

        raise ScriptError("Not implemented")

    def _cmd_set_menu_value(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setMenuValue <menuName> <value>
        raise ScriptError("Not implemented")

    def _cmd_set_menu_options(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setMenuOptions <menuName> <Q> <?> <+>

        raise ScriptError("Not implemented")

    def _cmd_set_precision(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setPrecision <value>

        val = self._convert_to_number(params[0].value)
        script.decimal_precision = val

        # EP
        self._last_precision = self._set_precision
        self._set_precision = val
        self._last_multiplier = pow(self._set_precision, 10)
        if self._set_precision == 0:
            self._max_float_variance = 0
        else:
            # EP: Effectively half of the next decimal beyond Precision
            self._max_float_variance = 0.5 / self._last_multiplier

        return CmdAction.none

    def _cmd_set_prog_var(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setProgVar <varName> <value>
        raise ScriptError("Not implemented")

    def _cmd_set_sector_parameter(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setSectorParameter <sectorIndex> <parameterName> <value>
        index = _convert_to_number(params[0].value)
        self._check_sector(index)

        if len(params[1].value) > 10:
            raise ScriptError("Sector parameter name cannot be longer than 10 characters")
        if len(params[2].value) > 40:
            raise ScriptError("Sector parameter value cannot be longer than 40 characters")
        self._db.set_sector_var(index, params[1].value, params[2].value)
        return CmdAction.none

    def _cmd_set_text_line_trigger(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setTextLineTrigger <name> <label> [<value>]

        """
                if (Length(Params) < 3) then
            Value := ''
          else
            Value := Params[2].Value;

          TScript(Script).SetTextLineTrigger(Params[0].Value, Params[1].Value, Value);
                """
        if len(params) < 3:
            value = ""
        else:
            value = params[2].value

        script.set_text_line_trigger(params[0].value, params[1].value, value)
        return CmdAction.none

    def _cmd_set_text_out_trigger(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setTextOutTrigger <name> <label> [<value>]

        if len(params) < 3:
            value = ''
        else:
            value = params[2].value

        script.set_text_out_trigger(params[0].value, params[1].value, value)
        return CmdAction.none

    def _cmd_set_text_trigger(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setTextTrigger <name> <label> [<value>]
        if len(params) < 3:
            value = ''
        else:
            value = params[2].value

        script.set_text_trigger(params[0].value, params[1].value, value)
        return CmdAction.none

    def _cmd_set_var(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        """
        CMD: setVar var <value>
        """
        if params[1].is_numeric:
            self._update_param(params[0], params[1].dec_value, params[1].sig_digits)
        else:
            params[0].value = params[1].value
        return CmdAction.none

    def _cmd_set_window_contents(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setWindowContents <windowName> <value>
        raise ScriptError("Not implemented")

    def _cmd_sound(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: sound <filename>
        raise ScriptError("Not implemented")

    def _cmd_stop(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: stop <scriptName>

        as_name = lambda path: splitext(basename(path))[0].upper()
        name = as_name(params[0].value)
        for x in range(0, self._interpreter.count):
            if as_name(self._interpreter.script(x)._cmp.script_file) == name:
                if self._interpreter.script(x) == script:
                    return CmdAction.stop
                else:
                    self._interpreter.stop(x)
                break

        return CmdAction.none

    def _cmd_strip_text(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: stripText var <value>
        value = params[0].value
        rem_text = params[1].value

        params[0].value = value.replace(rem_text, '')

        return CmdAction.none

    def _cmd_subtract(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        """
        CMD: subtract var <value>
        subtract a value from a variable
        """
        f1 = params[0].dec_value
        f2 = params[1].dec_value
        self._update_param(params[0], f1 - f2, script.decimal_precision)
        return CmdAction.none

    def _cmd_sys_check(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: sys_check
        return CmdAction.none

    def _cmd_sys_fail(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: sys_fail
        self._server.client_message(f'Unable to access subroutine - {ANSI_12}Authentication failure.')
        return CmdAction.stop

    def _cmd_sys_kill(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: sys_kill

        exit()  # will throw exceptions on exit - gives the user an impression of a serious bug out
        return CmdAction.stop

    def _cmd_sys_no_auth(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: sys_noAuth

        return CmdAction.none

    def _cmd_sys_nop(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: sys_nop

        return CmdAction.none

    def _cmd_sys_show_msg(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: sys_showMsg

        return CmdAction.none

    def _cmd_system_script(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: systemScript

        script.system = True

        return CmdAction.none

    def _cmd_uppercase(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: upperCase var
        params[0].value = params[0].value.upper()

        return CmdAction.none

    def _cmd_xor(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: xor var <value>

        b1 = _convert_to_boolean(params[0])
        b2 = _convert_to_boolean(params[1])

        params[0].value = _convert_bool_to_string(b1 ^ b2)

        return CmdAction.none

    def _cmd_wait_for(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: waitFor <value>
        script.wait_text = params[0].value
        script.wait_for_active = True

        return CmdAction.pause

    def _cmd_window(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: window <windowName> <sizeX> <sizeY> <title> [<ontop>]

        raise ScriptError("Not implemented")

    def _cmd_write(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: write <file> <value>
        raise ScriptError("Not implemented")

    def _cmd_get_timer(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getTimer

        # returns cpu ticks since restart
        raise ScriptError("Not implemented")

    def _cmd_read_to_array(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: readToArray <file> <storageArray>
        raise ScriptError("Not implemented")

    def _cmd_clear_avoid(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: clearAvoid <Sector>
        index = _convert_to_number(params[0].value)
        try:
            self._db.unset_void(index)
        except:
            raise ScriptError(f"'{index}' is not a sector number")

        return CmdAction.none

    def _cmd_get_all_courses(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: <2-DimensionArrayName> <StartingSector>

        return CmdAction.none

    def _cmd_get_file_list(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getFileList varArray <FileMask>
        raise ScriptError("Not implemented")

    def _cmd_get_script_version(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getScriptVersion <fileName> var

        if not isfile(params[0].value):
            raise ScriptError(f"File '{params[0].value}' not found")

        try:
            with open(params[0].value, 'rb') as f:
                hdr = ScriptFileHeader.from_bytes(f)
            if hdr.program_name != "TWX SCRIPT":
                raise ValueError()
            params[1].value = str(hdr.version)
        except:
            raise ScriptError("File is not a compiled TWX script")

        return CmdAction.none

    def _cmd_list_active_scripts(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: listActiveScripts <ArrayName>

        params[0].set_array_from_strings([s.script_name for s in script.owner.scripts])
        return CmdAction.none

    def _cmd_list_avoids(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: listAvoids <ArrayName>
        params[0].set_array_from_strings([str(a) for a in self._db.voids])
        return CmdAction.none

    def _cmd_list_sector_parameters(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: listSectorParameters <SectorIndex> <ArrayName>

        index = _convert_to_number(params[0].value)
        params[1].set_array_from_strings([str(a) for a in self._db.list_sector_vars(index)])
        return CmdAction.none

    def _cmd_set_avoid(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setAvoid <Sector>
        try:
            self._db.set_void(int(params[0].value))
        except ValueError:
            raise ScriptError(f"'{params[0].value}' is not a Sector number")
        return CmdAction.none

    def _cmd_cut_lengths(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: cutLengths <value> array <lengths1,length2,...>
        lengths = params[2].value.split(",")
        strings: List[str] = []
        position = 0
        i = 0
        while i < len(lengths) and position < len(params[0].value):
            int_length = int(lengths[i])
            strings.append(params[0].value[position:position+int_length])
            position += int_length
            i += 1

        arr: VarParam = params[1]
        arr.value = len(strings)
        arr.set_array_from_strings(strings)
        return CmdAction.none

    def _cmd_get_dir_list(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getDirList varArray <FileMask>
        raise ScriptError("Not implemented")

    def _cmd_get_word_count(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: getWordCount <text> storageVar

        params[1].value = str(len(params[0].value.split()))
        return CmdAction.none

    def _cmd_make_dir(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: MakeDir <directory name>

        raise ScriptError("Not implemented")

    def _cmd_pad_left(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: padLeft var <value>
        # add spaces to the left of a variable
        params[0].value = " " * int(params[1].dec_value) + params[0].value
        return CmdAction.none

    def _cmd_pad_right(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: padRight var <value>
        # add spaces to the right of a variable
        params[0].value += " " * int(params[1].dec_value)
        return CmdAction.none

    def _cmd_remove_dir(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: RemoveDir <directory name>
        raise ScriptError("Not implemented")

    def _cmd_set_menu_key(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: setMenuKey <value>

        raise ScriptError("Not implemented")

    def _cmd_split_text(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: splitText <text> varArray {delims}

        delims = params[2].value if len(params) > 2 else None
        strings = params[0].value.split(sep=delims)

        params[1].value = len(strings)
        params[1].set_array_from_strings(strings)

        return CmdAction.none

    def _cmd_trim(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: trim var

        params[0].value = params[0].value.strip()
        return CmdAction.none

    def _cmd_truncate(self, script: 'pytwx.script.Script', params: List[CmdParam]):
        # CMD: truncate var

        self._update_param(params[0], params[0].dec_value, 0)
        return CmdAction.none

    def _update_param(self, param: CmdParam, value: float, precision: int):
        if precision == 0:
            param.dec_value = int(value)
        else:
            param.dec_value = value

        param.sig_digits = precision  # EP - This will be needed for the FloatToText conversion


def _convert_to_boolean(param: CmdParam):
    if param.is_numeric:
        if param.dec_value == 0:
            return False
        elif param.dec_value == 1:
            return True
        else:
            raise ScriptError(f'Value must be either 0 or 1 (cannot be "{param.value}")')
    else:
        if param.value == '0':
            return False
        elif param.value == '1':
            return True
        else:
            raise ScriptError(f'Value must be either 0 or 1 (cannot be "{param.value}")')


def _convert_bool_to_string(b: bool) -> str:
    return '1' if b else '0'


def _convert_to_number(s: str):
    try:
        f = float(s)
        return round(f)
    except:
        raise ScriptError(f"'{s}' is not a number")

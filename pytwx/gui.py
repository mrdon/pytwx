import asyncio
from typing import Dict, Any

from pytwx.core import HistoryType
from pytwx.database import Sector


def _sector_to_data(index, sector):
    return {
        'id': int(index),
        'warps': [int(w) for w in sector.warp]
    }


class TwxGui:

    def __init__(self):
        self.events: asyncio.Queue = asyncio.Queue()
        self._current_sector_index = 0
        self._hashes = {}

    def add_to_history(self, history_type: HistoryType, value: str):
        pass

    def on_sector_update(self, sector: Sector, index: int):
        print("hash: {} contained {} existing: {}".format(sector.update, index in self._hashes, self._hashes.get(index,'nope')))
        if index not in self._hashes or self._hashes[index] != sector.update:
            self._hashes[index] = sector.update
            self.events.put_nowait(TwxEvent('sector', _sector_to_data(index, sector)))

    def on_current_sector_update(self, index: int):
        self._current_sector_index = index
        self.events.put_nowait(TwxEvent('currentSector', {'id': int(index)}))

    def add_script_menu(self, script: 'Script'):
        pass

    def remove_script_menu(self, script: 'Script'):
        pass


class TwxEvent:
    def __init__(self, name: str, data: Dict[str, Any]):
        self.name = name
        self.data = data


class MenuItem:
    pass
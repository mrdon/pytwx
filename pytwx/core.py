from enum import auto, Enum
from typing import Callable

from pytwx.utility import Pointer

endl = chr(13) + chr(10)


class HistoryType(Enum):
    fighter = auto()
    computer = auto()
    msg = auto()

NotificationEvent = Callable[[Pointer],None]


def post_notification(event: NotificationEvent, param: Pointer):
    '''
    New(PEvent);
  CopyMemory(PEvent, @@Event, SizeOf(TNotificationEvent));
  PostMessage(Application.Handle, WM_USER, Integer(PEvent), Integer(Param));
    '''
    pass
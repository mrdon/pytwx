import asyncio
import re
import struct
from io import FileIO
from typing import List, Callable


def get_parameters(s: str) -> List[str]:
    # Get text parameter
    if s == '':
        return ''

    return re.split('[ \x09]+', s.strip(" \x09"))


def get_parameter(s: str, parameter: int) -> str:
    params = get_parameters(s)
    return '' if len(params) <= parameter else params[parameter]
    #
    # p = 0
    # last = ' '
    # retn = ''
    #
    # for i in s:
    #     if p == parameter and i != ' ' and i != chr(9):
    #         retn = retn + i
    #
    #     if i == ' ' or i == chr(9) and last != ' ' and last != chr(9):
    #         p += 1
    #         if p > parameter:
    #             break
    #
    #     last = i
    #
    # return retn


def get_parameter_pos(s: str, parameter: int) -> int:
    # Get text parameter
    p = 0
    last = ' '

    i = 0
    for i in range(0, len(s)):
        if s[i] == ' ' and last != ' ':
            p += 1
        if p == parameter:
            break

        last = s[i]

    return i + 1


def strip_chars(s: str) -> str:
    return s.encode('ascii', errors='ignore').decode('ascii')


def fetch_script(s: str, include: bool) -> str:

    """
    var
  NameList : array[1..6] of string;
  Limit,
  I        : Integer;
begin
  I := 1;

  while (I <= 5) do
  begin
    NameList[I] := S;
    Inc(I, 2);
  end;

  I := 2;

  while (I <= 6) do
  begin
    NameList[I] := 'scripts\' + S;
    Inc(I, 2);
  end;

  CompleteFileName(NameList[1], 'ts');
  CompleteFileName(NameList[2], 'ts');
  CompleteFileName(NameList[3], 'cts');
  CompleteFileName(NameList[4], 'cts');
  CompleteFileName(NameList[5], 'inc');
  CompleteFileName(NameList[6], 'inc');

  if (Include) then
    Limit := 6
  else
    Limit := 4;

  Result := S;

  for I := 1 to Limit do
    if (FileExists(NameList[I])) then
    begin
      Result := NameList[I];
      Break;
    end;
end;

procedure CompleteFileName(var S : String; X : String);
var
  I : Integer;
begin
  // add an extention to the filename if there isn't one
  for I := length(S) downto 1 do
  begin
    if (S[I] = '.') then
      exit;
    if (S[I] = '\') then
      break;
  end;

  S := S + '.' + X;
end;
    :param s: 
    :param include: 
    :return: 
    """
    pass


def read_string(file: FileIO) -> str:
    length = struct.unpack('<i', file.read(4))[0]
    val = struct.unpack(f'<{length}s', file.read(length))[0].decode('ascii')
    # file.read(1)  # swallow null terminal
    return val


def format_datetime(d):
    if d.hour == 0 and d.minute == 0 and d.second == 0:
        return d.strftime("%x")
    else:
        return d.strftime("%x %X")


class Handled:
    def __init__(self):
        self._state = False

    def true(self):
        self._state = True

    def false(self):
        self._state = False

    def __bool__(self):
        return self._state


class Pointer:
    def __init__(self, buffer: bytearray = None, pos: int = 0):
        self.pos: int = pos
        self.buffer = buffer if buffer is not None else bytearray()

    def read_int(self, num_bytes: int):
        result = int.from_bytes(self.buffer[self.pos:self.pos + num_bytes], byteorder='little')
        self.pos += num_bytes
        return result


class PeriodicTimer:

    def __init__(self, delay_trigger: 'Trigger', interval: int, on_timer: Callable, enabled: bool):
        self.delay: int = 0
        self.delay_trigger: 'Trigger' = delay_trigger
        self.interval: int = interval
        self.on_timer: Callable = on_timer
        self.on_start_timer: Callable = None
        self.on_stop_timer: Callable = None
        self._enabled = False
        self.enabled = enabled

    @property
    def enabled(self) -> bool:
        return self._enabled

    @enabled.setter
    def enabled(self, val: bool):
        self._enabled = val
        if val:

            asyncio.Task(self.periodic())

    async def periodic(self):
        await asyncio.sleep(self.delay)
        # self.on_start_timer()
        while self._enabled:
            self.on_timer(self)
            await asyncio.sleep(self.interval)
        # self.on_stop_timer()
